nim (2.2.0-1) UNRELEASED; urgency=medium

  * New upstream version 2.2.0
  * WIP: refresh d/p/ for new upstream
  * d/nim-doc.docs: add install nim/doc/nimdoc.cls
  * d/rules: override_dh_auto_install target update nim-gdb.py install path
  * d/control:
      - set Maintainer to Debian QA Group <packages@qa.debian.org>
        for orphaned package
      - Package nim-doc add Multi-Arch: foreign
      - (build)-depends remove obsolete libpcre3(Closes: #1071691)
  * d/platforms.table: add loong64 loongarch64 line
  * d/rules: add loong64 Architecture support(Closes: #1089673)

 -- xiao sheng wen <atzlinux@debian.org>  Thu, 06 Mar 2025 09:41:57 +0800

nim (1.6.14-3) unstable; urgency=medium

  * Bump version to upload source-only

 -- Federico Ceratto <federico@debian.org>  Thu, 25 Apr 2024 21:32:48 +0200

nim (1.6.14-2) unstable; urgency=medium

  [ Samuel Thibault ]
  * control: Add hurd-amd64 case.

  [ Federico Ceratto ]
  * Drop libssl3 build dep (Closes: #1065904)

 -- Federico Ceratto <federico@debian.org>  Wed, 10 Apr 2024 18:18:55 +0200

nim (1.6.14-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Thu, 29 Jun 2023 12:09:29 +0100

nim (1.6.10-2) unstable; urgency=medium

  * Fix missing CSS, install nimpretty (Closes: #1010929)
    Thanks to Boris Jakubith
  * Change library path, fix docgen, install nim-gdb.py
    Thanks to Gabriel Huber
  * Disable Plausible analytics from koch docs

 -- Federico Ceratto <federico@debian.org>  Sun, 12 Feb 2023 16:48:12 +0000

nim (1.6.10-1) unstable; urgency=medium

  * New upstream release (Closes: #1022204, #1006989)
    Thanks Sergio Durigan Junior <sergiodj@debian.org>

 -- Federico Ceratto <federico@debian.org>  Wed, 23 Nov 2022 19:23:48 +0000

nim (1.6.8-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Sun, 09 Oct 2022 13:15:19 +0100

nim (1.6.6-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Fri, 06 May 2022 21:07:07 +0100

nim (1.6.4-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Thu, 10 Feb 2022 14:02:41 +0000

nim (1.6.2-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Fri, 17 Dec 2021 09:47:40 +0000

nim (1.6.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * d/tests/control: examples is not installed, so inject the
    content of hallo.nim during test, compile and run it
    as autopkgtests (as intended)

 -- Nilesh Patra <nilesh@debian.org>  Wed, 27 Oct 2021 00:29:02 +0530

nim (1.6.0-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Tue, 26 Oct 2021 11:47:32 +0100

nim (1.4.8-3) unstable; urgency=medium

  * Really release 1.4.8 to unstable

 -- Federico Ceratto <federico@debian.org>  Wed, 18 Aug 2021 12:44:55 +0100

nim (1.4.8-2) experimental; urgency=medium

  * Release 1.4.8 to unstable

 -- Federico Ceratto <federico@debian.org>  Mon, 16 Aug 2021 21:23:28 +0100

nim (1.4.8-1) experimental; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Sun, 20 Jun 2021 15:41:13 +0100

nim (1.4.6-1) experimental; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Mon, 19 Apr 2021 20:27:07 +0100

nim (1.4.2-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Wed, 02 Dec 2020 13:39:46 +0000

nim (1.4.0-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Sun, 25 Oct 2020 12:15:37 +0000

nim (1.2.6-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Fri, 07 Aug 2020 21:27:43 +0100

nim (1.2.6~rc1-1) unstable; urgency=medium

  * New upstream release candidate

 -- Federico Ceratto <federico@debian.org>  Wed, 29 Jul 2020 20:54:09 +0100

nim (1.2.4-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Thu, 09 Jul 2020 12:45:21 +0100

nim (1.2.2-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Wed, 17 Jun 2020 13:49:47 +0100

nim (1.2.1~rc2-1) unstable; urgency=medium

  * New upstream release candidate

 -- Federico Ceratto <federico@debian.org>  Wed, 03 Jun 2020 14:44:54 +0100

nim (1.2.1~rc1-1) unstable; urgency=medium

  * New upstream release candidate

 -- Federico Ceratto <federico@debian.org>  Thu, 21 May 2020 23:18:54 +0100

nim (1.2.0-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Sat, 04 Apr 2020 16:54:47 +0100

nim (1.2.0~rc3-1) experimental; urgency=medium

  * New upstream release candidate

 -- Federico Ceratto <federico@debian.org>  Fri, 03 Apr 2020 16:36:30 +0100

nim (1.2.0~rc2-1) experimental; urgency=medium

  * New upstream release candidate

 -- Federico Ceratto <federico@debian.org>  Fri, 03 Apr 2020 12:35:37 +0100

nim (1.2.0~rc-1) experimental; urgency=medium

  * New upstream release candidate

 -- Federico Ceratto <federico@debian.org>  Sat, 21 Mar 2020 19:48:28 +0000

nim (1.0.6-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Mon, 27 Jan 2020 15:07:29 +0000

nim (1.0.5~rc-1) experimental; urgency=medium

  * New upstream release candidate

 -- Federico Ceratto <federico@debian.org>  Sun, 19 Jan 2020 11:11:46 +0000

nim (1.0.4-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Wed, 27 Nov 2019 17:28:21 +0000

nim (1.0.3~rc-1) unstable; urgency=medium

  * New upstream release candidate

 -- Federico Ceratto <federico@debian.org>  Fri, 22 Nov 2019 14:57:35 +0000

nim (1.0.2-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Wed, 23 Oct 2019 14:08:41 +0100

nim (1.0.2~rc-1) experimental; urgency=medium

  * New upstream release candidate

 -- Federico Ceratto <federico@debian.org>  Wed, 23 Oct 2019 09:38:12 +0100

nim (1.0.2~b-1) experimental; urgency=medium

  * New upstream beta release

 -- Federico Ceratto <federico@debian.org>  Mon, 14 Oct 2019 13:13:10 +0100

nim (1.0.2~a-1) experimental; urgency=medium

  * New upstream alpha release

 -- Federico Ceratto <federico@debian.org>  Wed, 09 Oct 2019 18:05:45 +0100

nim (1.0.0-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Tue, 24 Sep 2019 09:41:47 +0100

nim (0.20.2-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Thu, 18 Jul 2019 21:01:58 +0100

nim (0.20.0-2) unstable; urgency=medium

  * Fix build on mips, powerpc, ppc64 and sparc64.
    Thanks to John Paul Adrian Glaubitz.

 -- Federico Ceratto <federico@debian.org>  Wed, 12 Jun 2019 22:13:33 +0100

nim (0.20.0-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Fri, 07 Jun 2019 10:37:13 +0100

nim (0.19.6-2) unstable; urgency=medium

  * Publish v. 0.19.6-2 in Unstable

 -- Federico Ceratto <federico@debian.org>  Sat, 25 May 2019 21:42:59 +0100

nim (0.19.6-1) experimental; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Tue, 14 May 2019 18:22:09 +0100

nim (0.19.4-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Tue, 05 Feb 2019 16:15:32 +0000

nim (0.19.2-2) unstable; urgency=medium

  * Remove MIPS arch (Closes: #919398)

 -- Federico Ceratto <federico@debian.org>  Thu, 17 Jan 2019 22:21:08 +0000

nim (0.19.2-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Thu, 03 Jan 2019 13:23:37 +0000

nim (0.19.0-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Thu, 27 Sep 2018 19:10:03 +0100

nim (0.18.0-2) unstable; urgency=medium

  * Install txt files
  [ Frédéric Bonnard ]
  * Fix FTBFS on ppc64, ppc64el, arm64 and re-enable mips64el (Closes: #892108)

 -- Federico Ceratto <federico@debian.org>  Thu, 15 Mar 2018 21:57:32 +0000

nim (0.18.0-1) unstable; urgency=medium

  * New upstream release
  * Switch to libssl1.1 (Closes: #882549)
  * Remove mips64el target (Closes: #876526)
  * Move packaging VCS to Salsa

 -- Federico Ceratto <federico@debian.org>  Sat, 03 Mar 2018 14:38:45 +0000

nim (0.17.2-1) unstable; urgency=medium

  * New upstream release 0.17.2

 -- Federico Ceratto <federico@debian.org>  Fri, 08 Sep 2017 18:48:00 +0100

nim (0.17.0-2) unstable; urgency=medium

  * Release to Unstable

 -- Federico Ceratto <federico@debian.org>  Tue, 11 Jul 2017 23:56:09 +0100

nim (0.17.0-1) experimental; urgency=medium

  * New upstream release 0.17.0

 -- Federico Ceratto <federico@debian.org>  Tue, 11 Jul 2017 23:55:20 +0100

nim (0.16.0-1) unstable; urgency=medium

  * New upstream release 0.16.0

 -- Federico Ceratto <federico@debian.org>  Mon, 09 Jan 2017 23:41:29 +0000

nim (0.15.2-2) unstable; urgency=medium

  * Upstream release 0.15.2

 -- Federico Ceratto <federico@debian.org>  Mon, 24 Oct 2016 12:31:57 +0100

nim (0.15.2-1) experimental; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Sun, 23 Oct 2016 23:23:55 +0100

nim (0.15.0-3) experimental; urgency=medium

  * Disable generating docs for coro.nim

 -- Federico Ceratto <federico@debian.org>  Thu, 20 Oct 2016 19:03:55 +0100

nim (0.15.0-2) unstable; urgency=medium

  * Support OpenSSL 1.0.2 (Closes: #840969)

 -- Federico Ceratto <federico@debian.org>  Mon, 17 Oct 2016 12:44:18 +0100

nim (0.15.0-1) unstable; urgency=medium

  * New upstream release
  * Add Suggests: nim-doc (Closes: #828051)

 -- Federico Ceratto <federico@debian.org>  Sat, 15 Oct 2016 23:39:18 +0100

nim (0.14.2-1) unstable; urgency=medium

  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Fri, 22 Jul 2016 09:21:28 +0100

nim (0.13.0-1) unstable; urgency=low

  [ Ximin Luo ]
  * Use secure links for Vcs-* fields.

  [ Federico Ceratto ]
  * New upstream release

 -- Federico Ceratto <federico@debian.org>  Mon, 21 Mar 2016 22:15:48 +0000

nim (0.12.0-2) unstable; urgency=medium

  * Fix build on some platforms.

 -- Ximin Luo <infinity0@debian.org>  Mon, 02 Nov 2015 12:42:57 +0100

nim (0.12.0-1) unstable; urgency=low

  [ Federico Ceratto ]
  * New upstream release

  [ Ximin Luo ]
  * Work around upstream build bugs.
  * Add newly-supported archs: arm64, mipsel, ppc64el (Closes: #794273)

 -- Ximin Luo <infinity0@debian.org>  Sun, 01 Nov 2015 19:25:54 +0100

nim (0.11.2+dfsg1-4) unstable; urgency=medium

  * Disable tests that cause FTBFS.

 -- Ximin Luo <infinity0@pwned.gg>  Sun, 09 Aug 2015 11:09:32 +0200

nim (0.11.2+dfsg1-3) unstable; urgency=medium

  * Fix build for kfreebsd-*

 -- Ximin Luo <infinity0@pwned.gg>  Sat, 08 Aug 2015 17:52:07 +0200

nim (0.11.2+dfsg1-2) unstable; urgency=medium

  * Fix build for architectures other than i386, amd64.

 -- Ximin Luo <infinity0@pwned.gg>  Thu, 30 Jul 2015 13:51:44 +0200

nim (0.11.2+dfsg1-1) unstable; urgency=low

  * Initial release (Closes: #778330)

 -- Ximin Luo <infinity0@pwned.gg>  Tue, 14 Jul 2015 14:11:51 +0200
