/* Generated by Nim Compiler v2.2.0 */
#define NIM_INTBITS 32

#include "nimbase.h"
#include <string.h>
#undef LANGUAGE_C
#undef MIPSEB
#undef MIPSEL
#undef PPC
#undef R3000
#undef R4000
#undef i386
#undef linux
#undef mips
#undef near
#undef far
#undef powerpc
#undef unix
typedef struct tySequence__ezS5mGPcRfBURJyMgqYVzA tySequence__ezS5mGPcRfBURJyMgqYVzA;
typedef struct tySequence__ezS5mGPcRfBURJyMgqYVzA_Content tySequence__ezS5mGPcRfBURJyMgqYVzA_Content;
typedef struct tyTuple__48iPM3yLBKU5nyEuYwJvuQ tyTuple__48iPM3yLBKU5nyEuYwJvuQ;
typedef struct tySequence__wW7xkohjuUKO9aMaB0nt9cqA tySequence__wW7xkohjuUKO9aMaB0nt9cqA;
typedef struct tySequence__wW7xkohjuUKO9aMaB0nt9cqA_Content tySequence__wW7xkohjuUKO9aMaB0nt9cqA_Content;
typedef struct tyTuple__R3ddFrA0G9buJY57Qhf9cTpg tyTuple__R3ddFrA0G9buJY57Qhf9cTpg;
typedef struct tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig;
typedef struct tySequence__qBes3XzcQQ9cyia9bRwnmbJw tySequence__qBes3XzcQQ9cyia9bRwnmbJw;
typedef struct tySequence__qBes3XzcQQ9cyia9bRwnmbJw_Content tySequence__qBes3XzcQQ9cyia9bRwnmbJw_Content;
typedef struct tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg;
typedef struct tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w;
typedef struct tySequence__OxRbUT0qZaZN5UjnqQjGPA tySequence__OxRbUT0qZaZN5UjnqQjGPA;
typedef struct tySequence__OxRbUT0qZaZN5UjnqQjGPA_Content tySequence__OxRbUT0qZaZN5UjnqQjGPA_Content;
typedef struct tyObject_PackedSym__KKnx6w8Cc9aaVsaPOpaqxbw tyObject_PackedSym__KKnx6w8Cc9aaVsaPOpaqxbw;
typedef struct tyObject_PackedLib__EpwVLk0Fuc5Q9agKLMWedbg tyObject_PackedLib__EpwVLk0Fuc5Q9agKLMWedbg;
typedef struct tyObject_PackedType__8M8VlYfwJ6AtpxG1HNn4bQ tyObject_PackedType__8M8VlYfwJ6AtpxG1HNn4bQ;
typedef struct tyObject_PackedInstantiation__C3FSePJFY8gUIMObsjT2dg tyObject_PackedInstantiation__C3FSePJFY8gUIMObsjT2dg;
typedef struct tyTuple__1v9bKyksXWMsm0vNwmZ4EuQ tyTuple__1v9bKyksXWMsm0vNwmZ4EuQ;
typedef struct tyObject_PackedNode__YxKk4C732K04CNgtMhmc0w tyObject_PackedNode__YxKk4C732K04CNgtMhmc0w;
typedef struct tyObject_NimRawSeq__Tvl1urqmI1tXqzkHPbWVJQ tyObject_NimRawSeq__Tvl1urqmI1tXqzkHPbWVJQ;
struct tySequence__ezS5mGPcRfBURJyMgqYVzA {
  NI len; tySequence__ezS5mGPcRfBURJyMgqYVzA_Content* p;
};
typedef NU32 tySet_tyEnum_TNodeFlag__3UjGgL9bLM4RXBnoZJs9cviw;
struct tyTuple__48iPM3yLBKU5nyEuYwJvuQ {
	NI32 Field0;
	tySet_tyEnum_TNodeFlag__3UjGgL9bLM4RXBnoZJs9cviw Field1;
};
struct tySequence__wW7xkohjuUKO9aMaB0nt9cqA {
  NI len; tySequence__wW7xkohjuUKO9aMaB0nt9cqA_Content* p;
};
struct tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig {
	NU32 module;
	NI32 item;
};
struct tyTuple__R3ddFrA0G9buJY57Qhf9cTpg {
	NI32 Field0;
	tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig Field1;
};
struct tySequence__qBes3XzcQQ9cyia9bRwnmbJw {
  NI len; tySequence__qBes3XzcQQ9cyia9bRwnmbJw_Content* p;
};
struct tySequence__OxRbUT0qZaZN5UjnqQjGPA {
  NI len; tySequence__OxRbUT0qZaZN5UjnqQjGPA_Content* p;
};
struct tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w {
	tySequence__OxRbUT0qZaZN5UjnqQjGPA nodes;
	tySequence__ezS5mGPcRfBURJyMgqYVzA withFlags;
	tySequence__wW7xkohjuUKO9aMaB0nt9cqA withTypes;
};
typedef NU8 tyEnum_TSymKind__9a75lHd9cV25XwN4jXXBmM0Q;
typedef NU64 tySet_tyEnum_TSymFlag__Ii9cPbwXR8ePKVRFOdTKpDg;
typedef NU16 tyEnum_TMagic__I3BbtzbLAKAfOym58MkGAg;
typedef NU32 tySet_tyEnum_TOption__LEwWf0yAgZkZx5Got2wGSQ;
typedef NU16 tySet_tyEnum_TLocFlag__Df76pxukl21zlyRFq9aa0Bg;
typedef NU8 tyEnum_TLibKind__BkKL8ReLoyYDI7of9aQPV0A;
struct tyObject_PackedLib__EpwVLk0Fuc5Q9agKLMWedbg {
	tyEnum_TLibKind__BkKL8ReLoyYDI7of9aQPV0A kind;
	NIM_BOOL generated;
	NIM_BOOL isOverridden;
	NU32 name;
	NI32 path;
};
struct tyObject_PackedSym__KKnx6w8Cc9aaVsaPOpaqxbw {
	NI32 id;
	tyEnum_TSymKind__9a75lHd9cV25XwN4jXXBmM0Q kind;
	NU32 name;
	tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig typ;
	tySet_tyEnum_TSymFlag__Ii9cPbwXR8ePKVRFOdTKpDg flags;
	tyEnum_TMagic__I3BbtzbLAKAfOym58MkGAg magic;
	NU32 info;
	NI32 ast;
	tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig owner;
	tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig guard;
	NI bitsize;
	NI alignment;
	tySet_tyEnum_TOption__LEwWf0yAgZkZx5Got2wGSQ options;
	NI position;
	NI32 offset;
	NI32 disamb;
	NU32 externalName;
	tySet_tyEnum_TLocFlag__Df76pxukl21zlyRFq9aa0Bg locFlags;
	tyObject_PackedLib__EpwVLk0Fuc5Q9agKLMWedbg annex;
	NI32 constraint;
	tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig instantiatedFrom;
};
typedef NU8 tyEnum_TTypeKind__DYVdhzQ9cMfgoirdzGDwSCg;
typedef NU8 tyEnum_TCallingConvention__MvGn9cvx9bNYQKo7Hhd5GM9cQ;
typedef NU64 tySet_tyEnum_TTypeFlag__HJaExo9c84N85e9br9anJmV1A;
struct tyObject_PackedType__8M8VlYfwJ6AtpxG1HNn4bQ {
	NI32 id;
	tyEnum_TTypeKind__DYVdhzQ9cMfgoirdzGDwSCg kind;
	tyEnum_TCallingConvention__MvGn9cvx9bNYQKo7Hhd5GM9cQ callConv;
	tySet_tyEnum_TTypeFlag__HJaExo9c84N85e9br9anJmV1A flags;
	tySequence__qBes3XzcQQ9cyia9bRwnmbJw types;
	NI32 n;
	tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig sym;
	tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig owner;
	NI64 size;
	NI16 align;
	NI16 paddingAtEnd;
	tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig typeInst;
	NI32 nonUniqueId;
};
struct tyObject_PackedInstantiation__C3FSePJFY8gUIMObsjT2dg {
	tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig key;
	tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig sym;
	tySequence__qBes3XzcQQ9cyia9bRwnmbJw concreteTypes;
};
struct tyTuple__1v9bKyksXWMsm0vNwmZ4EuQ {
	NI Field0;
	NI Field1;
};
struct tyObject_PackedNode__YxKk4C732K04CNgtMhmc0w {
	NU32 x;
	NU32 info;
};
typedef NU8 tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw;
struct tyObject_NimRawSeq__Tvl1urqmI1tXqzkHPbWVJQ {
	NI len;
	void* p;
};
struct tySequence__ezS5mGPcRfBURJyMgqYVzA_Content { NI cap; tyTuple__48iPM3yLBKU5nyEuYwJvuQ data[SEQ_DECL_SIZE]; };
struct tySequence__wW7xkohjuUKO9aMaB0nt9cqA_Content { NI cap; tyTuple__R3ddFrA0G9buJY57Qhf9cTpg data[SEQ_DECL_SIZE]; };
struct tySequence__qBes3XzcQQ9cyia9bRwnmbJw_Content { NI cap; tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig data[SEQ_DECL_SIZE]; };
struct tySequence__OxRbUT0qZaZN5UjnqQjGPA_Content { NI cap; tyObject_PackedNode__YxKk4C732K04CNgtMhmc0w data[SEQ_DECL_SIZE]; };
N_LIB_PRIVATE N_NIMCALL(void, alignedDealloc)(void* p_p0, NI align_p1);
N_LIB_PRIVATE N_NIMCALL(void, loadSeq__icZpacked95ast_u1280)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tySequence__OxRbUT0qZaZN5UjnqQjGPA* s_p1);
N_LIB_PRIVATE N_NIMCALL(void, loadSeq__icZpacked95ast_u1365)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tySequence__ezS5mGPcRfBURJyMgqYVzA* s_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___icZpacked95ast_u265)(tySequence__ezS5mGPcRfBURJyMgqYVzA dest_p0);
N_LIB_PRIVATE N_NIMCALL(void, loadSeq__icZpacked95ast_u1450)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tySequence__wW7xkohjuUKO9aMaB0nt9cqA* s_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___icZpacked95ast_u370)(tySequence__wW7xkohjuUKO9aMaB0nt9cqA dest_p0);
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void);
static N_INLINE(void, nimZeroMem)(void* p_p0, NI size_p1);
static N_INLINE(void, nimSetMem__systemZmemory_u7)(void* a_p0, int v_p1, NI size_p2);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___icZic_u3863)(tySequence__qBes3XzcQQ9cyia9bRwnmbJw dest_p0);
N_LIB_PRIVATE N_NIMCALL(void, setLen__icZic_u3883)(tySequence__qBes3XzcQQ9cyia9bRwnmbJw* s_p0, NI newlen_p1);
static N_INLINE(NI, span__icZpacked95ast_u1123)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w tree_p0, NI pos_p1);
static N_INLINE(NIM_BOOL, isAtom__icZpacked95ast_u547)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w tree_p0, NI pos_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___icZpacked95ast_u123)(tySequence__OxRbUT0qZaZN5UjnqQjGPA dest_p0);
N_LIB_PRIVATE N_NIMCALL(void*, newSeqPayload)(NI cap_p0, NI elemSize_p1, NI elemAlign_p2);
static N_INLINE(NIM_BOOL, sameSeqPayload)(void* x_p0, void* y_p1);
N_LIB_PRIVATE N_NIMCALL(void, addNode__icZpacked95ast_u556)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w* t_p0, tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw kind_p1, NI32 operand_p2, tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig typeId_p3, NU32 info_p4, tySet_tyEnum_TNodeFlag__3UjGgL9bLM4RXBnoZJs9cviw flags_p5);
N_LIB_PRIVATE N_NIMCALL(void, add__icZpacked95ast_u483)(tySequence__OxRbUT0qZaZN5UjnqQjGPA* x_p0, tyObject_PackedNode__YxKk4C732K04CNgtMhmc0w y_p1);
N_LIB_PRIVATE N_NIMCALL(void, add__icZpacked95ast_u578)(tySequence__ezS5mGPcRfBURJyMgqYVzA* x_p0, tyTuple__48iPM3yLBKU5nyEuYwJvuQ y_p1);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, eqeq___icZpacked95ast_u598)(tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig x_p0, tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig y_p1);
N_LIB_PRIVATE N_NIMCALL(void, add__icZpacked95ast_u611)(tySequence__wW7xkohjuUKO9aMaB0nt9cqA* x_p0, tyTuple__R3ddFrA0G9buJY57Qhf9cTpg y_p1);
N_LIB_PRIVATE N_NIMCALL(void, storeSeq__icZpacked95ast_u1538)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tySequence__OxRbUT0qZaZN5UjnqQjGPA s_p1);
N_LIB_PRIVATE N_NIMCALL(void, storeSeq__icZpacked95ast_u1588)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tySequence__ezS5mGPcRfBURJyMgqYVzA s_p1);
N_LIB_PRIVATE N_NIMCALL(void, storeSeq__icZpacked95ast_u1638)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tySequence__wW7xkohjuUKO9aMaB0nt9cqA s_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqcopy___icZic_u3866)(tySequence__qBes3XzcQQ9cyia9bRwnmbJw* dest_p0, tySequence__qBes3XzcQQ9cyia9bRwnmbJw src_p1);
static N_INLINE(void, nextChild__icZpacked95ast_u804)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w tree_p0, NI* pos_p1);
N_LIB_PRIVATE NIM_CONST tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig nilItemId__icZpacked95ast_u17 = {((NU32)0), ((NI32)0)}
;
extern NIM_BOOL nimInErrorMode__system_u4310;
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___icZpacked95ast_u265)(tySequence__ezS5mGPcRfBURJyMgqYVzA dest_p0) {
	if (dest_p0.p && !(dest_p0.p->cap & NIM_STRLIT_FLAG)) {
 alignedDealloc(dest_p0.p, NIM_ALIGNOF(tyTuple__48iPM3yLBKU5nyEuYwJvuQ));
}
}
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___icZpacked95ast_u370)(tySequence__wW7xkohjuUKO9aMaB0nt9cqA dest_p0) {
	if (dest_p0.p && !(dest_p0.p->cap & NIM_STRLIT_FLAG)) {
 alignedDealloc(dest_p0.p, NIM_ALIGNOF(tyTuple__R3ddFrA0G9buJY57Qhf9cTpg));
}
}
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___icZic_u3863)(tySequence__qBes3XzcQQ9cyia9bRwnmbJw dest_p0) {
	if (dest_p0.p && !(dest_p0.p->cap & NIM_STRLIT_FLAG)) {
 alignedDealloc(dest_p0.p, NIM_ALIGNOF(tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig));
}
}
N_LIB_PRIVATE N_NIMCALL(void, eqsink___icZpacked95ast_u274)(tySequence__ezS5mGPcRfBURJyMgqYVzA* dest_p0, tySequence__ezS5mGPcRfBURJyMgqYVzA src_p1) {
	if ((*dest_p0).p != src_p1.p) {	eqdestroy___icZpacked95ast_u265((*dest_p0));
	}
(*dest_p0).len = src_p1.len; (*dest_p0).p = src_p1.p;
}
N_LIB_PRIVATE N_NIMCALL(void, eqsink___icZpacked95ast_u379)(tySequence__wW7xkohjuUKO9aMaB0nt9cqA* dest_p0, tySequence__wW7xkohjuUKO9aMaB0nt9cqA src_p1) {
	if ((*dest_p0).p != src_p1.p) {	eqdestroy___icZpacked95ast_u370((*dest_p0));
	}
(*dest_p0).len = src_p1.len; (*dest_p0).p = src_p1.p;
}
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void) {
	NIM_BOOL* result;
	result = (&nimInErrorMode__system_u4310);
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, load__icZpacked95ast_u1277)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w* t_p1) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	loadSeq__icZpacked95ast_u1280(f_p0, (&(*t_p1).nodes));
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	loadSeq__icZpacked95ast_u1365(f_p0, (&(*t_p1).withFlags));
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	loadSeq__icZpacked95ast_u1450(f_p0, (&(*t_p1).withTypes));
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
static N_INLINE(void, nimSetMem__systemZmemory_u7)(void* a_p0, int v_p1, NI size_p2) {
	void* T1_;
	T1_ = (void*)0;
	T1_ = memset(a_p0, v_p1, ((size_t) (size_p2)));
}
static N_INLINE(void, nimZeroMem)(void* p_p0, NI size_p1) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	nimSetMem__systemZmemory_u7(p_p0, ((int)0), size_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, eqwasMoved___icZic_u6576)(tyObject_PackedSym__KKnx6w8Cc9aaVsaPOpaqxbw* dest_p0) {
	(*dest_p0).id = 0;
	(*dest_p0).kind = 0;
	(*dest_p0).name = 0;
	(*dest_p0).typ.module = 0;
	(*dest_p0).typ.item = 0;
	nimZeroMem((void*)(&(*dest_p0).flags), sizeof(tySet_tyEnum_TSymFlag__Ii9cPbwXR8ePKVRFOdTKpDg));
	(*dest_p0).magic = 0;
	(*dest_p0).info = 0;
	(*dest_p0).ast = 0;
	(*dest_p0).owner.module = 0;
	(*dest_p0).owner.item = 0;
	(*dest_p0).guard.module = 0;
	(*dest_p0).guard.item = 0;
	(*dest_p0).bitsize = 0;
	(*dest_p0).alignment = 0;
	nimZeroMem((void*)(&(*dest_p0).options), sizeof(tySet_tyEnum_TOption__LEwWf0yAgZkZx5Got2wGSQ));
	(*dest_p0).position = 0;
	(*dest_p0).offset = 0;
	(*dest_p0).disamb = 0;
	(*dest_p0).externalName = 0;
	nimZeroMem((void*)(&(*dest_p0).locFlags), sizeof(tySet_tyEnum_TLocFlag__Df76pxukl21zlyRFq9aa0Bg));
	(*dest_p0).annex.kind = 0;
	(*dest_p0).annex.generated = 0;
	(*dest_p0).annex.isOverridden = 0;
	(*dest_p0).annex.name = 0;
	(*dest_p0).annex.path = 0;
	(*dest_p0).constraint = 0;
	(*dest_p0).instantiatedFrom.module = 0;
	(*dest_p0).instantiatedFrom.item = 0;
}
N_LIB_PRIVATE N_NIMCALL(void, eqsink___icZic_u3872)(tySequence__qBes3XzcQQ9cyia9bRwnmbJw* dest_p0, tySequence__qBes3XzcQQ9cyia9bRwnmbJw src_p1) {
	if ((*dest_p0).p != src_p1.p) {	eqdestroy___icZic_u3863((*dest_p0));
	}
(*dest_p0).len = src_p1.len; (*dest_p0).p = src_p1.p;
}
N_LIB_PRIVATE N_NIMCALL(void, eqsink___icZic_u4926)(tyObject_PackedType__8M8VlYfwJ6AtpxG1HNn4bQ* dest_p0, tyObject_PackedType__8M8VlYfwJ6AtpxG1HNn4bQ* src_p1) {
	(*dest_p0).id = (*src_p1).id;
	(*dest_p0).kind = (*src_p1).kind;
	(*dest_p0).callConv = (*src_p1).callConv;
	(*dest_p0).flags = (*src_p1).flags;
	if ((*dest_p0).types.p != (*src_p1).types.p) {	eqdestroy___icZic_u3863((*dest_p0).types);
	}
(*dest_p0).types.len = (*src_p1).types.len; (*dest_p0).types.p = (*src_p1).types.p;
	(*dest_p0).n = (*src_p1).n;
	(*dest_p0).sym.module = (*src_p1).sym.module;
	(*dest_p0).sym.item = (*src_p1).sym.item;
	(*dest_p0).owner.module = (*src_p1).owner.module;
	(*dest_p0).owner.item = (*src_p1).owner.item;
	(*dest_p0).size = (*src_p1).size;
	(*dest_p0).align = (*src_p1).align;
	(*dest_p0).paddingAtEnd = (*src_p1).paddingAtEnd;
	(*dest_p0).typeInst.module = (*src_p1).typeInst.module;
	(*dest_p0).typeInst.item = (*src_p1).typeInst.item;
	(*dest_p0).nonUniqueId = (*src_p1).nonUniqueId;
}
N_LIB_PRIVATE N_NIMCALL(void, eqwasMoved___icZic_u4914)(tyObject_PackedType__8M8VlYfwJ6AtpxG1HNn4bQ* dest_p0) {
	(*dest_p0).id = 0;
	(*dest_p0).kind = 0;
	(*dest_p0).callConv = 0;
	nimZeroMem((void*)(&(*dest_p0).flags), sizeof(tySet_tyEnum_TTypeFlag__HJaExo9c84N85e9br9anJmV1A));
	(*dest_p0).types.len = 0; (*dest_p0).types.p = NIM_NIL;
	(*dest_p0).n = 0;
	(*dest_p0).sym.module = 0;
	(*dest_p0).sym.item = 0;
	(*dest_p0).owner.module = 0;
	(*dest_p0).owner.item = 0;
	(*dest_p0).size = 0;
	(*dest_p0).align = 0;
	(*dest_p0).paddingAtEnd = 0;
	(*dest_p0).typeInst.module = 0;
	(*dest_p0).typeInst.item = 0;
	(*dest_p0).nonUniqueId = 0;
}
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___icZic_u4917)(tyObject_PackedType__8M8VlYfwJ6AtpxG1HNn4bQ* dest_p0) {
	eqdestroy___icZic_u3863((*dest_p0).types);
}
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___icZic_u7507)(tyObject_PackedInstantiation__C3FSePJFY8gUIMObsjT2dg dest_p0) {
	eqdestroy___icZic_u3863(dest_p0.concreteTypes);
}
N_LIB_PRIVATE N_NIMCALL(void, eqwasMoved___icZic_u7504)(tyObject_PackedInstantiation__C3FSePJFY8gUIMObsjT2dg* dest_p0) {
	(*dest_p0).key.module = 0;
	(*dest_p0).key.item = 0;
	(*dest_p0).sym.module = 0;
	(*dest_p0).sym.item = 0;
	(*dest_p0).concreteTypes.len = 0; (*dest_p0).concreteTypes.p = NIM_NIL;
}
N_LIB_PRIVATE N_NIMCALL(void, eqwasMoved___icZic_u3860)(tySequence__qBes3XzcQQ9cyia9bRwnmbJw* dest_p0) {
	(*dest_p0).len = 0; (*dest_p0).p = NIM_NIL;
}
N_LIB_PRIVATE N_NIMCALL(tySequence__qBes3XzcQQ9cyia9bRwnmbJw, eqdup___icZic_u3869)(tySequence__qBes3XzcQQ9cyia9bRwnmbJw src_p0) {
	tySequence__qBes3XzcQQ9cyia9bRwnmbJw result;
	NI T1_;
	NI colontmp_;
	result.len = 0; result.p = NIM_NIL;
	T1_ = src_p0.len;
	setLen__icZic_u3883((&result), T1_);
	colontmp_ = ((NI)0);
	{
		while (1) {
			NI T4_;
			T4_ = result.len;
			if (!(colontmp_ < T4_)) goto LA3;
			result.p->data[colontmp_].module = src_p0.p->data[colontmp_].module;
			result.p->data[colontmp_].item = src_p0.p->data[colontmp_].item;
			colontmp_ += ((NI)1);
		} LA3: ;
	}
	return result;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig, findType__icZpacked95ast_u1031)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w tree_p0, NI n_p1) {
	tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig result;
{	{
		tyTuple__R3ddFrA0G9buJY57Qhf9cTpg* x;
		NI i;
		NI L;
		NI T2_;
		x = (tyTuple__R3ddFrA0G9buJY57Qhf9cTpg*)0;
		i = ((NI)0);
		T2_ = tree_p0.withTypes.len;
		L = T2_;
		{
			while (1) {
				if (!(i < L)) goto LA4;
				x = (&tree_p0.withTypes.p->data[i]);
				{
					if (!((*x).Field0 == ((NI32) (n_p1)))) goto LA7_;
					result = (*x).Field1;
					goto BeforeRet_;
				}
LA7_: ;
				{
					if (!(((NI32) (n_p1)) < (*x).Field0)) goto LA11_;
					result = nilItemId__icZpacked95ast_u17;
					goto BeforeRet_;
				}
LA11_: ;
				i += ((NI)1);
			} LA4: ;
		}
	}
	result = nilItemId__icZpacked95ast_u17;
	goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(tySet_tyEnum_TNodeFlag__3UjGgL9bLM4RXBnoZJs9cviw, findFlags__icZpacked95ast_u1074)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w tree_p0, NI n_p1) {
	tySet_tyEnum_TNodeFlag__3UjGgL9bLM4RXBnoZJs9cviw result;
{	{
		tyTuple__48iPM3yLBKU5nyEuYwJvuQ* x;
		NI i;
		NI L;
		NI T2_;
		x = (tyTuple__48iPM3yLBKU5nyEuYwJvuQ*)0;
		i = ((NI)0);
		T2_ = tree_p0.withFlags.len;
		L = T2_;
		{
			while (1) {
				if (!(i < L)) goto LA4;
				x = (&tree_p0.withFlags.p->data[i]);
				{
					if (!((*x).Field0 == ((NI32) (n_p1)))) goto LA7_;
					result = (*x).Field1;
					goto BeforeRet_;
				}
LA7_: ;
				{
					if (!(((NI32) (n_p1)) < (*x).Field0)) goto LA11_;
					result = 0;
					goto BeforeRet_;
				}
LA11_: ;
				i += ((NI)1);
			} LA4: ;
		}
	}
	result = 0;
	goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
static N_INLINE(NIM_BOOL, isAtom__icZpacked95ast_u547)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w tree_p0, NI pos_p1) {
	NIM_BOOL result;
	result = (NIM_BOOL)0;
	result = (((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw) ((NU32)(tree_p0.nodes.p->data[pos_p1].x & ((NU32)255)))) <= ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)23));
	return result;
}
static N_INLINE(NI, span__icZpacked95ast_u1123)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w tree_p0, NI pos_p1) {
	NI result;
	NI colontmpD_;
	NI colontmpD__2;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NI)0;
	colontmpD_ = (NI)0;
	colontmpD__2 = (NI)0;
	{
		NIM_BOOL T3_;
		T3_ = (NIM_BOOL)0;
		T3_ = isAtom__icZpacked95ast_u547(tree_p0, pos_p1);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		if (!T3_) goto LA4_;
		colontmpD_ = ((NI)1);
		result = colontmpD_;
	}
	goto LA1_;
LA4_: ;
	{
		colontmpD__2 = ((NI) ((NU32)((NU32)(tree_p0.nodes.p->data[pos_p1].x) >> (NU32)(((NU32)8)))));
		result = colontmpD__2;
	}
LA1_: ;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(tyTuple__1v9bKyksXWMsm0vNwmZ4EuQ, sons2__icZpacked95ast_u1131)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w tree_p0, NI n_p1) {
	tyTuple__1v9bKyksXWMsm0vNwmZ4EuQ result;
	NI a;
	NI b;
	NI T1_;
	NI colontmp_;
	NI colontmp__2;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	nimZeroMem((void*)(&result), sizeof(tyTuple__1v9bKyksXWMsm0vNwmZ4EuQ));
	a = (NI)(n_p1 + ((NI)1));
	T1_ = (NI)0;
	T1_ = span__icZpacked95ast_u1123(tree_p0, a);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	b = (NI)(a + T1_);
	colontmp_ = a;
	colontmp__2 = b;
	result.Field0 = colontmp_;
	result.Field1 = colontmp__2;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, eqsink___icZpacked95ast_u255)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w* dest_p0, tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w src_p1) {
	if ((*dest_p0).nodes.p != src_p1.nodes.p) {	eqdestroy___icZpacked95ast_u123((*dest_p0).nodes);
	}
(*dest_p0).nodes.len = src_p1.nodes.len; (*dest_p0).nodes.p = src_p1.nodes.p;
	if ((*dest_p0).withFlags.p != src_p1.withFlags.p) {	eqdestroy___icZpacked95ast_u265((*dest_p0).withFlags);
	}
(*dest_p0).withFlags.len = src_p1.withFlags.len; (*dest_p0).withFlags.p = src_p1.withFlags.p;
	if ((*dest_p0).withTypes.p != src_p1.withTypes.p) {	eqdestroy___icZpacked95ast_u370((*dest_p0).withTypes);
	}
(*dest_p0).withTypes.len = src_p1.withTypes.len; (*dest_p0).withTypes.p = src_p1.withTypes.p;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w, newTreeFrom__icZpacked95ast_u113)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w old_p0) {
	tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w result;
	nimZeroMem((void*)(&result), sizeof(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w));
	result.nodes.len = 0; result.nodes.p = (tySequence__OxRbUT0qZaZN5UjnqQjGPA_Content*) newSeqPayload(0, sizeof(tyObject_PackedNode__YxKk4C732K04CNgtMhmc0w), NIM_ALIGNOF(tyObject_PackedNode__YxKk4C732K04CNgtMhmc0w));
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___icZpacked95ast_u246)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w dest_p0) {
	eqdestroy___icZpacked95ast_u123(dest_p0.nodes);
	eqdestroy___icZpacked95ast_u265(dest_p0.withFlags);
	eqdestroy___icZpacked95ast_u370(dest_p0.withTypes);
}
static N_INLINE(NIM_BOOL, sameSeqPayload)(void* x_p0, void* y_p1) {
	NIM_BOOL result;
	result = ((*((tyObject_NimRawSeq__Tvl1urqmI1tXqzkHPbWVJQ*) (x_p0))).p == (*((tyObject_NimRawSeq__Tvl1urqmI1tXqzkHPbWVJQ*) (y_p1))).p);
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, eqcopy___icZic_u3866)(tySequence__qBes3XzcQQ9cyia9bRwnmbJw* dest_p0, tySequence__qBes3XzcQQ9cyia9bRwnmbJw src_p1) {
	NI T6_;
	NI colontmp_;
{	{
		NIM_BOOL T3_;
		T3_ = (NIM_BOOL)0;
		T3_ = sameSeqPayload(dest_p0, (&src_p1));
		if (!T3_) goto LA4_;
		goto BeforeRet_;
	}
LA4_: ;
	T6_ = src_p1.len;
	setLen__icZic_u3883((&(*dest_p0)), T6_);
	colontmp_ = ((NI)0);
	{
		while (1) {
			NI T9_;
			T9_ = (*dest_p0).len;
			if (!(colontmp_ < T9_)) goto LA8;
			(*dest_p0).p->data[colontmp_].module = src_p1.p->data[colontmp_].module;
			(*dest_p0).p->data[colontmp_].item = src_p1.p->data[colontmp_].item;
			colontmp_ += ((NI)1);
		} LA8: ;
	}
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, addNode__icZpacked95ast_u556)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w* t_p0, tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw kind_p1, NI32 operand_p2, tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig typeId_p3, NU32 info_p4, tySet_tyEnum_TNodeFlag__3UjGgL9bLM4RXBnoZJs9cviw flags_p5) {
	NU32 colontmpD_;
	tyObject_PackedNode__YxKk4C732K04CNgtMhmc0w T1_;
	colontmpD_ = (NU32)0;
	T1_.x = (NU32)(((NU32) (kind_p1)) | (NU32)((NU32)(((NU32) (operand_p2))) << (NU32)(((NU32)8))));
	colontmpD_ = info_p4;
	T1_.info = colontmpD_;
	add__icZpacked95ast_u483((&(*t_p0).nodes), T1_);
	{
		tySet_tyEnum_TNodeFlag__3UjGgL9bLM4RXBnoZJs9cviw T4_;
		tySet_tyEnum_TNodeFlag__3UjGgL9bLM4RXBnoZJs9cviw colontmpD__2;
		tyTuple__48iPM3yLBKU5nyEuYwJvuQ T7_;
		NI T8_;
		T4_ = 0;
		if (!!((flags_p5 == T4_))) goto LA5_;
		nimZeroMem((void*)(&colontmpD__2), sizeof(tySet_tyEnum_TNodeFlag__3UjGgL9bLM4RXBnoZJs9cviw));
		T8_ = (*t_p0).nodes.len;
		T7_.Field0 = (NI32)(((NI32) (T8_)) - ((NI32)1));
		colontmpD__2 = flags_p5;
		T7_.Field1 = colontmpD__2;
		add__icZpacked95ast_u578((&(*t_p0).withFlags), T7_);
	}
LA5_: ;
	{
		NIM_BOOL T11_;
		tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig colontmpD__3;
		tyTuple__R3ddFrA0G9buJY57Qhf9cTpg T14_;
		NI T15_;
		T11_ = (NIM_BOOL)0;
		T11_ = eqeq___icZpacked95ast_u598(typeId_p3, nilItemId__icZpacked95ast_u17);
		if (!!(T11_)) goto LA12_;
		nimZeroMem((void*)(&colontmpD__3), sizeof(tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig));
		T15_ = (*t_p0).nodes.len;
		T14_.Field0 = (NI32)(((NI32) (T15_)) - ((NI32)1));
		colontmpD__3 = typeId_p3;
		T14_.Field1 = colontmpD__3;
		add__icZpacked95ast_u611((&(*t_p0).withTypes), T14_);
	}
LA12_: ;
}
N_LIB_PRIVATE N_NIMCALL(NI, prepare__icZpacked95ast_u692)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w* tree_p0, tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw kind_p1, tySet_tyEnum_TNodeFlag__3UjGgL9bLM4RXBnoZJs9cviw flags_p2, tyObject_PackedItemId__WqAMZCDwoZiUuOt3ohcDig typeId_p3, NU32 info_p4) {
	NI result;
	NI T1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	T1_ = (*tree_p0).nodes.len;
	result = T1_;
	addNode__icZpacked95ast_u556(tree_p0, kind_p1, ((NI32)0), typeId_p3, info_p4, flags_p2);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, patch__icZpacked95ast_u750)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w* tree_p0, NI pos_p1) {
	NI pos_2;
	tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw k;
	NI32 distance;
	NI T1_;
	pos_2 = pos_p1;
	k = ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw) ((NU32)((*tree_p0).nodes.p->data[pos_2].x & ((NU32)255))));
	T1_ = (*tree_p0).nodes.len;
	distance = ((NI32) ((NI)(T1_ - pos_2)));
	(*tree_p0).nodes.p->data[pos_2].x = (NU32)(((NU32) (k)) | (NU32)((NU32)(((NU32) (distance))) << (NU32)(((NU32)8))));
}
N_LIB_PRIVATE N_NIMCALL(void, store__icZpacked95ast_u1535)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w t_p1) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	storeSeq__icZpacked95ast_u1538(f_p0, t_p1.nodes);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	storeSeq__icZpacked95ast_u1588(f_p0, t_p1.withFlags);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	storeSeq__icZpacked95ast_u1638(f_p0, t_p1.withTypes);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, eqcopy___icZic_u4920)(tyObject_PackedType__8M8VlYfwJ6AtpxG1HNn4bQ* dest_p0, tyObject_PackedType__8M8VlYfwJ6AtpxG1HNn4bQ* src_p1) {
	(*dest_p0).id = (*src_p1).id;
	(*dest_p0).kind = (*src_p1).kind;
	(*dest_p0).callConv = (*src_p1).callConv;
	(*dest_p0).flags = (*src_p1).flags;
	eqcopy___icZic_u3866((&(*dest_p0).types), (*src_p1).types);
	(*dest_p0).n = (*src_p1).n;
	(*dest_p0).sym.module = (*src_p1).sym.module;
	(*dest_p0).sym.item = (*src_p1).sym.item;
	(*dest_p0).owner.module = (*src_p1).owner.module;
	(*dest_p0).owner.item = (*src_p1).owner.item;
	(*dest_p0).size = (*src_p1).size;
	(*dest_p0).align = (*src_p1).align;
	(*dest_p0).paddingAtEnd = (*src_p1).paddingAtEnd;
	(*dest_p0).typeInst.module = (*src_p1).typeInst.module;
	(*dest_p0).typeInst.item = (*src_p1).typeInst.item;
	(*dest_p0).nonUniqueId = (*src_p1).nonUniqueId;
}
static N_INLINE(void, nextChild__icZpacked95ast_u804)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w tree_p0, NI* pos_p1) {
	{
		if (!(((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)23) < ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw) ((NU32)(tree_p0.nodes.p->data[(*pos_p1)].x & ((NU32)255)))))) goto LA3_;
		(*pos_p1) += ((NI) ((NU32)((NU32)(tree_p0.nodes.p->data[(*pos_p1)].x) >> (NU32)(((NU32)8)))));
	}
	goto LA1_;
LA3_: ;
	{
		(*pos_p1) += ((NI)1);
	}
LA1_: ;
}
N_LIB_PRIVATE N_NIMCALL(NI, ithSon__icZpacked95ast_u1191)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w tree_p0, NI n_p1, NI i_p2) {
	NI result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = 0;
	{
		NI count;
		if (!(((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)23) < ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw) ((NU32)(tree_p0.nodes.p->data[n_p1].x & ((NU32)255)))))) goto LA3_;
		count = ((NI)0);
		{
			NI child;
			NI pos;
			NI last;
			child = (NI)0;
			pos = n_p1;
			last = (NI)(pos + ((NI) ((NU32)((NU32)(tree_p0.nodes.p->data[pos].x) >> (NU32)(((NU32)8))))));
			pos += ((NI)1);
			{
				while (1) {
					if (!(pos < last)) goto LA7;
					child = pos;
					{
						if (!(count == i_p2)) goto LA10_;
						result = child;
						goto BeforeRet_;
					}
LA10_: ;
					count += ((NI)1);
					nextChild__icZpacked95ast_u804(tree_p0, (&pos));
					if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
				} LA7: ;
			}
		}
	}
LA3_: ;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NI, parentImpl__icZpacked95ast_u963)(tyObject_PackedTree__YtSUB9b7CaIoOyHxDty9bm6w tree_p0, NI n_p1) {
	NI result;
	NI pos;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NI)0;
	pos = (NI)(n_p1 - ((NI)1));
	{
		while (1) {
			NIM_BOOL T3_;
			NIM_BOOL T5_;
			T3_ = (NIM_BOOL)0;
			T3_ = (((NI)0) <= pos);
			if (!(T3_)) goto LA4_;
			T5_ = (NIM_BOOL)0;
			T5_ = isAtom__icZpacked95ast_u547(tree_p0, pos);
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			if (T5_) goto LA6_;
			T5_ = ((NI)((NI)(pos + ((NI) ((NU32)((NU32)(tree_p0.nodes.p->data[pos].x) >> (NU32)(((NU32)8)))))) - ((NI)1)) < n_p1);
LA6_: ;
			T3_ = T5_;
LA4_: ;
			if (!T3_) goto LA2;
			pos -= ((NI)1);
		} LA2: ;
	}
	result = pos;
	}BeforeRet_: ;
	return result;
}
