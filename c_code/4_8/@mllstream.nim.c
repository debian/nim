/* Generated by Nim Compiler v2.2.0 */
#define NIM_INTBITS 32

#include "nimbase.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#undef LANGUAGE_C
#undef MIPSEB
#undef MIPSEL
#undef PPC
#undef R3000
#undef R4000
#undef i386
#undef linux
#undef mips
#undef near
#undef far
#undef powerpc
#undef unix
typedef struct tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg;
typedef struct NimStrPayload NimStrPayload;
typedef struct NimStringV2 NimStringV2;
typedef struct RootObj RootObj;
typedef struct TNimTypeV2 TNimTypeV2;
typedef struct tyObject_RefHeader__GDnGTjNyvwISJbkIvAst3A tyObject_RefHeader__GDnGTjNyvwISJbkIvAst3A;
typedef struct tyObject_GcEnv__HccVszwojlCZRN46BE9cG8w tyObject_GcEnv__HccVszwojlCZRN46BE9cG8w;
typedef struct tyObject_CellSeq__gW9aeWLFbrNaeUKXqxYBXhg tyObject_CellSeq__gW9aeWLFbrNaeUKXqxYBXhg;
typedef struct tyTuple__N4J9cV4JZGem3ljqqj5rT0Q tyTuple__N4J9cV4JZGem3ljqqj5rT0Q;
typedef struct tyObject_CellSeq__IHvIqxITv1yMjJj8lwLrhg tyObject_CellSeq__IHvIqxITv1yMjJj8lwLrhg;
typedef struct tyTuple__DTRsWi2kgTewbhN2TT5mbA tyTuple__DTRsWi2kgTewbhN2TT5mbA;
struct NimStrPayload {
	NI cap;
	NIM_CHAR data[SEQ_DECL_SIZE];
};
struct NimStringV2 {
	NI len;
	NimStrPayload* p;
};
typedef NU8 tyEnum_FileMode__YkEVJbjDa9aPvYTBr8yiUWg;
struct TNimTypeV2 {
	void* destructor;
	NI size;
	NI16 align;
	NI16 depth;
	NU32* display;
	void* traceImpl;
	void* typeInfoV1;
	NI flags;
	void* vTable[SEQ_DECL_SIZE];
};
struct RootObj {
	TNimTypeV2* m_type;
};
typedef NU8 tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ;
typedef struct {
N_NIMCALL_PTR(NI, ClP_0) (tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* s_p0, void* buf_p1, NI bufLen_p2, void* ClE_0);
void* ClE_0;
} tyProc__WBr0JFZ9bbIGwzgqNYcBJtQ;
typedef struct {
N_NIMCALL_PTR(void, ClP_0) (void* ClE_0);
void* ClE_0;
} tyProc__HzVCwACFYM9cx9aV62PdjtuA;
struct tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg {
	RootObj Sup;
	tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ kind;
	FILE* f;
	NimStringV2 s;
	NI rd;
	NI wr;
	NI lineOffset;
	tyProc__WBr0JFZ9bbIGwzgqNYcBJtQ repl;
	tyProc__HzVCwACFYM9cx9aV62PdjtuA onPrompt;
};
struct tyObject_RefHeader__GDnGTjNyvwISJbkIvAst3A {
	NI rc;
	NI rootIdx;
};
struct tyObject_CellSeq__gW9aeWLFbrNaeUKXqxYBXhg {
	NI len;
	NI cap;
	tyTuple__N4J9cV4JZGem3ljqqj5rT0Q* d;
};
struct tyObject_CellSeq__IHvIqxITv1yMjJj8lwLrhg {
	NI len;
	NI cap;
	tyTuple__DTRsWi2kgTewbhN2TT5mbA* d;
};
struct tyObject_GcEnv__HccVszwojlCZRN46BE9cG8w {
	tyObject_CellSeq__gW9aeWLFbrNaeUKXqxYBXhg traceStack;
	tyObject_CellSeq__IHvIqxITv1yMjJj8lwLrhg toFree;
	NI freed;
	NI touched;
	NI edges;
	NI rcSum;
	NIM_BOOL keepThreshold;
};
struct tyTuple__N4J9cV4JZGem3ljqqj5rT0Q {
	void** Field0;
	TNimTypeV2* Field1;
};
typedef N_CLOSURE_PTR(void, TM__ml9aueblxGz9aYsRLKvPmpow_4) (void);
typedef N_CLOSURE_PTR(NI, TM__ml9aueblxGz9aYsRLKvPmpow_5) (tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* s_p0, void* buf_p1, NI bufLen_p2);
typedef NU8 tySet_tyChar__nmiMWKVIe46vacnhAFrQvw[32];
N_LIB_PRIVATE N_NIMCALL(void*, nimNewObj)(NI size_p0, NI alignment_p1);
N_LIB_PRIVATE N_NIMCALL(void, rttiDestroy__llstream_u369)(void* dest_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___llstream_u56)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* dest_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___system_u2597)(NimStringV2 dest_p0);
static N_INLINE(NIM_BOOL, nimDecRefIsLastCyclicDyn)(void* p_p0);
static N_INLINE(NI, minuspercent___system_u799)(NI x_p0, NI y_p1);
N_LIB_PRIVATE N_NOINLINE(void, rememberCycle__system_u3476)(NIM_BOOL isDestroyAction_p0, tyObject_RefHeader__GDnGTjNyvwISJbkIvAst3A* s_p1, TNimTypeV2* desc_p2);
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void);
N_LIB_PRIVATE N_NIMCALL(void, nimDestroyAndDispose)(void* p_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqtrace___llstream_u68)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* dest_p0, void* env_p1);
static N_INLINE(void, nimTraceRefDyn)(void* q_p0, void* env_p1);
static N_INLINE(void, add__system_u2959)(tyObject_CellSeq__gW9aeWLFbrNaeUKXqxYBXhg* s_p0, void** c_p1, TNimTypeV2* t_p2);
N_LIB_PRIVATE N_NIMCALL(void, resize__system_u2967)(tyObject_CellSeq__gW9aeWLFbrNaeUKXqxYBXhg* s_p0);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, open__stdZsyncio_u417)(FILE** f_p0, NimStringV2 filename_p1, tyEnum_FileMode__YkEVJbjDa9aPvYTBr8yiUWg mode_p2, NI bufSize_p3);
N_LIB_PRIVATE N_NIMCALL(void, eqsink___llstream_u122)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg** dest_p0, tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* src_p1);
static N_INLINE(void, nimIncRefCyclic)(void* p_p0, NIM_BOOL cyclic_p1);
static N_INLINE(void, copyMem__system_u1713)(void* dest_p0, void* source_p1, NI size_p2);
static N_INLINE(void, nimCopyMem)(void* dest_p0, void* source_p1, NI size_p2);
N_LIB_PRIVATE N_NIMCALL(NI, readBuffer__stdZsyncio_u164)(FILE* f_p0, void* buffer_p1, NI len_p2);
N_LIB_PRIVATE N_NIMCALL(void, close__stdZsyncio_u266)(FILE* f_p0);
N_LIB_PRIVATE N_NIMCALL(void, setLengthStrV2)(NimStringV2* s_p0, NI newLen_p1);
static N_INLINE(void, nimAddCharV1)(NimStringV2* s_p0, NIM_CHAR c_p1);
N_LIB_PRIVATE N_NIMCALL(void, prepareAdd)(NimStringV2* s_p0, NI addLen_p1);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, readLine__stdZsyncio_u288)(FILE* f_p0, NimStringV2* line_p1);
static N_INLINE(void, appendString)(NimStringV2* dest_p0, NimStringV2 src_p1);
N_LIB_PRIVATE N_NIMCALL(void, write__stdZsyncio_u253)(FILE* f_p0, NimStringV2 s_p1);
N_LIB_PRIVATE N_NIMCALL(NI, writeBuffer__stdZsyncio_u195)(FILE* f_p0, void* buffer_p1, NI len_p2);
N_LIB_PRIVATE N_NIMCALL(void, llStreamWrite__llstream_u329)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* s_p0, NimStringV2 data_p1);
static N_INLINE(void, nimZeroMem)(void* p_p0, NI size_p1);
static N_INLINE(void, nimSetMem__systemZmemory_u7)(void* a_p0, int v_p1, NI size_p2);
N_LIB_PRIVATE N_NIMCALL(void, eqdup___llstream_u159)(tyProc__WBr0JFZ9bbIGwzgqNYcBJtQ src_p0, NIM_BOOL cyclic_p1, tyProc__WBr0JFZ9bbIGwzgqNYcBJtQ* Result);
N_LIB_PRIVATE N_NIMCALL(void, eqdup___llstream_u191)(tyProc__HzVCwACFYM9cx9aV62PdjtuA src_p0, NIM_BOOL cyclic_p1, tyProc__HzVCwACFYM9cx9aV62PdjtuA* Result);
N_LIB_PRIVATE N_NIMCALL(void, eqsink___system_u2606)(NimStringV2* dest_p0, NimStringV2 src_p1);
N_NIMCALL(NimStringV2, rawNewString)(NI cap_p0);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, readLineFromStdin__llstream_u215)(NimStringV2 prompt_p0, NimStringV2* line_p1);
N_LIB_PRIVATE N_NIMCALL(void, flushFile__stdZsyncio_u275)(FILE* f_p0);
static N_INLINE(void, quit__system_u7726)(NI errorcode_p0);
N_LIB_PRIVATE N_NIMCALL(NI, countTriples__llstream_u246)(NimStringV2 s_p0);
static N_INLINE(NIM_BOOL, continueLine__llstream_u238)(NimStringV2 line_p0, NIM_BOOL inTripleString_p1);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, endsWith__llstream_u219)(NimStringV2 x_p0, tySet_tyChar__nmiMWKVIe46vacnhAFrQvw s_p1);
N_LIB_PRIVATE TNimTypeV2 NTIv2__FoDsO8lsQbcCVaLehnuOYg_;
static NIM_CONST tyProc__HzVCwACFYM9cx9aV62PdjtuA TM__ml9aueblxGz9aYsRLKvPmpow_3 = {NIM_NIL,NIM_NIL};
static const struct {
  NI cap; NIM_CHAR data[1+1];
} TM__ml9aueblxGz9aYsRLKvPmpow_6 = { 1 | NIM_STRLIT_FLAG, "\012" };
static const NimStringV2 TM__ml9aueblxGz9aYsRLKvPmpow_7 = {1, (NimStrPayload*)&TM__ml9aueblxGz9aYsRLKvPmpow_6};
static const struct {
  NI cap; NIM_CHAR data[0+1];
} TM__ml9aueblxGz9aYsRLKvPmpow_8 = { 0 | NIM_STRLIT_FLAG, "" };
static const NimStringV2 TM__ml9aueblxGz9aYsRLKvPmpow_9 = {0, (NimStrPayload*)&TM__ml9aueblxGz9aYsRLKvPmpow_8};
static const NimStringV2 TM__ml9aueblxGz9aYsRLKvPmpow_10 = {0, (NimStrPayload*)&TM__ml9aueblxGz9aYsRLKvPmpow_8};
static const NimStringV2 TM__ml9aueblxGz9aYsRLKvPmpow_11 = {1, (NimStrPayload*)&TM__ml9aueblxGz9aYsRLKvPmpow_6};
static const struct {
  NI cap; NIM_CHAR data[4+1];
} TM__ml9aueblxGz9aYsRLKvPmpow_12 = { 4 | NIM_STRLIT_FLAG, ">>> " };
static const NimStringV2 TM__ml9aueblxGz9aYsRLKvPmpow_13 = {4, (NimStrPayload*)&TM__ml9aueblxGz9aYsRLKvPmpow_12};
static const struct {
  NI cap; NIM_CHAR data[4+1];
} TM__ml9aueblxGz9aYsRLKvPmpow_14 = { 4 | NIM_STRLIT_FLAG, "... " };
static const NimStringV2 TM__ml9aueblxGz9aYsRLKvPmpow_15 = {4, (NimStrPayload*)&TM__ml9aueblxGz9aYsRLKvPmpow_14};
static const NimStringV2 TM__ml9aueblxGz9aYsRLKvPmpow_16 = {1, (NimStrPayload*)&TM__ml9aueblxGz9aYsRLKvPmpow_6};
static NIM_CONST tySet_tyChar__nmiMWKVIe46vacnhAFrQvw TM__ml9aueblxGz9aYsRLKvPmpow_17 = {
0x00, 0x00, 0x00, 0x00, 0x7a, 0xbc, 0x00, 0xf4,
0x01, 0x00, 0x00, 0x50, 0x00, 0x00, 0x00, 0x50,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
;
extern NIM_BOOL nimInErrorMode__system_u4310;
static NIM_CONST NU32 TM__ml9aueblxGz9aYsRLKvPmpow_2[2] = {3701606400, 2162965248};
N_LIB_PRIVATE TNimTypeV2 NTIv2__FoDsO8lsQbcCVaLehnuOYg_ = {.destructor = (void*)rttiDestroy__llstream_u369, .size = sizeof(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg), .align = (NI16) NIM_ALIGNOF(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg), .depth = 1, .display = TM__ml9aueblxGz9aYsRLKvPmpow_2, .traceImpl = (void*)eqtrace___llstream_u68, .flags = 0};
static N_INLINE(NI, minuspercent___system_u799)(NI x_p0, NI y_p1) {
	NI result;
	result = ((NI) ((NU)((NU32)(((NU) (x_p0))) - (NU32)(((NU) (y_p1))))));
	return result;
}
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void) {
	NIM_BOOL* result;
	result = (&nimInErrorMode__system_u4310);
	return result;
}
static N_INLINE(NIM_BOOL, nimDecRefIsLastCyclicDyn)(void* p_p0) {
	NIM_BOOL result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NIM_BOOL)0;
	{
		tyObject_RefHeader__GDnGTjNyvwISJbkIvAst3A* cell;
		NI T5_;
		if (!!((p_p0 == NIM_NIL))) goto LA3_;
		T5_ = (NI)0;
		T5_ = minuspercent___system_u799(((NI) (ptrdiff_t) (p_p0)), ((NI)8));
		cell = ((tyObject_RefHeader__GDnGTjNyvwISJbkIvAst3A*) (T5_));
		{
			if (!((NI)((*cell).rc & ((NI)-16)) == ((NI)0))) goto LA8_;
			result = NIM_TRUE;
		}
		goto LA6_;
LA8_: ;
		{
			(*cell).rc -= ((NI)16);
		}
LA6_: ;
		rememberCycle__system_u3476(result, cell, (*((TNimTypeV2**) (p_p0))));
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
LA3_: ;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___llstream_u56)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* dest_p0) {
	eqdestroy___system_u2597((*dest_p0).s);
	{
		NIM_BOOL T3_;
		T3_ = (NIM_BOOL)0;
		T3_ = nimDecRefIsLastCyclicDyn((*dest_p0).repl.ClE_0);
		if (!T3_) goto LA4_;
		nimDestroyAndDispose((*dest_p0).repl.ClE_0);
	}
LA4_: ;
	{
		NIM_BOOL T8_;
		T8_ = (NIM_BOOL)0;
		T8_ = nimDecRefIsLastCyclicDyn((*dest_p0).onPrompt.ClE_0);
		if (!T8_) goto LA9_;
		nimDestroyAndDispose((*dest_p0).onPrompt.ClE_0);
	}
LA9_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, rttiDestroy__llstream_u369)(void* dest_p0) {
	eqdestroy___llstream_u56((&(*((tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg*) (dest_p0)))));
}
static N_INLINE(void, add__system_u2959)(tyObject_CellSeq__gW9aeWLFbrNaeUKXqxYBXhg* s_p0, void** c_p1, TNimTypeV2* t_p2) {
	void** colontmp_;
	TNimTypeV2* colontmp__2;
	{
		if (!((*s_p0).cap <= (*s_p0).len)) goto LA3_;
		resize__system_u2967(s_p0);
	}
LA3_: ;
	colontmp_ = c_p1;
	colontmp__2 = t_p2;
	(*s_p0).d[(*s_p0).len].Field0 = colontmp_;
	(*s_p0).d[(*s_p0).len].Field1 = colontmp__2;
	(*s_p0).len += ((NI)1);
}
static N_INLINE(void, nimTraceRefDyn)(void* q_p0, void* env_p1) {
	void** p;
	p = ((void**) (q_p0));
	{
		tyObject_GcEnv__HccVszwojlCZRN46BE9cG8w* j;
		if (!!(((*p) == NIM_NIL))) goto LA3_;
		j = ((tyObject_GcEnv__HccVszwojlCZRN46BE9cG8w*) (env_p1));
		add__system_u2959((&(*j).traceStack), p, (*((TNimTypeV2**) ((*p)))));
	}
LA3_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, eqtrace___llstream_u68)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* dest_p0, void* env_p1) {
	nimTraceRefDyn((&(*dest_p0).repl.ClE_0), env_p1);
	nimTraceRefDyn((&(*dest_p0).onPrompt.ClE_0), env_p1);
}
N_LIB_PRIVATE N_NIMCALL(void, eqsink___llstream_u122)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg** dest_p0, tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* src_p1) {
	tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* colontmp_;
	colontmp_ = (*dest_p0);
	(*dest_p0) = src_p1;
	{
		NIM_BOOL T3_;
		T3_ = (NIM_BOOL)0;
		T3_ = nimDecRefIsLastCyclicDyn(colontmp_);
		if (!T3_) goto LA4_;
		nimDestroyAndDispose(colontmp_);
	}
LA4_: ;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg*, llStreamOpen__llstream_u135)(NimStringV2 filename_p0, tyEnum_FileMode__YkEVJbjDa9aPvYTBr8yiUWg mode_p1) {
	tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* result;
	tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* T1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	T1_ = NIM_NIL;
	T1_ = (tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg*) nimNewObj(sizeof(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg), NIM_ALIGNOF(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg));
	(*T1_).Sup.m_type = (&NTIv2__FoDsO8lsQbcCVaLehnuOYg_);
	(*T1_).kind = ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)2);
	result = T1_;
	{
		NIM_BOOL T4_;
		T4_ = (NIM_BOOL)0;
		T4_ = open__stdZsyncio_u417(&(*result).f, filename_p0, mode_p1, ((NI)-1));
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		if (!!(T4_)) goto LA5_;
		eqsink___llstream_u122(&result, ((tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg*) NIM_NIL));
	}
LA5_: ;
	}BeforeRet_: ;
	return result;
}
static N_INLINE(void, nimIncRefCyclic)(void* p_p0, NIM_BOOL cyclic_p1) {
	tyObject_RefHeader__GDnGTjNyvwISJbkIvAst3A* h;
	NI T1_;
	T1_ = (NI)0;
	T1_ = minuspercent___system_u799(((NI) (ptrdiff_t) (p_p0)), ((NI)8));
	h = ((tyObject_RefHeader__GDnGTjNyvwISJbkIvAst3A*) (T1_));
	(*h).rc += ((NI)16);
}
N_LIB_PRIVATE N_NIMCALL(void, eqcopy___llstream_u114)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg** dest_p0, tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* src_p1, NIM_BOOL cyclic_p2) {
	tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* colontmp_;
	colontmp_ = (*dest_p0);
	{
		if (!src_p1) goto LA3_;
		nimIncRefCyclic(src_p1, cyclic_p2);
	}
LA3_: ;
	(*dest_p0) = src_p1;
	{
		NIM_BOOL T7_;
		T7_ = (NIM_BOOL)0;
		T7_ = nimDecRefIsLastCyclicDyn(colontmp_);
		if (!T7_) goto LA8_;
		nimDestroyAndDispose(colontmp_);
	}
LA8_: ;
}
static N_INLINE(void, nimCopyMem)(void* dest_p0, void* source_p1, NI size_p2) {
	void* T1_;
	T1_ = (void*)0;
	T1_ = memcpy(dest_p0, source_p1, ((size_t) (size_p2)));
}
static N_INLINE(void, copyMem__system_u1713)(void* dest_p0, void* source_p1, NI size_p2) {
	nimCopyMem(dest_p0, source_p1, size_p2);
}
N_LIB_PRIVATE N_NIMCALL(NI, llStreamRead__llstream_u287)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* s_p0, void* buf_p1, NI bufLen_p2) {
	NI result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NI)0;
	switch ((*s_p0).kind) {
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)0):
	{
		result = ((NI)0);
	}
	break;
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)1):
	{
		result = ((bufLen_p2 <= (NI)((*s_p0).s.len - (*s_p0).rd)) ? bufLen_p2 : (NI)((*s_p0).s.len - (*s_p0).rd));
		{
			if (!(((NI)0) < result)) goto LA5_;
			copyMem__system_u1713(buf_p1, ((void*) ((&(*s_p0).s.p->data[(NI)(((NI)0) + (*s_p0).rd)]))), (result));
			(*s_p0).rd += result;
		}
LA5_: ;
	}
	break;
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)2):
	{
		result = readBuffer__stdZsyncio_u164((*s_p0).f, buf_p1, (bufLen_p2));
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
	break;
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)3):
	{
		{
			if (!!(((*s_p0).onPrompt.ClP_0 == TM__ml9aueblxGz9aYsRLKvPmpow_3.ClP_0 && (*s_p0).onPrompt.ClE_0 == TM__ml9aueblxGz9aYsRLKvPmpow_3.ClE_0))) goto LA11_;
			(*s_p0).onPrompt.ClE_0? (*s_p0).onPrompt.ClP_0((*s_p0).onPrompt.ClE_0):((TM__ml9aueblxGz9aYsRLKvPmpow_4)((*s_p0).onPrompt.ClP_0))();
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		}
LA11_: ;
		result = (*s_p0).repl.ClE_0? (*s_p0).repl.ClP_0(s_p0, buf_p1, bufLen_p2, (*s_p0).repl.ClE_0):((TM__ml9aueblxGz9aYsRLKvPmpow_5)((*s_p0).repl.ClP_0))(s_p0, buf_p1, bufLen_p2);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
	break;
	default: __builtin_unreachable();
	}
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, llStreamClose__llstream_u213)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* s_p0) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	switch ((*s_p0).kind) {
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)0):
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)1):
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)3):
	{
	}
	break;
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)2):
	{
		close__stdZsyncio_u266((*s_p0).f);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
	break;
	default: __builtin_unreachable();
	}
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___llstream_u111)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* dest_p0) {
	{
		NIM_BOOL T3_;
		T3_ = (NIM_BOOL)0;
		T3_ = nimDecRefIsLastCyclicDyn(dest_p0);
		if (!T3_) goto LA4_;
		nimDestroyAndDispose(dest_p0);
	}
LA4_: ;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg*, llStreamOpen__llstream_u132)(FILE* f_p0) {
	tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* result;
	FILE* colontmpD_;
	tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* T1_;
	colontmpD_ = (FILE*)0;
	T1_ = NIM_NIL;
	T1_ = (tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg*) nimNewObj(sizeof(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg), NIM_ALIGNOF(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg));
	(*T1_).Sup.m_type = (&NTIv2__FoDsO8lsQbcCVaLehnuOYg_);
	(*T1_).kind = ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)2);
	colontmpD_ = f_p0;
	(*T1_).f = colontmpD_;
	result = T1_;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg*, llStreamOpen__llstream_u31)(NimStringV2 data_p0) {
	tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* result;
	tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* T1_;
	NimStringV2 blitTmp;
	T1_ = NIM_NIL;
	T1_ = (tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg*) nimNewObj(sizeof(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg), NIM_ALIGNOF(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg));
	(*T1_).Sup.m_type = (&NTIv2__FoDsO8lsQbcCVaLehnuOYg_);
	(*T1_).kind = ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)1);
	blitTmp = data_p0;
	(*T1_).s = blitTmp;
	result = T1_;
	return result;
}
static N_INLINE(void, nimAddCharV1)(NimStringV2* s_p0, NIM_CHAR c_p1) {
	prepareAdd(s_p0, ((NI)1));
	(*(*s_p0).p).data[(*s_p0).len] = c_p1;
	(*s_p0).len += ((NI)1);
	(*(*s_p0).p).data[(*s_p0).len] = 0;
}
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, llStreamReadLine__llstream_u305)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* s_p0, NimStringV2* line_p1) {
	NIM_BOOL result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NIM_BOOL)0;
	setLengthStrV2((&(*line_p1)), ((NI)0));
	switch ((*s_p0).kind) {
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)0):
	{
		result = NIM_TRUE;
	}
	break;
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)1):
	{
		NIM_BOOL T12_;
		{
			while (1) {
				if (!((*s_p0).rd < (*s_p0).s.len)) goto LA4;
				switch (((NU8)((*s_p0).s.p->data[(*s_p0).rd]))) {
				case 13:
				{
					(*s_p0).rd += ((NI)1);
					{
						if (!((NU8)((*s_p0).s.p->data[(*s_p0).rd]) == (NU8)(10))) goto LA8_;
						(*s_p0).rd += ((NI)1);
					}
LA8_: ;
					goto LA3;
				}
				break;
				case 10:
				{
					(*s_p0).rd += ((NI)1);
					goto LA3;
				}
				break;
				default:
				{
					nimAddCharV1((&(*line_p1)), (*s_p0).s.p->data[(*s_p0).rd]);
					(*s_p0).rd += ((NI)1);
				}
				break;
				}
			} LA4: ;
		} LA3: ;
		T12_ = (NIM_BOOL)0;
		T12_ = (((NI)0) < (*line_p1).len);
		if (T12_) goto LA13_;
		T12_ = ((*s_p0).rd < (*s_p0).s.len);
LA13_: ;
		result = T12_;
	}
	break;
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)2):
	{
		result = readLine__stdZsyncio_u288((*s_p0).f, line_p1);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
	break;
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)3):
	{
		result = readLine__stdZsyncio_u288(stdin, line_p1);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
	break;
	default: __builtin_unreachable();
	}
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg*, eqdup___llstream_u118)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* src_p0, NIM_BOOL cyclic_p1) {
	tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* result;
	tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* colontmp_;
	result = NIM_NIL;
	colontmp_ = result;
	result = src_p0;
	{
		if (!src_p0) goto LA3_;
		nimIncRefCyclic(src_p0, cyclic_p1);
	}
LA3_: ;
	return result;
}
static N_INLINE(void, appendString)(NimStringV2* dest_p0, NimStringV2 src_p1) {
	{
		if (!(((NI)0) < src_p1.len)) goto LA3_;
		copyMem__system_u1713(((void*) ((&(*(*dest_p0).p).data[(*dest_p0).len]))), ((void*) ((&(*src_p1.p).data[((NI)0)]))), ((NI)(src_p1.len + ((NI)1))));
		(*dest_p0).len += src_p1.len;
	}
LA3_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, llStreamWrite__llstream_u329)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* s_p0, NimStringV2 data_p1) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	switch ((*s_p0).kind) {
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)0):
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)3):
	{
	}
	break;
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)1):
	{
		prepareAdd((&(*s_p0).s), data_p1.len + 0);
appendString((&(*s_p0).s), data_p1);
		(*s_p0).wr += data_p1.len;
	}
	break;
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)2):
	{
		write__stdZsyncio_u253((*s_p0).f, data_p1);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
	break;
	default: __builtin_unreachable();
	}
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, endsWith__llstream_u219)(NimStringV2 x_p0, tySet_tyChar__nmiMWKVIe46vacnhAFrQvw s_p1) {
	NIM_BOOL result;
	NI i;
	i = (NI)(x_p0.len - ((NI)1));
	{
		while (1) {
			NIM_BOOL T3_;
			T3_ = (NIM_BOOL)0;
			T3_ = (((NI)0) <= i);
			if (!(T3_)) goto LA4_;
			T3_ = ((NU8)(x_p0.p->data[i]) == (NU8)(32));
LA4_: ;
			if (!T3_) goto LA2;
			i -= ((NI)1);
		} LA2: ;
	}
	{
		NIM_BOOL T7_;
		T7_ = (NIM_BOOL)0;
		T7_ = (((NI)0) <= i);
		if (!(T7_)) goto LA8_;
		T7_ = ((s_p1[(NU)((((NU8)(x_p0.p->data[i]))))>>3] &(1U<<((NU)((((NU8)(x_p0.p->data[i]))))&7U)))!=0);
LA8_: ;
		if (!T7_) goto LA9_;
		result = NIM_TRUE;
	}
	goto LA5_;
LA9_: ;
	{
		result = NIM_FALSE;
	}
LA5_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, llStreamWrite__llstream_u340)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* s_p0, NIM_CHAR data_p1) {
	NIM_CHAR c;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	c = (NIM_CHAR)0;
	switch ((*s_p0).kind) {
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)0):
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)3):
	{
	}
	break;
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)1):
	{
		nimAddCharV1((&(*s_p0).s), data_p1);
		(*s_p0).wr += ((NI)1);
	}
	break;
	case ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)2):
	{
		NI T4_;
		c = data_p1;
		T4_ = (NI)0;
		T4_ = writeBuffer__stdZsyncio_u195((*s_p0).f, ((void*) ((&c))), ((NI)1));
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		(void)(T4_);
	}
	break;
	default: __builtin_unreachable();
	}
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, eqwasMoved___llstream_u108)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg** dest_p0) {
	(*dest_p0) = 0;
}
N_LIB_PRIVATE N_NIMCALL(void, llStreamWriteln__llstream_u337)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* s_p0, NimStringV2 data_p1) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	llStreamWrite__llstream_u329(s_p0, data_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	llStreamWrite__llstream_u329(s_p0, TM__ml9aueblxGz9aYsRLKvPmpow_7);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
static N_INLINE(void, nimSetMem__systemZmemory_u7)(void* a_p0, int v_p1, NI size_p2) {
	void* T1_;
	T1_ = (void*)0;
	T1_ = memset(a_p0, v_p1, ((size_t) (size_p2)));
}
static N_INLINE(void, nimZeroMem)(void* p_p0, NI size_p1) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	nimSetMem__systemZmemory_u7(p_p0, ((int)0), size_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, eqdup___llstream_u159)(tyProc__WBr0JFZ9bbIGwzgqNYcBJtQ src_p0, NIM_BOOL cyclic_p1, tyProc__WBr0JFZ9bbIGwzgqNYcBJtQ* Result) {
	void* colontmp_;
	nimZeroMem((void*)Result, sizeof(tyProc__WBr0JFZ9bbIGwzgqNYcBJtQ));
	colontmp_ = (*Result).ClE_0;
	(*Result).ClE_0 = src_p0.ClE_0;
	(*Result).ClP_0 = src_p0.ClP_0;
	{
		if (!src_p0.ClE_0) goto LA3_;
		nimIncRefCyclic(src_p0.ClE_0, cyclic_p1);
	}
LA3_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, eqdup___llstream_u191)(tyProc__HzVCwACFYM9cx9aV62PdjtuA src_p0, NIM_BOOL cyclic_p1, tyProc__HzVCwACFYM9cx9aV62PdjtuA* Result) {
	void* colontmp_;
	nimZeroMem((void*)Result, sizeof(tyProc__HzVCwACFYM9cx9aV62PdjtuA));
	colontmp_ = (*Result).ClE_0;
	(*Result).ClE_0 = src_p0.ClE_0;
	(*Result).ClP_0 = src_p0.ClP_0;
	{
		if (!src_p0.ClE_0) goto LA3_;
		nimIncRefCyclic(src_p0.ClE_0, cyclic_p1);
	}
LA3_: ;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg*, llStreamOpenStdIn__llstream_u145)(tyProc__WBr0JFZ9bbIGwzgqNYcBJtQ r_p0, tyProc__HzVCwACFYM9cx9aV62PdjtuA onPrompt_p1) {
	tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* result;
	tyProc__WBr0JFZ9bbIGwzgqNYcBJtQ colontmpD_;
	tyProc__HzVCwACFYM9cx9aV62PdjtuA colontmpD__2;
	tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* T1_;
	nimZeroMem((void*)(&colontmpD_), sizeof(tyProc__WBr0JFZ9bbIGwzgqNYcBJtQ));
	nimZeroMem((void*)(&colontmpD__2), sizeof(tyProc__HzVCwACFYM9cx9aV62PdjtuA));
	T1_ = NIM_NIL;
	T1_ = (tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg*) nimNewObj(sizeof(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg), NIM_ALIGNOF(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg));
	(*T1_).Sup.m_type = (&NTIv2__FoDsO8lsQbcCVaLehnuOYg_);
	(*T1_).kind = ((tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ)3);
	(*T1_).s = TM__ml9aueblxGz9aYsRLKvPmpow_9;
	(*T1_).lineOffset = ((NI)-1);
	eqdup___llstream_u159(r_p0, NIM_TRUE, (&colontmpD_));
	(*T1_).repl.ClE_0 = colontmpD_.ClE_0;
	(*T1_).repl.ClP_0 = colontmpD_.ClP_0;
	eqdup___llstream_u191(onPrompt_p1, NIM_TRUE, (&colontmpD__2));
	(*T1_).onPrompt.ClE_0 = colontmpD__2.ClE_0;
	(*T1_).onPrompt.ClP_0 = colontmpD__2.ClP_0;
	result = T1_;
	return result;
}
static N_INLINE(void, quit__system_u7726)(NI errorcode_p0) {
	{
		if (!(errorcode_p0 < ((NI)-128))) goto LA3_;
		exit(((int)-128));
	}
	goto LA1_;
LA3_: ;
	{
		if (!(((NI)127) < errorcode_p0)) goto LA6_;
		exit(((int)127));
	}
	goto LA1_;
LA6_: ;
	{
		exit(((int) (errorcode_p0)));
	}
LA1_: ;
}
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, readLineFromStdin__llstream_u215)(NimStringV2 prompt_p0, NimStringV2* line_p1) {
	NIM_BOOL result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NIM_BOOL)0;
	write__stdZsyncio_u253(stdout, prompt_p0);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	flushFile__stdZsyncio_u275(stdout);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	result = readLine__stdZsyncio_u288(stdin, line_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	{
		if (!!(result)) goto LA3_;
		write__stdZsyncio_u253(stdout, TM__ml9aueblxGz9aYsRLKvPmpow_11);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		quit__system_u7726(((NI)0));
	}
LA3_: ;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NI, countTriples__llstream_u246)(NimStringV2 s_p0) {
	NI result;
	NI i;
	result = ((NI)0);
	i = ((NI)0);
	{
		while (1) {
			if (!((NI)(i + ((NI)2)) < s_p0.len)) goto LA2;
			{
				NIM_BOOL T5_;
				NIM_BOOL T6_;
				T5_ = (NIM_BOOL)0;
				T6_ = (NIM_BOOL)0;
				T6_ = ((NU8)(s_p0.p->data[i]) == (NU8)(34));
				if (!(T6_)) goto LA7_;
				T6_ = ((NU8)(s_p0.p->data[(NI)(i + ((NI)1))]) == (NU8)(34));
LA7_: ;
				T5_ = T6_;
				if (!(T5_)) goto LA8_;
				T5_ = ((NU8)(s_p0.p->data[(NI)(i + ((NI)2))]) == (NU8)(34));
LA8_: ;
				if (!T5_) goto LA9_;
				result += ((NI)1);
				i += ((NI)2);
			}
LA9_: ;
			i += ((NI)1);
		} LA2: ;
	}
	return result;
}
static N_INLINE(NIM_BOOL, continueLine__llstream_u238)(NimStringV2 line_p0, NIM_BOOL inTripleString_p1) {
	NIM_BOOL result;
	NIM_BOOL T1_;
	NIM_BOOL T3_;
	NIM_BOOL T5_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NIM_BOOL)0;
	T1_ = (NIM_BOOL)0;
	T1_ = inTripleString_p1;
	if (T1_) goto LA2_;
	T3_ = (NIM_BOOL)0;
	T3_ = (((NI)0) < line_p0.len);
	if (!(T3_)) goto LA4_;
	T5_ = (NIM_BOOL)0;
	T5_ = ((NU8)(line_p0.p->data[((NI)0)]) == (NU8)(32));
	if (T5_) goto LA6_;
	T5_ = endsWith__llstream_u219(line_p0, TM__ml9aueblxGz9aYsRLKvPmpow_17);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
LA6_: ;
	T3_ = T5_;
LA4_: ;
	T1_ = T3_;
LA2_: ;
	result = T1_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NI, llReadFromStdin__llstream_u141)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* s_p0, void* buf_p1, NI bufLen_p2) {
	NI result;
	NimStringV2 line;
	NI triples;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NI)0;
	line.len = 0; line.p = NIM_NIL;
	eqsink___system_u2606((&(*s_p0).s), TM__ml9aueblxGz9aYsRLKvPmpow_10);
	(*s_p0).rd = ((NI)0);
	line = rawNewString(((NI)120));
	triples = ((NI)0);
	{
		NimStringV2 colontmpD_;
		NimStringV2 colontmpD__2;
		colontmpD_.len = 0; colontmpD_.p = NIM_NIL;
		colontmpD__2.len = 0; colontmpD__2.p = NIM_NIL;
		while (1) {
			NimStringV2 T5_;
			NIM_BOOL T11_;
			NI T12_;
			T5_.len = 0; T5_.p = NIM_NIL;
			{
				if (!((*s_p0).s.len == ((NI)0))) goto LA8_;
				eqsink___system_u2606((&colontmpD_), TM__ml9aueblxGz9aYsRLKvPmpow_13);
				T5_ = colontmpD_;
			}
			goto LA6_;
LA8_: ;
			{
				eqsink___system_u2606((&colontmpD__2), TM__ml9aueblxGz9aYsRLKvPmpow_15);
				T5_ = colontmpD__2;
			}
LA6_: ;
			T11_ = (NIM_BOOL)0;
			T11_ = readLineFromStdin__llstream_u215(T5_, (&line));
			if (NIM_UNLIKELY(*nimErr_)) goto LA3_;
			if (!T11_) goto LA4;
			prepareAdd((&(*s_p0).s), line.len + 0);
appendString((&(*s_p0).s), line);
			prepareAdd((&(*s_p0).s), 1);
appendString((&(*s_p0).s), TM__ml9aueblxGz9aYsRLKvPmpow_16);
			T12_ = (NI)0;
			T12_ = countTriples__llstream_u246(line);
			if (NIM_UNLIKELY(*nimErr_)) goto LA3_;
			triples += T12_;
			{
				NIM_BOOL T15_;
				T15_ = (NIM_BOOL)0;
				T15_ = continueLine__llstream_u238(line, ((NI)(triples & ((NI)1)) == ((NI)1)));
				if (NIM_UNLIKELY(*nimErr_)) goto LA3_;
				if (!!(T15_)) goto LA16_;
				eqdestroy___system_u2597(colontmpD__2);
				eqdestroy___system_u2597(colontmpD_);
				goto LA2;
			}
LA16_: ;
		} LA4: ;
		{
			LA3_:;
		}
		{
			eqdestroy___system_u2597(colontmpD__2);
			eqdestroy___system_u2597(colontmpD_);
		}
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	} LA2: ;
	(*s_p0).lineOffset += ((NI)1);
	result = ((bufLen_p2 <= (NI)((*s_p0).s.len - (*s_p0).rd)) ? bufLen_p2 : (NI)((*s_p0).s.len - (*s_p0).rd));
	{
		if (!(((NI)0) < result)) goto LA22_;
		copyMem__system_u1713(buf_p1, ((void*) ((&(*s_p0).s.p->data[(*s_p0).rd]))), (result));
		(*s_p0).rd += result;
	}
LA22_: ;
	{
		LA1_:;
	}
	{
		eqdestroy___system_u2597(line);
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
