/* Generated by Nim Compiler v2.2.0 */
#define NIM_INTBITS 64

#include "nimbase.h"
#include <string.h>
#undef LANGUAGE_C
#undef MIPSEB
#undef MIPSEL
#undef PPC
#undef R3000
#undef R4000
#undef i386
#undef linux
#undef mips
#undef near
#undef far
#undef powerpc
#undef unix
typedef struct tyObject_TCtx__FHLmhXedV9bg0OyLunsHRew tyObject_TCtx__FHLmhXedV9bg0OyLunsHRew;
typedef struct tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA;
typedef struct NimStrPayload NimStrPayload;
typedef struct NimStringV2 NimStringV2;
typedef struct tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ;
typedef struct tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg;
typedef struct tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg;
typedef struct tyObject_TIdent__LG4CHFyohryWo9bzZeyjDjA tyObject_TIdent__LG4CHFyohryWo9bzZeyjDjA;
typedef struct tySequence__9ahcSjIErJohSBBFLwuF56Q tySequence__9ahcSjIErJohSBBFLwuF56Q;
typedef struct tySequence__9ahcSjIErJohSBBFLwuF56Q_Content tySequence__9ahcSjIErJohSBBFLwuF56Q_Content;
typedef struct tySequence__fnH5Zt4fsGx0AgDTOw8pKw tySequence__fnH5Zt4fsGx0AgDTOw8pKw;
typedef struct tySequence__fnH5Zt4fsGx0AgDTOw8pKw_Content tySequence__fnH5Zt4fsGx0AgDTOw8pKw_Content;
typedef struct tyTuple__ZQn9aE2eP9aC9aAInis8k8aDQ tyTuple__ZQn9aE2eP9aC9aAInis8k8aDQ;
typedef struct tyObject_TPassContext__QQOybmyOwFn29csyFzTSLNQ tyObject_TPassContext__QQOybmyOwFn29csyFzTSLNQ;
typedef struct RootObj RootObj;
typedef struct TNimTypeV2 TNimTypeV2;
typedef struct tyObject_IdGeneratorcolonObjectType___G9bSmxY5z1At9bXNKpE9bKuug tyObject_IdGeneratorcolonObjectType___G9bSmxY5z1At9bXNKpE9bKuug;
typedef struct tySequence__YdLNCDKYeipzJx3I8Xw82Q tySequence__YdLNCDKYeipzJx3I8Xw82Q;
typedef struct tySequence__YdLNCDKYeipzJx3I8Xw82Q_Content tySequence__YdLNCDKYeipzJx3I8Xw82Q_Content;
typedef struct tySequence__TLbZ2GqL3Zio1f8bNpj2NA tySequence__TLbZ2GqL3Zio1f8bNpj2NA;
typedef struct tySequence__TLbZ2GqL3Zio1f8bNpj2NA_Content tySequence__TLbZ2GqL3Zio1f8bNpj2NA_Content;
typedef struct tySequence__d8ZV4IMgHt9biik9cS9a0hrmg tySequence__d8ZV4IMgHt9biik9cS9a0hrmg;
typedef struct tySequence__d8ZV4IMgHt9biik9cS9a0hrmg_Content tySequence__d8ZV4IMgHt9biik9cS9a0hrmg_Content;
typedef struct tyObject_PProccolonObjectType___BUXHs9a6Ck9awUko3NbJ3Fyg tyObject_PProccolonObjectType___BUXHs9a6Ck9awUko3NbJ3Fyg;
typedef struct tySequence__TEgqV1E9aHqnwBUoeXjBI9aA tySequence__TEgqV1E9aHqnwBUoeXjBI9aA;
typedef struct tySequence__TEgqV1E9aHqnwBUoeXjBI9aA_Content tySequence__TEgqV1E9aHqnwBUoeXjBI9aA_Content;
typedef struct tyObject_Table__tro5f9chxPVpES1mQpDwZtQ tyObject_Table__tro5f9chxPVpES1mQpDwZtQ;
typedef struct tySequence__pUxI6Ljo6IKKHXKOZkQ7Og tySequence__pUxI6Ljo6IKKHXKOZkQ7Og;
typedef struct tySequence__pUxI6Ljo6IKKHXKOZkQ7Og_Content tySequence__pUxI6Ljo6IKKHXKOZkQ7Og_Content;
typedef struct tyObject_IdentCachecolonObjectType___ieLwK4T49bbUIZTTVH9cycZA tyObject_IdentCachecolonObjectType___ieLwK4T49bbUIZTTVH9cycZA;
typedef struct tyObject_ConfigRefcolonObjectType___S9atckdgEHYT7qfQK9cvzJOg tyObject_ConfigRefcolonObjectType___S9atckdgEHYT7qfQK9cvzJOg;
typedef struct tyObject_ModuleGraphcolonObjectType___TuJBn0NOJllZfOUYECIyKw tyObject_ModuleGraphcolonObjectType___TuJBn0NOJllZfOUYECIyKw;
typedef struct tyObject_Profiler__YI4zs2CaXXndlYireD0I8A tyObject_Profiler__YI4zs2CaXXndlYireD0I8A;
typedef struct tyObject_TStackFrame__a20zU43j9cYHpYOOr1c2hZQ tyObject_TStackFrame__a20zU43j9cYHpYOOr1c2hZQ;
typedef struct tyObject_Table__nBRoNCTSfpfbfAeK9bNW1qw tyObject_Table__nBRoNCTSfpfbfAeK9bNW1qw;
typedef struct tySequence__E8pi9b5QNahsURYzXMjh3qw tySequence__E8pi9b5QNahsURYzXMjh3qw;
typedef struct tySequence__E8pi9b5QNahsURYzXMjh3qw_Content tySequence__E8pi9b5QNahsURYzXMjh3qw_Content;
typedef struct tyObject_ItemId__N3z7uIvKCrk0S9cej5ew0nA tyObject_ItemId__N3z7uIvKCrk0S9cej5ew0nA;
typedef struct tyObject_TLoc__KvQVlvKb6mbV567NQ0hMOw tyObject_TLoc__KvQVlvKb6mbV567NQ0hMOw;
typedef struct tyObject_TLib__sVuuRvQkiYNOUiPRZhmZig tyObject_TLib__sVuuRvQkiYNOUiPRZhmZig;
typedef struct tyObject_VmArgs__PuiloXB3J4yNffJquqRVvg tyObject_VmArgs__PuiloXB3J4yNffJquqRVvg;
typedef struct tyTuple__sr3s7uVHpu9cEbbDnCSg8yQ tyTuple__sr3s7uVHpu9cEbbDnCSg8yQ;
typedef struct tyTuple__47w2DboNEPf69aPgubZdd7Q tyTuple__47w2DboNEPf69aPgubZdd7Q;
typedef struct tyObject_TFullReg__kL8NONjyku1Uli8eXEVZ0A tyObject_TFullReg__kL8NONjyku1Uli8eXEVZ0A;
struct tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA {
	NU16 line;
	NI16 col;
	NI32 fileIndex;
};
struct NimStrPayload {
	NI cap;
	NIM_CHAR data[SEQ_DECL_SIZE];
};
struct NimStringV2 {
	NI len;
	NimStrPayload* p;
};
typedef NU32 tySet_tyEnum_TNodeFlag__3UjGgL9bLM4RXBnoZJs9cviw;
typedef NU8 tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw;
struct tySequence__9ahcSjIErJohSBBFLwuF56Q {
  NI len; tySequence__9ahcSjIErJohSBBFLwuF56Q_Content* p;
};
struct tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ {
	tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg* typ;
	tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA info;
	tySet_tyEnum_TNodeFlag__3UjGgL9bLM4RXBnoZJs9cviw flags;
	tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw kind;
union{
	struct {
	NI64 intVal;
} _kind_1;
	struct {
	NF floatVal;
} _kind_2;
	struct {
	NimStringV2 strVal;
} _kind_3;
	struct {
	tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* sym;
} _kind_4;
	struct {
	tyObject_TIdent__LG4CHFyohryWo9bzZeyjDjA* ident;
} _kind_5;
	struct {
	tySequence__9ahcSjIErJohSBBFLwuF56Q sons;
} _kind_6;
};
};
struct tySequence__fnH5Zt4fsGx0AgDTOw8pKw {
  NI len; tySequence__fnH5Zt4fsGx0AgDTOw8pKw_Content* p;
};
struct tyTuple__ZQn9aE2eP9aC9aAInis8k8aDQ {
	tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* Field0;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* Field1;
};
struct TNimTypeV2 {
	void* destructor;
	NI size;
	NI16 align;
	NI16 depth;
	NU32* display;
	void* traceImpl;
	void* typeInfoV1;
	NI flags;
	void* vTable[SEQ_DECL_SIZE];
};
struct RootObj {
	TNimTypeV2* m_type;
};
struct tyObject_TPassContext__QQOybmyOwFn29csyFzTSLNQ {
	RootObj Sup;
	tyObject_IdGeneratorcolonObjectType___G9bSmxY5z1At9bXNKpE9bKuug* idgen;
};
struct tySequence__YdLNCDKYeipzJx3I8Xw82Q {
  NI len; tySequence__YdLNCDKYeipzJx3I8Xw82Q_Content* p;
};
struct tySequence__TLbZ2GqL3Zio1f8bNpj2NA {
  NI len; tySequence__TLbZ2GqL3Zio1f8bNpj2NA_Content* p;
};
struct tySequence__d8ZV4IMgHt9biik9cS9a0hrmg {
  NI len; tySequence__d8ZV4IMgHt9biik9cS9a0hrmg_Content* p;
};
typedef NU8 tyEnum_TEvalMode__sQljSFf0fjnsNV1fpgXakg;
typedef NU8 tySet_tyEnum_TSandboxFlag__II2KpGKWwJH9bdjbBum5d1g;
struct tySequence__TEgqV1E9aHqnwBUoeXjBI9aA {
  NI len; tySequence__TEgqV1E9aHqnwBUoeXjBI9aA_Content* p;
};
struct tySequence__pUxI6Ljo6IKKHXKOZkQ7Og {
  NI len; tySequence__pUxI6Ljo6IKKHXKOZkQ7Og_Content* p;
};
struct tyObject_Table__tro5f9chxPVpES1mQpDwZtQ {
	tySequence__pUxI6Ljo6IKKHXKOZkQ7Og data;
	NI counter;
};
struct tyObject_Profiler__YI4zs2CaXXndlYireD0I8A {
	NF tEnter;
	tyObject_TStackFrame__a20zU43j9cYHpYOOr1c2hZQ* tos;
};
struct tySequence__E8pi9b5QNahsURYzXMjh3qw {
  NI len; tySequence__E8pi9b5QNahsURYzXMjh3qw_Content* p;
};
struct tyObject_Table__nBRoNCTSfpfbfAeK9bNW1qw {
	tySequence__E8pi9b5QNahsURYzXMjh3qw data;
	NI counter;
};
struct tyObject_TCtx__FHLmhXedV9bg0OyLunsHRew {
	tyObject_TPassContext__QQOybmyOwFn29csyFzTSLNQ Sup;
	tySequence__YdLNCDKYeipzJx3I8Xw82Q code;
	tySequence__TLbZ2GqL3Zio1f8bNpj2NA debug;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* globals;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* constants;
	tySequence__d8ZV4IMgHt9biik9cS9a0hrmg types;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* currentExceptionA;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* currentExceptionB;
	NI exceptionInstr;
	tyObject_PProccolonObjectType___BUXHs9a6Ck9awUko3NbJ3Fyg* prc;
	tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* module;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* callsite;
	tyEnum_TEvalMode__sQljSFf0fjnsNV1fpgXakg mode;
	tySet_tyEnum_TSandboxFlag__II2KpGKWwJH9bdjbBum5d1g features;
	NIM_BOOL traceActive;
	NI loopIterations;
	tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA comesFromHeuristic;
	tySequence__TEgqV1E9aHqnwBUoeXjBI9aA callbacks;
	tyObject_Table__tro5f9chxPVpES1mQpDwZtQ callbackIndex;
	NimStringV2 errorFlag;
	tyObject_IdentCachecolonObjectType___ieLwK4T49bbUIZTTVH9cycZA* cache;
	tyObject_ConfigRefcolonObjectType___S9atckdgEHYT7qfQK9cvzJOg* config;
	tyObject_ModuleGraphcolonObjectType___TuJBn0NOJllZfOUYECIyKw* graph;
	NI oldErrorCount;
	tyObject_Profiler__YI4zs2CaXXndlYireD0I8A profiler;
	NI* templInstCounter;
	tySequence__fnH5Zt4fsGx0AgDTOw8pKw vmstateDiff;
	tyObject_Table__nBRoNCTSfpfbfAeK9bNW1qw procToCodePos;
};
struct tyObject_ItemId__N3z7uIvKCrk0S9cej5ew0nA {
	NI32 module;
	NI32 item;
};
typedef NU8 tyEnum_TSymKind__9a75lHd9cV25XwN4jXXBmM0Q;
typedef NU16 tyEnum_TMagic__I3BbtzbLAKAfOym58MkGAg;
typedef NU64 tySet_tyEnum_TSymFlag__Ii9cPbwXR8ePKVRFOdTKpDg;
typedef NU32 tySet_tyEnum_TOption__LEwWf0yAgZkZx5Got2wGSQ;
typedef NU8 tyEnum_TLocKind__4f5UmCRM8alrCgj32FqIRQ;
typedef NU8 tyEnum_TStorageLoc__f19b6ZkRyh7BNPxlyjRbEGg;
typedef NU16 tySet_tyEnum_TLocFlag__Df76pxukl21zlyRFq9aa0Bg;
struct tyObject_TLoc__KvQVlvKb6mbV567NQ0hMOw {
	tyEnum_TLocKind__4f5UmCRM8alrCgj32FqIRQ k;
	tyEnum_TStorageLoc__f19b6ZkRyh7BNPxlyjRbEGg storage;
	tySet_tyEnum_TLocFlag__Df76pxukl21zlyRFq9aa0Bg flags;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* lode;
	NimStringV2 snippet;
};
struct tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg {
	tyObject_ItemId__N3z7uIvKCrk0S9cej5ew0nA itemId;
	tyEnum_TSymKind__9a75lHd9cV25XwN4jXXBmM0Q kind;
union{
	struct {
	tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* gcUnsafetyReason;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* transformedBody;
} _kind_1;
	struct {
	tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* guard;
	NI bitsize;
	NI alignment;
} _kind_2;
};
	tyEnum_TMagic__I3BbtzbLAKAfOym58MkGAg magic;
	tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg* typ;
	tyObject_TIdent__LG4CHFyohryWo9bzZeyjDjA* name;
	tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA info;
	tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* owner;
	tySet_tyEnum_TSymFlag__Ii9cPbwXR8ePKVRFOdTKpDg flags;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* ast;
	tySet_tyEnum_TOption__LEwWf0yAgZkZx5Got2wGSQ options;
	NI position;
	NI32 offset;
	NI32 disamb;
	tyObject_TLoc__KvQVlvKb6mbV567NQ0hMOw loc;
	tyObject_TLib__sVuuRvQkiYNOUiPRZhmZig* annex;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* constraint;
	tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* instantiatedFrom;
};
struct tySequence__9ahcSjIErJohSBBFLwuF56Q_Content { NI cap; tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* data[SEQ_DECL_SIZE]; };
struct tySequence__fnH5Zt4fsGx0AgDTOw8pKw_Content { NI cap; tyTuple__ZQn9aE2eP9aC9aAInis8k8aDQ data[SEQ_DECL_SIZE]; };
struct tySequence__YdLNCDKYeipzJx3I8Xw82Q_Content { NI cap; NU64 data[SEQ_DECL_SIZE]; };
struct tySequence__TLbZ2GqL3Zio1f8bNpj2NA_Content { NI cap; tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA data[SEQ_DECL_SIZE]; };
struct tySequence__d8ZV4IMgHt9biik9cS9a0hrmg_Content { NI cap; tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg* data[SEQ_DECL_SIZE]; };
typedef struct {
N_NIMCALL_PTR(void, ClP_0) (tyObject_VmArgs__PuiloXB3J4yNffJquqRVvg* args_p0, void* ClE_0);
void* ClE_0;
} tyProc__c9bXGQkBnyyGGVw063jrwZw;
struct tySequence__TEgqV1E9aHqnwBUoeXjBI9aA_Content { NI cap; tyProc__c9bXGQkBnyyGGVw063jrwZw data[SEQ_DECL_SIZE]; };
struct tyTuple__sr3s7uVHpu9cEbbDnCSg8yQ {
	NI Field0;
	NimStringV2 Field1;
	NI Field2;
};
struct tySequence__pUxI6Ljo6IKKHXKOZkQ7Og_Content { NI cap; tyTuple__sr3s7uVHpu9cEbbDnCSg8yQ data[SEQ_DECL_SIZE]; };
struct tyTuple__47w2DboNEPf69aPgubZdd7Q {
	NI Field0;
	NI Field1;
	NI Field2;
};
struct tySequence__E8pi9b5QNahsURYzXMjh3qw_Content { NI cap; tyTuple__47w2DboNEPf69aPgubZdd7Q data[SEQ_DECL_SIZE]; };
struct tyObject_VmArgs__PuiloXB3J4yNffJquqRVvg {
	NI ra;
	NI rb;
	NI rc;
	tyObject_TFullReg__kL8NONjyku1Uli8eXEVZ0A* slots;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* currentException;
	tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA currentLineInfo;
};
N_LIB_PRIVATE N_NIMCALL(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ*, newNodeI__ast_u4064)(tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw kind_p0, tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA info_p1);
N_LIB_PRIVATE N_NIMCALL(void, add__ast_u3188)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* father_p0, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* son_p1);
N_LIB_PRIVATE N_NIMCALL(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ*, newStrNode__ast_u5393)(NimStringV2 strVal_p0, tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA info_p1);
N_LIB_PRIVATE N_NIMCALL(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ*, newIntNode__ast_u5167)(tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw kind_p0, NI64 intVal_p1);
N_LIB_PRIVATE N_NIMCALL(void, append__macrocacheimpl_u4)(tyObject_TCtx__FHLmhXedV9bg0OyLunsHRew* c_p0, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* n_p1);
N_LIB_PRIVATE N_NIMCALL(void, add__macrocacheimpl_u7)(tySequence__fnH5Zt4fsGx0AgDTOw8pKw* x_p0, tyTuple__ZQn9aE2eP9aC9aAInis8k8aDQ y_p1);
static N_INLINE(void, nimZeroMem)(void* p_p0, NI size_p1);
static N_INLINE(void, nimSetMem__systemZmemory_u7)(void* a_p0, int v_p1, NI size_p2);
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void);
N_LIB_PRIVATE N_NIMCALL(tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg*, eqdup___ast_u4552)(tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* src_p0);
N_LIB_PRIVATE N_NIMCALL(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ*, eqdup___ast_u3231)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* src_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___ast_u3225)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* dest_p0);
N_LIB_PRIVATE N_NIMCALL(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ*, copyTree__ast_u6839)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* src_p0);
static const struct {
  NI cap; NIM_CHAR data[3+1];
} TM__HpV7Go5mo9cm54tY2JkX3mg_2 = { 3 | NIM_STRLIT_FLAG, "inc" };
static const NimStringV2 TM__HpV7Go5mo9cm54tY2JkX3mg_3 = {3, (NimStrPayload*)&TM__HpV7Go5mo9cm54tY2JkX3mg_2};
static const struct {
  NI cap; NIM_CHAR data[3+1];
} TM__HpV7Go5mo9cm54tY2JkX3mg_4 = { 3 | NIM_STRLIT_FLAG, "add" };
static const NimStringV2 TM__HpV7Go5mo9cm54tY2JkX3mg_5 = {3, (NimStrPayload*)&TM__HpV7Go5mo9cm54tY2JkX3mg_4};
static const struct {
  NI cap; NIM_CHAR data[4+1];
} TM__HpV7Go5mo9cm54tY2JkX3mg_6 = { 4 | NIM_STRLIT_FLAG, "incl" };
static const NimStringV2 TM__HpV7Go5mo9cm54tY2JkX3mg_7 = {4, (NimStrPayload*)&TM__HpV7Go5mo9cm54tY2JkX3mg_6};
static const struct {
  NI cap; NIM_CHAR data[3+1];
} TM__HpV7Go5mo9cm54tY2JkX3mg_8 = { 3 | NIM_STRLIT_FLAG, "put" };
static const NimStringV2 TM__HpV7Go5mo9cm54tY2JkX3mg_9 = {3, (NimStrPayload*)&TM__HpV7Go5mo9cm54tY2JkX3mg_8};
extern NIM_BOOL nimInErrorMode__system_u4310;
static N_INLINE(void, nimSetMem__systemZmemory_u7)(void* a_p0, int v_p1, NI size_p2) {
	void* T1_;
	T1_ = (void*)0;
	T1_ = memset(a_p0, v_p1, ((size_t) (size_p2)));
}
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void) {
	NIM_BOOL* result;
	result = (&nimInErrorMode__system_u4310);
	return result;
}
static N_INLINE(void, nimZeroMem)(void* p_p0, NI size_p1) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	nimSetMem__systemZmemory_u7(p_p0, ((int)0), size_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, append__macrocacheimpl_u4)(tyObject_TCtx__FHLmhXedV9bg0OyLunsHRew* c_p0, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* n_p1) {
	tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* colontmpD_;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* colontmpD__2;
	tyTuple__ZQn9aE2eP9aC9aAInis8k8aDQ T1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	colontmpD_ = NIM_NIL;
	colontmpD__2 = NIM_NIL;
	nimZeroMem((void*)(&T1_), sizeof(tyTuple__ZQn9aE2eP9aC9aAInis8k8aDQ));
	colontmpD_ = eqdup___ast_u4552((*c_p0).module);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	T1_.Field0 = colontmpD_;
	colontmpD__2 = eqdup___ast_u3231(n_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	T1_.Field1 = colontmpD__2;
	add__macrocacheimpl_u7((&(*c_p0).vmstateDiff), T1_);
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, recordInc__macrocacheimpl_u27)(tyObject_TCtx__FHLmhXedV9bg0OyLunsHRew* c_p0, tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA info_p1, NimStringV2 key_p2, NI64 by_p3) {
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* recorded;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* colontmpD_;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* colontmpD__2;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* colontmpD__3;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	recorded = NIM_NIL;
	colontmpD_ = NIM_NIL;
	colontmpD__2 = NIM_NIL;
	colontmpD__3 = NIM_NIL;
	recorded = newNodeI__ast_u4064(((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)163), info_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	colontmpD_ = newStrNode__ast_u5393(TM__HpV7Go5mo9cm54tY2JkX3mg_3, info_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	add__ast_u3188(recorded, colontmpD_);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	colontmpD__2 = newStrNode__ast_u5393(key_p2, info_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	add__ast_u3188(recorded, colontmpD__2);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	colontmpD__3 = newIntNode__ast_u5167(((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)6), by_p3);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	add__ast_u3188(recorded, colontmpD__3);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	append__macrocacheimpl_u4(c_p0, recorded);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	eqdestroy___ast_u3225(colontmpD__3);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	eqdestroy___ast_u3225(colontmpD__2);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	eqdestroy___ast_u3225(colontmpD_);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	eqdestroy___ast_u3225(recorded);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, recordAdd__macrocacheimpl_u40)(tyObject_TCtx__FHLmhXedV9bg0OyLunsHRew* c_p0, tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA info_p1, NimStringV2 key_p2, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* val_p3) {
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* recorded;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* colontmpD_;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* colontmpD__2;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* colontmpD__3;
NIM_BOOL oldNimErrFin1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	recorded = NIM_NIL;
	colontmpD_ = NIM_NIL;
	colontmpD__2 = NIM_NIL;
	colontmpD__3 = NIM_NIL;
	recorded = newNodeI__ast_u4064(((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)163), info_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	colontmpD_ = newStrNode__ast_u5393(TM__HpV7Go5mo9cm54tY2JkX3mg_5, info_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	add__ast_u3188(recorded, colontmpD_);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	colontmpD__2 = newStrNode__ast_u5393(key_p2, info_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	add__ast_u3188(recorded, colontmpD__2);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	colontmpD__3 = copyTree__ast_u6839(val_p3);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	add__ast_u3188(recorded, colontmpD__3);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	append__macrocacheimpl_u4(c_p0, recorded);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		LA1_:;
	}
	{
		oldNimErrFin1_ = *nimErr_; *nimErr_ = NIM_FALSE;
		eqdestroy___ast_u3225(colontmpD__3);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqdestroy___ast_u3225(colontmpD__2);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqdestroy___ast_u3225(colontmpD_);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqdestroy___ast_u3225(recorded);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		*nimErr_ = oldNimErrFin1_;
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, recordIncl__macrocacheimpl_u46)(tyObject_TCtx__FHLmhXedV9bg0OyLunsHRew* c_p0, tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA info_p1, NimStringV2 key_p2, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* val_p3) {
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* recorded;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* colontmpD_;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* colontmpD__2;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* colontmpD__3;
NIM_BOOL oldNimErrFin1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	recorded = NIM_NIL;
	colontmpD_ = NIM_NIL;
	colontmpD__2 = NIM_NIL;
	colontmpD__3 = NIM_NIL;
	recorded = newNodeI__ast_u4064(((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)163), info_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	colontmpD_ = newStrNode__ast_u5393(TM__HpV7Go5mo9cm54tY2JkX3mg_7, info_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	add__ast_u3188(recorded, colontmpD_);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	colontmpD__2 = newStrNode__ast_u5393(key_p2, info_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	add__ast_u3188(recorded, colontmpD__2);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	colontmpD__3 = copyTree__ast_u6839(val_p3);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	add__ast_u3188(recorded, colontmpD__3);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	append__macrocacheimpl_u4(c_p0, recorded);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		LA1_:;
	}
	{
		oldNimErrFin1_ = *nimErr_; *nimErr_ = NIM_FALSE;
		eqdestroy___ast_u3225(colontmpD__3);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqdestroy___ast_u3225(colontmpD__2);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqdestroy___ast_u3225(colontmpD_);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqdestroy___ast_u3225(recorded);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		*nimErr_ = oldNimErrFin1_;
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, recordPut__macrocacheimpl_u33)(tyObject_TCtx__FHLmhXedV9bg0OyLunsHRew* c_p0, tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA info_p1, NimStringV2 key_p2, NimStringV2 k_p3, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* val_p4) {
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* recorded;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* colontmpD_;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* colontmpD__2;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* colontmpD__3;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* colontmpD__4;
NIM_BOOL oldNimErrFin1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	recorded = NIM_NIL;
	colontmpD_ = NIM_NIL;
	colontmpD__2 = NIM_NIL;
	colontmpD__3 = NIM_NIL;
	colontmpD__4 = NIM_NIL;
	recorded = newNodeI__ast_u4064(((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)163), info_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	colontmpD_ = newStrNode__ast_u5393(TM__HpV7Go5mo9cm54tY2JkX3mg_9, info_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	add__ast_u3188(recorded, colontmpD_);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	colontmpD__2 = newStrNode__ast_u5393(key_p2, info_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	add__ast_u3188(recorded, colontmpD__2);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	colontmpD__3 = newStrNode__ast_u5393(k_p3, info_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	add__ast_u3188(recorded, colontmpD__3);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	colontmpD__4 = copyTree__ast_u6839(val_p4);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	add__ast_u3188(recorded, colontmpD__4);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	append__macrocacheimpl_u4(c_p0, recorded);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		LA1_:;
	}
	{
		oldNimErrFin1_ = *nimErr_; *nimErr_ = NIM_FALSE;
		eqdestroy___ast_u3225(colontmpD__4);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqdestroy___ast_u3225(colontmpD__3);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqdestroy___ast_u3225(colontmpD__2);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqdestroy___ast_u3225(colontmpD_);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqdestroy___ast_u3225(recorded);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		*nimErr_ = oldNimErrFin1_;
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
