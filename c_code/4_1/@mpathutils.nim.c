/* Generated by Nim Compiler v2.2.0 */
#define NIM_INTBITS 32

#include "nimbase.h"
#include <string.h>
#undef LANGUAGE_C
#undef MIPSEB
#undef MIPSEL
#undef PPC
#undef R3000
#undef R4000
#undef i386
#undef linux
#undef mips
#undef near
#undef far
#undef powerpc
#undef unix
typedef struct NimStrPayload NimStrPayload;
typedef struct NimStringV2 NimStringV2;
typedef struct tyTuple__7q7q3E6Oj24ZNVJb9aonhAg tyTuple__7q7q3E6Oj24ZNVJb9aonhAg;
struct NimStrPayload {
	NI cap;
	NIM_CHAR data[SEQ_DECL_SIZE];
};
struct NimStringV2 {
	NI len;
	NimStrPayload* p;
};
struct tyTuple__7q7q3E6Oj24ZNVJb9aonhAg {
	NimStringV2 Field0;
	NimStringV2 Field1;
	NimStringV2 Field2;
};
N_LIB_PRIVATE N_NIMCALL(NimStringV2, eqdup___system_u2603)(NimStringV2 src_p0);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, nosrelativePath)(NimStringV2 path_p0, NimStringV2 base_p1, NIM_CHAR sep_p2);
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void);
static N_INLINE(NIM_BOOL, eqImpl__pathutils_u124)(NimStringV2 x_p0, NimStringV2 y_p1);
N_LIB_PRIVATE N_NIMCALL(NI, noscmpPaths)(NimStringV2 pathA_p0, NimStringV2 pathB_p1);
static N_INLINE(NIM_BOOL, isEmpty__pathutils_u139)(NimStringV2 x_p0);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, nosgetCurrentDir)(void);
N_LIB_PRIVATE N_NIMCALL(void, eqcopy___system_u2600)(NimStringV2* dest_p0, NimStringV2 src_p1);
N_NIMCALL(NimStringV2, rawNewString)(NI cap_p0);
N_LIB_PRIVATE N_NIMCALL(void, addNormalizePath__pureZpathnorm_u86)(NimStringV2 x_p0, NimStringV2* result_p1, NI* state_p2, NIM_CHAR dirSep_p3);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___system_u2597)(NimStringV2 dest_p0);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, nosisAbsolute)(NimStringV2 path_p0);
static N_INLINE(NimStringV2, slash___stdZprivateZospaths2_u91)(NimStringV2 head_p0, NimStringV2 tail_p1);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, nosjoinPath)(NimStringV2 head_p0, NimStringV2 tail_p1);
static N_INLINE(void, nimZeroMem)(void* p_p0, NI size_p1);
static N_INLINE(void, nimSetMem__systemZmemory_u7)(void* a_p0, int v_p1, NI size_p2);
N_LIB_PRIVATE N_NIMCALL(tyTuple__7q7q3E6Oj24ZNVJb9aonhAg, nossplitFile)(NimStringV2 path_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqwasMoved___system_u2594)(NimStringV2* dest_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___pureZos_u169)(tyTuple__7q7q3E6Oj24ZNVJb9aonhAg dest_p0);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, customPathImpl__pathutils_u250)(NimStringV2 x_p0);
N_LIB_PRIVATE N_NIMCALL(NI, skipHomeDir__pathutils_u230)(NimStringV2 x_p0);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, nsuStartsWith)(NimStringV2 s_p0, NimStringV2 prefix_p1);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, nsuContinuesWith)(NimStringV2 s_p0, NimStringV2 substr_p1, NI start_p2);
static N_INLINE(void, appendString)(NimStringV2* dest_p0, NimStringV2 src_p1);
static N_INLINE(void, copyMem__system_u1713)(void* dest_p0, void* source_p1, NI size_p2);
static N_INLINE(void, nimCopyMem)(void* dest_p0, void* source_p1, NI size_p2);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, relevantPart__pathutils_u233)(NimStringV2 s_p0, NI afterSlashX_p1);
static N_INLINE(void, nimAddCharV1)(NimStringV2* s_p0, NIM_CHAR c_p1);
N_LIB_PRIVATE N_NIMCALL(void, prepareAdd)(NimStringV2* s_p0, NI addLen_p1);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, slash___pathutils_u135)(NimStringV2 base_p0, NimStringV2 f_p1);
static const struct {
  NI cap; NIM_CHAR data[6+1];
} TM__r4WvUOkTx4ylahJtpRE9aTQ_2 = { 6 | NIM_STRLIT_FLAG, "/home/" };
static const NimStringV2 TM__r4WvUOkTx4ylahJtpRE9aTQ_3 = {6, (NimStrPayload*)&TM__r4WvUOkTx4ylahJtpRE9aTQ_2};
static const struct {
  NI cap; NIM_CHAR data[7+1];
} TM__r4WvUOkTx4ylahJtpRE9aTQ_4 = { 7 | NIM_STRLIT_FLAG, "/Users/" };
static const NimStringV2 TM__r4WvUOkTx4ylahJtpRE9aTQ_5 = {7, (NimStrPayload*)&TM__r4WvUOkTx4ylahJtpRE9aTQ_4};
static const struct {
  NI cap; NIM_CHAR data[5+1];
} TM__r4WvUOkTx4ylahJtpRE9aTQ_6 = { 5 | NIM_STRLIT_FLAG, "/mnt/" };
static const NimStringV2 TM__r4WvUOkTx4ylahJtpRE9aTQ_7 = {5, (NimStrPayload*)&TM__r4WvUOkTx4ylahJtpRE9aTQ_6};
static const NimStringV2 TM__r4WvUOkTx4ylahJtpRE9aTQ_8 = {7, (NimStrPayload*)&TM__r4WvUOkTx4ylahJtpRE9aTQ_4};
static const struct {
  NI cap; NIM_CHAR data[7+1];
} TM__r4WvUOkTx4ylahJtpRE9aTQ_9 = { 7 | NIM_STRLIT_FLAG, "//user/" };
static const NimStringV2 TM__r4WvUOkTx4ylahJtpRE9aTQ_10 = {7, (NimStrPayload*)&TM__r4WvUOkTx4ylahJtpRE9aTQ_9};
extern NIM_BOOL nimInErrorMode__system_u4310;
N_LIB_PRIVATE N_NIMCALL(NimStringV2, dollar___options_u3932)(NimStringV2 x_p0) {
	NimStringV2 result;
	NimStringV2 colontmpD_;
	colontmpD_.len = 0; colontmpD_.p = NIM_NIL;
	colontmpD_ = eqdup___system_u2603(x_p0);
	result = colontmpD_;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NimStringV2, dollar___options_u4022)(NimStringV2 x_p0) {
	NimStringV2 result;
	NimStringV2 colontmpD_;
	colontmpD_.len = 0; colontmpD_.p = NIM_NIL;
	colontmpD_ = eqdup___system_u2603(x_p0);
	result = colontmpD_;
	return result;
}
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void) {
	NIM_BOOL* result;
	result = (&nimInErrorMode__system_u4310);
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NimStringV2, relativeTo__pathutils_u196)(NimStringV2 fullPath_p0, NimStringV2 baseFilename_p1, NIM_CHAR sep_p2) {
	NimStringV2 result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result.len = 0; result.p = NIM_NIL;
	result = nosrelativePath(fullPath_p0, baseFilename_p1, sep_p2);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
static N_INLINE(NIM_BOOL, eqImpl__pathutils_u124)(NimStringV2 x_p0, NimStringV2 y_p1) {
	NIM_BOOL result;
	NI T1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NIM_BOOL)0;
	T1_ = (NI)0;
	T1_ = noscmpPaths(x_p0, y_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	result = (T1_ == ((NI)0));
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, eqeq___options_u3645)(NimStringV2 x_p0, NimStringV2 y_p1) {
	NIM_BOOL result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NIM_BOOL)0;
	result = eqImpl__pathutils_u124(x_p0, y_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
static N_INLINE(NIM_BOOL, isEmpty__pathutils_u139)(NimStringV2 x_p0) {
	NIM_BOOL result;
	result = (x_p0.len == ((NI)0));
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NimStringV2, slash___pathutils_u166)(NimStringV2 base_p0, NimStringV2 f_p1) {
	NimStringV2 result;
	NimStringV2 base_2;
	NimStringV2 T2_;
	NI state;
	NimStringV2 T9_;
	NimStringV2 T10_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result.len = 0; result.p = NIM_NIL;
	base_2.len = 0; base_2.p = NIM_NIL;
	T2_.len = 0; T2_.p = NIM_NIL;
	{
		NIM_BOOL T5_;
		T5_ = (NIM_BOOL)0;
		T5_ = isEmpty__pathutils_u139(base_p0);
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
		if (!T5_) goto LA6_;
		base_2 = nosgetCurrentDir();
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	}
	goto LA3_;
LA6_: ;
	{
		eqcopy___system_u2600((&base_2), base_p0);
	}
LA3_: ;
	result = rawNewString(((NI)(base_2.len + f_p1.len)));
	state = ((NI)0);
	T9_.len = 0; T9_.p = NIM_NIL;
	T9_ = base_2;
	addNormalizePath__pureZpathnorm_u86(T9_, (&result), (&state), 47);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	T10_.len = 0; T10_.p = NIM_NIL;
	T10_ = f_p1;
	addNormalizePath__pureZpathnorm_u86(T10_, (&result), (&state), 47);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		LA1_:;
	}
	{
		eqdestroy___system_u2597(base_2);
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
static N_INLINE(NimStringV2, slash___stdZprivateZospaths2_u91)(NimStringV2 head_p0, NimStringV2 tail_p1) {
	NimStringV2 result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result.len = 0; result.p = NIM_NIL;
	result = nosjoinPath(head_p0, tail_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NimStringV2, toAbsoluteDir__pathutils_u117)(NimStringV2 path_p0) {
	NimStringV2 result;
	NimStringV2 T1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result.len = 0; result.p = NIM_NIL;
	T1_.len = 0; T1_.p = NIM_NIL;
	{
		NIM_BOOL T4_;
		NimStringV2 colontmpD_;
		T4_ = (NIM_BOOL)0;
		T4_ = nosisAbsolute(path_p0);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		if (!T4_) goto LA5_;
		colontmpD_.len = 0; colontmpD_.p = NIM_NIL;
		colontmpD_ = eqdup___system_u2603(path_p0);
		result = colontmpD_;
	}
	goto LA2_;
LA5_: ;
	{
		NimStringV2 colontmpD__2;
		colontmpD__2.len = 0; colontmpD__2.p = NIM_NIL;
		colontmpD__2 = nosgetCurrentDir();
		if (NIM_UNLIKELY(*nimErr_)) goto LA8_;
		result = slash___stdZprivateZospaths2_u91(colontmpD__2, path_p0);
		if (NIM_UNLIKELY(*nimErr_)) goto LA8_;
		{
			LA8_:;
		}
		{
			eqdestroy___system_u2597(colontmpD__2);
		}
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
LA2_: ;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NimStringV2, slash___pathutils_u135)(NimStringV2 base_p0, NimStringV2 f_p1) {
	NimStringV2 result;
	NimStringV2 base_2;
	NimStringV2 T2_;
	NI state;
	NimStringV2 T9_;
	NimStringV2 T10_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result.len = 0; result.p = NIM_NIL;
	base_2.len = 0; base_2.p = NIM_NIL;
	T2_.len = 0; T2_.p = NIM_NIL;
	{
		NIM_BOOL T5_;
		T5_ = (NIM_BOOL)0;
		T5_ = isEmpty__pathutils_u139(base_p0);
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
		if (!T5_) goto LA6_;
		base_2 = nosgetCurrentDir();
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	}
	goto LA3_;
LA6_: ;
	{
		eqcopy___system_u2600((&base_2), base_p0);
	}
LA3_: ;
	result = rawNewString(((NI)(base_2.len + f_p1.len)));
	state = ((NI)0);
	T9_.len = 0; T9_.p = NIM_NIL;
	T9_ = base_2;
	addNormalizePath__pureZpathnorm_u86(T9_, (&result), (&state), 47);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	T10_.len = 0; T10_.p = NIM_NIL;
	T10_ = f_p1;
	addNormalizePath__pureZpathnorm_u86(T10_, (&result), (&state), 47);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		LA1_:;
	}
	{
		eqdestroy___system_u2597(base_2);
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
static N_INLINE(void, nimSetMem__systemZmemory_u7)(void* a_p0, int v_p1, NI size_p2) {
	void* T1_;
	T1_ = (void*)0;
	T1_ = memset(a_p0, v_p1, ((size_t) (size_p2)));
}
static N_INLINE(void, nimZeroMem)(void* p_p0, NI size_p1) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	nimSetMem__systemZmemory_u7(p_p0, ((int)0), size_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(tyTuple__7q7q3E6Oj24ZNVJb9aonhAg, splitFile__pathutils_u39)(NimStringV2 x_p0) {
	tyTuple__7q7q3E6Oj24ZNVJb9aonhAg result;
	tyTuple__7q7q3E6Oj24ZNVJb9aonhAg tmpTuple;
	NimStringV2 a;
	NimStringV2 b;
	NimStringV2 c;
	NimStringV2 colontmp_;
	NimStringV2 colontmp__2;
	NimStringV2 colontmp__3;
	NimStringV2 blitTmp;
	NimStringV2 blitTmp_2;
	NimStringV2 blitTmp_3;
	NimStringV2 blitTmp_4;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	nimZeroMem((void*)(&result), sizeof(tyTuple__7q7q3E6Oj24ZNVJb9aonhAg));
	nimZeroMem((void*)(&tmpTuple), sizeof(tyTuple__7q7q3E6Oj24ZNVJb9aonhAg));
	a.len = 0; a.p = NIM_NIL;
	b.len = 0; b.p = NIM_NIL;
	c.len = 0; c.p = NIM_NIL;
	colontmp_.len = 0; colontmp_.p = NIM_NIL;
	colontmp__2.len = 0; colontmp__2.p = NIM_NIL;
	colontmp__3.len = 0; colontmp__3.p = NIM_NIL;
	tmpTuple = nossplitFile(x_p0);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	a = tmpTuple.Field0;
	eqwasMoved___system_u2594((&tmpTuple.Field0));
	b = tmpTuple.Field1;
	eqwasMoved___system_u2594((&tmpTuple.Field1));
	c = tmpTuple.Field2;
	eqwasMoved___system_u2594((&tmpTuple.Field2));
	blitTmp = a;
	colontmp_ = blitTmp;
	colontmp__2 = b;
	colontmp__3 = c;
	blitTmp_2 = colontmp_;
	result.Field0 = blitTmp_2;
	blitTmp_3 = colontmp__2;
	result.Field1 = blitTmp_3;
	blitTmp_4 = colontmp__3;
	result.Field2 = blitTmp_4;
	eqdestroy___pureZos_u169(tmpTuple);
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___pathutils_u73)(tyTuple__7q7q3E6Oj24ZNVJb9aonhAg dest_p0) {
	eqdestroy___system_u2597(dest_p0.Field0);
	eqdestroy___system_u2597(dest_p0.Field1);
	eqdestroy___system_u2597(dest_p0.Field2);
}
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, eqeq___nimconf_u332)(NimStringV2 x_p0, NimStringV2 y_p1) {
	NIM_BOOL result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NIM_BOOL)0;
	result = eqImpl__pathutils_u124(x_p0, y_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NI, skipHomeDir__pathutils_u230)(NimStringV2 x_p0) {
	NI result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	{
		NIM_BOOL T3_;
		T3_ = (NIM_BOOL)0;
		T3_ = nsuStartsWith(x_p0, TM__r4WvUOkTx4ylahJtpRE9aTQ_3);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		if (T3_) goto LA4_;
		T3_ = nsuStartsWith(x_p0, TM__r4WvUOkTx4ylahJtpRE9aTQ_5);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
LA4_: ;
		if (!T3_) goto LA5_;
		result = ((NI)3);
	}
	goto LA1_;
LA5_: ;
	{
		NIM_BOOL T8_;
		T8_ = (NIM_BOOL)0;
		T8_ = nsuStartsWith(x_p0, TM__r4WvUOkTx4ylahJtpRE9aTQ_7);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		if (!(T8_)) goto LA9_;
		T8_ = nsuContinuesWith(x_p0, TM__r4WvUOkTx4ylahJtpRE9aTQ_8, ((NI)6));
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
LA9_: ;
		if (!T8_) goto LA10_;
		result = ((NI)5);
	}
	goto LA1_;
LA10_: ;
	{
		result = ((NI)0);
	}
LA1_: ;
	}BeforeRet_: ;
	return result;
}
static N_INLINE(void, nimCopyMem)(void* dest_p0, void* source_p1, NI size_p2) {
	void* T1_;
	T1_ = (void*)0;
	T1_ = memcpy(dest_p0, source_p1, ((size_t) (size_p2)));
}
static N_INLINE(void, copyMem__system_u1713)(void* dest_p0, void* source_p1, NI size_p2) {
	nimCopyMem(dest_p0, source_p1, size_p2);
}
static N_INLINE(void, appendString)(NimStringV2* dest_p0, NimStringV2 src_p1) {
	{
		if (!(((NI)0) < src_p1.len)) goto LA3_;
		copyMem__system_u1713(((void*) ((&(*(*dest_p0).p).data[(*dest_p0).len]))), ((void*) ((&(*src_p1.p).data[((NI)0)]))), ((NI)(src_p1.len + ((NI)1))));
		(*dest_p0).len += src_p1.len;
	}
LA3_: ;
}
static N_INLINE(void, nimAddCharV1)(NimStringV2* s_p0, NIM_CHAR c_p1) {
	prepareAdd(s_p0, ((NI)1));
	(*(*s_p0).p).data[(*s_p0).len] = c_p1;
	(*s_p0).len += ((NI)1);
	(*(*s_p0).p).data[(*s_p0).len] = 0;
}
N_LIB_PRIVATE N_NIMCALL(NimStringV2, relevantPart__pathutils_u233)(NimStringV2 s_p0, NI afterSlashX_p1) {
	NimStringV2 result;
	NI slashes;
	result.len = 0; result.p = NIM_NIL;
	result = rawNewString(((NI)(s_p0.len - ((NI)8))));
	slashes = afterSlashX_p1;
	{
		NI i;
		NI colontmp_;
		NI i_2;
		i = (NI)0;
		colontmp_ = (NI)0;
		colontmp_ = s_p0.len;
		i_2 = ((NI)0);
		{
			while (1) {
				if (!(i_2 < colontmp_)) goto LA3;
				i = i_2;
				{
					if (!(slashes == ((NI)0))) goto LA6_;
					nimAddCharV1((&result), s_p0.p->data[i]);
				}
				goto LA4_;
LA6_: ;
				{
					if (!((NU8)(s_p0.p->data[i]) == (NU8)(47))) goto LA9_;
					slashes -= ((NI)1);
				}
				goto LA4_;
LA9_: ;
LA4_: ;
				i_2 += ((NI)1);
			} LA3: ;
		}
	}
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NimStringV2, customPathImpl__pathutils_u250)(NimStringV2 x_p0) {
	NimStringV2 result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result.len = 0; result.p = NIM_NIL;
	{
		NIM_BOOL T3_;
		NimStringV2 colontmpD_;
		NimStringV2 colontmpD__2;
		T3_ = (NIM_BOOL)0;
		T3_ = nosisAbsolute(x_p0);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		if (!!(T3_)) goto LA4_;
		colontmpD_.len = 0; colontmpD_.p = NIM_NIL;
		colontmpD__2.len = 0; colontmpD__2.p = NIM_NIL;
		colontmpD_ = nosgetCurrentDir();
		if (NIM_UNLIKELY(*nimErr_)) goto LA6_;
		colontmpD__2 = slash___stdZprivateZospaths2_u91(colontmpD_, x_p0);
		if (NIM_UNLIKELY(*nimErr_)) goto LA6_;
		result = customPathImpl__pathutils_u250(colontmpD__2);
		if (NIM_UNLIKELY(*nimErr_)) goto LA6_;
		{
			LA6_:;
		}
		{
			eqdestroy___system_u2597(colontmpD__2);
			eqdestroy___system_u2597(colontmpD_);
		}
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
	goto LA1_;
LA4_: ;
	{
		NI slashes;
		slashes = skipHomeDir__pathutils_u230(x_p0);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		{
			NimStringV2 colontmpD__3;
			NimStringV2 T14_;
			if (!(((NI)0) < slashes)) goto LA12_;
			colontmpD__3.len = 0; colontmpD__3.p = NIM_NIL;
			T14_.len = 0; T14_.p = NIM_NIL;
			colontmpD__3 = relevantPart__pathutils_u233(x_p0, slashes);
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			T14_ = rawNewString(colontmpD__3.len + 7);
appendString((&T14_), TM__r4WvUOkTx4ylahJtpRE9aTQ_10);
appendString((&T14_), colontmpD__3);
			result = T14_;
			eqdestroy___system_u2597(colontmpD__3);
		}
		goto LA10_;
LA12_: ;
		{
			eqcopy___system_u2600((&result), x_p0);
		}
LA10_: ;
	}
LA1_: ;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NimStringV2, customPath__pathutils_u254)(NimStringV2 x_p0) {
	NimStringV2 result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result.len = 0; result.p = NIM_NIL;
	result = customPathImpl__pathutils_u250(x_p0);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NimStringV2, dollar___docgen_u12964)(NimStringV2 x_p0) {
	NimStringV2 result;
	NimStringV2 colontmpD_;
	colontmpD_.len = 0; colontmpD_.p = NIM_NIL;
	colontmpD_ = eqdup___system_u2603(x_p0);
	result = colontmpD_;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, eqeq___nimconf_u472)(NimStringV2 x_p0, NimStringV2 y_p1) {
	NIM_BOOL result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NIM_BOOL)0;
	result = eqImpl__pathutils_u124(x_p0, y_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NimStringV2, toAbsolute__pathutils_u201)(NimStringV2 file_p0, NimStringV2 base_p1) {
	NimStringV2 result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result.len = 0; result.p = NIM_NIL;
	{
		NIM_BOOL T3_;
		NimStringV2 colontmpD_;
		T3_ = (NIM_BOOL)0;
		T3_ = nosisAbsolute(file_p0);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		if (!T3_) goto LA4_;
		colontmpD_.len = 0; colontmpD_.p = NIM_NIL;
		colontmpD_ = eqdup___system_u2603(file_p0);
		result = colontmpD_;
	}
	goto LA1_;
LA4_: ;
	{
		result = slash___pathutils_u135(base_p1, file_p0);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
LA1_: ;
	}BeforeRet_: ;
	return result;
}
