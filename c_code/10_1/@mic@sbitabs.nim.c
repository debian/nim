/* Generated by Nim Compiler v2.2.0 */
#define NIM_INTBITS 32

#include "nimbase.h"
#include <string.h>
#undef LANGUAGE_C
#undef MIPSEB
#undef MIPSEL
#undef PPC
#undef R3000
#undef R4000
#undef i386
#undef linux
#undef mips
#undef near
#undef far
#undef powerpc
#undef unix
typedef struct tySequence__9bAGqSvkAaFL9bWjsEPslrFA tySequence__9bAGqSvkAaFL9bWjsEPslrFA;
typedef struct tySequence__9bAGqSvkAaFL9bWjsEPslrFA_Content tySequence__9bAGqSvkAaFL9bWjsEPslrFA_Content;
typedef struct tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg;
typedef struct tyObject_BiTable__KR9c7QZguPO3OKZOIcphgcA tyObject_BiTable__KR9c7QZguPO3OKZOIcphgcA;
typedef struct tySequence__sM4lkSb7zS6F7OVMvW9cffQ tySequence__sM4lkSb7zS6F7OVMvW9cffQ;
typedef struct tySequence__sM4lkSb7zS6F7OVMvW9cffQ_Content tySequence__sM4lkSb7zS6F7OVMvW9cffQ_Content;
typedef struct tyObject_BiTable__9aUXwPoJbNPqj9a249c0iSmlg tyObject_BiTable__9aUXwPoJbNPqj9a249c0iSmlg;
typedef struct tySequence__IHUFRsFxZNv7YydiUO2esQ tySequence__IHUFRsFxZNv7YydiUO2esQ;
typedef struct tySequence__IHUFRsFxZNv7YydiUO2esQ_Content tySequence__IHUFRsFxZNv7YydiUO2esQ_Content;
typedef struct tyObject_NimRawSeq__Tvl1urqmI1tXqzkHPbWVJQ tyObject_NimRawSeq__Tvl1urqmI1tXqzkHPbWVJQ;
typedef struct NimStrPayload NimStrPayload;
typedef struct NimStringV2 NimStringV2;
struct tySequence__9bAGqSvkAaFL9bWjsEPslrFA {
  NI len; tySequence__9bAGqSvkAaFL9bWjsEPslrFA_Content* p;
};
struct tySequence__sM4lkSb7zS6F7OVMvW9cffQ {
  NI len; tySequence__sM4lkSb7zS6F7OVMvW9cffQ_Content* p;
};
struct tyObject_BiTable__KR9c7QZguPO3OKZOIcphgcA {
	tySequence__sM4lkSb7zS6F7OVMvW9cffQ vals;
	tySequence__9bAGqSvkAaFL9bWjsEPslrFA keys;
};
struct tySequence__IHUFRsFxZNv7YydiUO2esQ {
  NI len; tySequence__IHUFRsFxZNv7YydiUO2esQ_Content* p;
};
struct tyObject_BiTable__9aUXwPoJbNPqj9a249c0iSmlg {
	tySequence__IHUFRsFxZNv7YydiUO2esQ vals;
	tySequence__9bAGqSvkAaFL9bWjsEPslrFA keys;
};
struct tyObject_NimRawSeq__Tvl1urqmI1tXqzkHPbWVJQ {
	NI len;
	void* p;
};
struct NimStrPayload {
	NI cap;
	NIM_CHAR data[SEQ_DECL_SIZE];
};
struct NimStringV2 {
	NI len;
	NimStrPayload* p;
};
struct tySequence__9bAGqSvkAaFL9bWjsEPslrFA_Content { NI cap; NU32 data[SEQ_DECL_SIZE]; };
struct tySequence__sM4lkSb7zS6F7OVMvW9cffQ_Content { NI cap; NimStringV2 data[SEQ_DECL_SIZE]; };
struct tySequence__IHUFRsFxZNv7YydiUO2esQ_Content { NI cap; NI64 data[SEQ_DECL_SIZE]; };
N_LIB_PRIVATE N_NIMCALL(void, alignedDealloc)(void* p_p0, NI align_p1);
N_LIB_PRIVATE N_NIMCALL(void, loadSeq__icZic_u7647)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tySequence__sM4lkSb7zS6F7OVMvW9cffQ* s_p1);
N_LIB_PRIVATE N_NIMCALL(void, loadSeq__icZic_u7683)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tySequence__9bAGqSvkAaFL9bWjsEPslrFA* s_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___icZic_u1677)(tySequence__9bAGqSvkAaFL9bWjsEPslrFA dest_p0);
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void);
N_LIB_PRIVATE N_NIMCALL(void, loadSeq__icZic_u7964)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tySequence__IHUFRsFxZNv7YydiUO2esQ* s_p1);
static N_INLINE(NIM_BOOL, sameSeqPayload)(void* x_p0, void* y_p1);
N_LIB_PRIVATE N_NIMCALL(void, setLen__icZic_u1580)(tySequence__9bAGqSvkAaFL9bWjsEPslrFA* s_p0, NI newlen_p1);
N_LIB_PRIVATE N_NIMCALL(NI, hash__pureZhashes_u593)(NimStringV2 x_p0);
static N_INLINE(NIM_BOOL, eqStrings)(NimStringV2 a_p0, NimStringV2 b_p1);
static N_INLINE(NIM_BOOL, equalMem__system_u1721)(void* a_p0, void* b_p1, NI size_p2);
static N_INLINE(int, nimCmpMem)(void* a_p0, void* b_p1, NI size_p2);
static N_INLINE(NI, nextTry__icZbitabs_u12)(NI h_p0, NI maxHash_p1);
static N_INLINE(NIM_BOOL, mustRehash__icZbitabs_u56)(NI length_p0, NI counter_p1);
N_LIB_PRIVATE N_NIMCALL(void, enlarge__icZic_u1565)(tyObject_BiTable__KR9c7QZguPO3OKZOIcphgcA* t_p0);
N_LIB_PRIVATE N_NIMCALL(void, newSeq__icZic_u1572)(tySequence__9bAGqSvkAaFL9bWjsEPslrFA* s_p0, NI len_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqwasMoved___icZiclineinfos_u100)(NU32* dest_p0);
N_LIB_PRIVATE N_NIMCALL(void, add__stdZenumutils_u71)(tySequence__sM4lkSb7zS6F7OVMvW9cffQ* x_p0, NimStringV2 y_p1);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, eqdup___system_u2603)(NimStringV2 src_p0);
static N_INLINE(NI, hash__pureZjson_u3365)(NI64 x_p0);
static N_INLINE(NI, hashWangYi1__pureZhashes_u139)(NU64 x_p0);
static N_INLINE(NU64, hiXorLo__pureZhashes_u80)(NU64 a_p0, NU64 b_p1);
static N_INLINE(NU64, hiXorLoFallback64__pureZhashes_u36)(NU64 a_p0, NU64 b_p1);
N_LIB_PRIVATE N_NIMCALL(void, enlarge__icZic_u3374)(tyObject_BiTable__9aUXwPoJbNPqj9a249c0iSmlg* t_p0);
N_LIB_PRIVATE N_NIMCALL(void, add__icZic_u3428)(tySequence__IHUFRsFxZNv7YydiUO2esQ* x_p0, NI64 y_p1);
N_LIB_PRIVATE N_NIMCALL(void, storeSeq__icZic_u10174)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tySequence__sM4lkSb7zS6F7OVMvW9cffQ s_p1);
N_LIB_PRIVATE N_NIMCALL(void, storeSeq__icZic_u10207)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tySequence__9bAGqSvkAaFL9bWjsEPslrFA s_p1);
N_LIB_PRIVATE N_NIMCALL(void, storeSeq__icZic_u10319)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tySequence__IHUFRsFxZNv7YydiUO2esQ s_p1);
extern NIM_BOOL nimInErrorMode__system_u4311;
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___icZic_u1677)(tySequence__9bAGqSvkAaFL9bWjsEPslrFA dest_p0) {
	if (dest_p0.p && !(dest_p0.p->cap & NIM_STRLIT_FLAG)) {
 alignedDealloc(dest_p0.p, NIM_ALIGNOF(NU32));
}
}
N_LIB_PRIVATE N_NIMCALL(void, eqsink___icZic_u1686)(tySequence__9bAGqSvkAaFL9bWjsEPslrFA* dest_p0, tySequence__9bAGqSvkAaFL9bWjsEPslrFA src_p1) {
	if ((*dest_p0).p != src_p1.p) {	eqdestroy___icZic_u1677((*dest_p0));
	}
(*dest_p0).len = src_p1.len; (*dest_p0).p = src_p1.p;
}
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void) {
	NIM_BOOL* result;
	result = (&nimInErrorMode__system_u4311);
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, load__icZic_u7643)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tyObject_BiTable__KR9c7QZguPO3OKZOIcphgcA* t_p1) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	loadSeq__icZic_u7647(f_p0, (&(*t_p1).vals));
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	loadSeq__icZic_u7683(f_p0, (&(*t_p1).keys));
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, load__icZic_u7960)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tyObject_BiTable__9aUXwPoJbNPqj9a249c0iSmlg* t_p1) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	loadSeq__icZic_u7964(f_p0, (&(*t_p1).vals));
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	loadSeq__icZic_u7683(f_p0, (&(*t_p1).keys));
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
static N_INLINE(NIM_BOOL, sameSeqPayload)(void* x_p0, void* y_p1) {
	NIM_BOOL result;
	result = ((*((tyObject_NimRawSeq__Tvl1urqmI1tXqzkHPbWVJQ*) (x_p0))).p == (*((tyObject_NimRawSeq__Tvl1urqmI1tXqzkHPbWVJQ*) (y_p1))).p);
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, eqcopy___icZic_u1680)(tySequence__9bAGqSvkAaFL9bWjsEPslrFA* dest_p0, tySequence__9bAGqSvkAaFL9bWjsEPslrFA src_p1) {
	NI T6_;
	NI colontmp_;
{	{
		NIM_BOOL T3_;
		T3_ = (NIM_BOOL)0;
		T3_ = sameSeqPayload(dest_p0, (&src_p1));
		if (!T3_) goto LA4_;
		goto BeforeRet_;
	}
LA4_: ;
	T6_ = src_p1.len;
	setLen__icZic_u1580((&(*dest_p0)), T6_);
	colontmp_ = ((NI)0);
	{
		while (1) {
			NI T9_;
			T9_ = (*dest_p0).len;
			if (!(colontmp_ < T9_)) goto LA8;
			(*dest_p0).p->data[colontmp_] = src_p1.p->data[colontmp_];
			colontmp_ += ((NI)1);
		} LA8: ;
	}
	}BeforeRet_: ;
}
static N_INLINE(int, nimCmpMem)(void* a_p0, void* b_p1, NI size_p2) {
	int result;
	result = memcmp(a_p0, b_p1, ((size_t) (size_p2)));
	return result;
}
static N_INLINE(NIM_BOOL, equalMem__system_u1721)(void* a_p0, void* b_p1, NI size_p2) {
	NIM_BOOL result;
	int T1_;
	T1_ = (int)0;
	T1_ = nimCmpMem(a_p0, b_p1, size_p2);
	result = (T1_ == ((NI32)0));
	return result;
}
static N_INLINE(NIM_BOOL, eqStrings)(NimStringV2 a_p0, NimStringV2 b_p1) {
	NIM_BOOL result;
	NI alen;
	NI blen;
{	result = (NIM_BOOL)0;
	alen = a_p0.len;
	blen = b_p1.len;
	{
		if (!(alen == blen)) goto LA3_;
		{
			if (!(alen == ((NI)0))) goto LA7_;
			result = NIM_TRUE;
			goto BeforeRet_;
		}
LA7_: ;
		result = equalMem__system_u1721(((void*) ((&a_p0.p->data[((NI)0)]))), ((void*) ((&b_p1.p->data[((NI)0)]))), (alen));
		goto BeforeRet_;
	}
LA3_: ;
	}BeforeRet_: ;
	return result;
}
static N_INLINE(NI, nextTry__icZbitabs_u12)(NI h_p0, NI maxHash_p1) {
	NI result;
	result = (NI)((NI)(h_p0 + ((NI)1)) & maxHash_p1);
	return result;
}
static N_INLINE(NIM_BOOL, mustRehash__icZbitabs_u56)(NI length_p0, NI counter_p1) {
	NIM_BOOL result;
	NIM_BOOL T1_;
	T1_ = (NIM_BOOL)0;
	T1_ = ((NI)(length_p0 * ((NI)2)) < (NI)(counter_p1 * ((NI)3)));
	if (T1_) goto LA2_;
	T1_ = ((NI)(length_p0 - counter_p1) < ((NI)4));
LA2_: ;
	result = T1_;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, enlarge__icZic_u1565)(tyObject_BiTable__KR9c7QZguPO3OKZOIcphgcA* t_p0) {
	tySequence__9bAGqSvkAaFL9bWjsEPslrFA n;
	NI T1_;
	tySequence__9bAGqSvkAaFL9bWjsEPslrFA T2_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	n.len = 0; n.p = NIM_NIL;
	T1_ = (*t_p0).keys.len;
	newSeq__icZic_u1572((&n), ((NI)(T1_ * ((NI)2))));
	T2_.len = 0; T2_.p = NIM_NIL;
	T2_ = (*t_p0).keys;
	(*t_p0).keys = n;
	n = T2_;
	{
		NI i;
		NI colontmp_;
		NI T4_;
		NI res;
		i = (NI)0;
		colontmp_ = (NI)0;
		T4_ = (n.len-1);
		colontmp_ = T4_;
		res = ((NI)0);
		{
			while (1) {
				NU32 eh;
				if (!(res <= colontmp_)) goto LA6;
				i = ((NI) (res));
				eh = n.p->data[i];
				{
					NI j;
					NI T11_;
					NI T12_;
					if (!((NU32)(((NU32)0)) < (NU32)(eh))) goto LA9_;
					T11_ = (NI)0;
					T11_ = hash__pureZhashes_u593((*t_p0).vals.p->data[(NI)(((NI) (eh)) - ((NI)1))]);
					if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
					T12_ = ((*t_p0).keys.len-1);
					j = (NI)(T11_ & T12_);
					{
						while (1) {
							NI T15_;
							if (!((NU32)(((NU32)0)) < (NU32)((*t_p0).keys.p->data[j]))) goto LA14;
							T15_ = ((*t_p0).keys.len-1);
							j = nextTry__icZbitabs_u12(j, T15_);
							if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
						} LA14: ;
					}
					(*t_p0).keys.p->data[j] = n.p->data[i];
					eqwasMoved___icZiclineinfos_u100((&n.p->data[i]));
				}
LA9_: ;
				res += ((NI)1);
			} LA6: ;
		}
	}
	eqdestroy___icZic_u1677(n);
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(NU32, getOrIncl__icZic_u1540)(tyObject_BiTable__KR9c7QZguPO3OKZOIcphgcA* t_p0, NimStringV2 v_p1) {
	NU32 result;
	NimStringV2 colontmpD_;
	NI origH;
	NI h;
	NI T1_;
	NI T35_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NU32)0;
	colontmpD_.len = 0; colontmpD_.p = NIM_NIL;
	origH = hash__pureZhashes_u593(v_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	T1_ = ((*t_p0).keys.len-1);
	h = (NI)(origH & T1_);
	{
		NI T4_;
		T4_ = (*t_p0).keys.len;
		if (!!((T4_ == ((NI)0)))) goto LA5_;
		{
			while (1) {
				NU32 litId;
				NI T17_;
				litId = (*t_p0).keys.p->data[h];
				{
					if (!!(((NU32)(((NU32)0)) < (NU32)(litId)))) goto LA11_;
					goto LA7;
				}
LA11_: ;
				{
					if (!eqStrings((*t_p0).vals.p->data[(NI)(((NI) ((*t_p0).keys.p->data[h])) - ((NI)1))], v_p1)) goto LA15_;
					result = litId;
					goto BeforeRet_;
				}
LA15_: ;
				T17_ = ((*t_p0).keys.len-1);
				h = nextTry__icZbitabs_u12(h, T17_);
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			}
		} LA7: ;
		{
			NI T20_;
			NI T21_;
			NIM_BOOL T22_;
			NI T25_;
			T20_ = (*t_p0).keys.len;
			T21_ = (*t_p0).vals.len;
			T22_ = (NIM_BOOL)0;
			T22_ = mustRehash__icZbitabs_u56(T20_, T21_);
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			if (!T22_) goto LA23_;
			enlarge__icZic_u1565(t_p0);
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			T25_ = ((*t_p0).keys.len-1);
			h = (NI)(origH & T25_);
			{
				while (1) {
					NU32 litId_2;
					NI T32_;
					litId_2 = (*t_p0).keys.p->data[h];
					{
						if (!!(((NU32)(((NU32)0)) < (NU32)(litId_2)))) goto LA30_;
						goto LA26;
					}
LA30_: ;
					T32_ = ((*t_p0).keys.len-1);
					h = nextTry__icZbitabs_u12(h, T32_);
					if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
				}
			} LA26: ;
		}
LA23_: ;
	}
	goto LA2_;
LA5_: ;
	{
		NI T34_;
		setLen__icZic_u1580((&(*t_p0).keys), ((NI)16));
		T34_ = ((*t_p0).keys.len-1);
		h = (NI)(origH & T34_);
	}
LA2_: ;
	T35_ = (*t_p0).vals.len;
	result = ((NU32) ((NI)(T35_ + ((NI)1))));
	(*t_p0).keys.p->data[h] = result;
	colontmpD_ = eqdup___system_u2603(v_p1);
	add__stdZenumutils_u71((&(*t_p0).vals), colontmpD_);
	}BeforeRet_: ;
	return result;
}
static N_INLINE(NU64, hiXorLoFallback64__pureZhashes_u36)(NU64 a_p0, NU64 b_p1) {
	NU64 result;
	NU64 colontmpD_;
	NU64 colontmpD__2;
	NU64 colontmpD__3;
	NU64 colontmpD__4;
	NU64 aH;
	NU64 aL;
	NU64 bH;
	NU64 bL;
	NU64 rHH;
	NU64 rHL;
	NU64 rLH;
	NU64 rLL;
	NU64 t;
	NU64 c;
	NU64 lo;
	NU64 T6_;
	NU64 hi;
{	colontmpD_ = (NU64)0;
	colontmpD__2 = (NU64)0;
	colontmpD__3 = (NU64)0;
	colontmpD__4 = (NU64)0;
	aH = (NU64)((NU64)(a_p0) >> (NU64)(((NI)32)));
	aL = (NU64)(a_p0 & 4294967295ULL);
	bH = (NU64)((NU64)(b_p1) >> (NU64)(((NI)32)));
	bL = (NU64)(b_p1 & 4294967295ULL);
	rHH = (NU64)((NU64)(aH) * (NU64)(bH));
	rHL = (NU64)((NU64)(aH) * (NU64)(bL));
	rLH = (NU64)((NU64)(aL) * (NU64)(bH));
	rLL = (NU64)((NU64)(aL) * (NU64)(bL));
	t = (NU64)((NU64)(rLL) + (NU64)((NU64)((NU64)(rHL) << (NU64)(((NI)32)))));
	{
		if (!((NU64)(t) < (NU64)(rLL))) goto LA3_;
		colontmpD_ = 1ULL;
		c = colontmpD_;
	}
	goto LA1_;
LA3_: ;
	{
		colontmpD__2 = 0ULL;
		c = colontmpD__2;
	}
LA1_: ;
	lo = (NU64)((NU64)(t) + (NU64)((NU64)((NU64)(rLH) << (NU64)(((NI)32)))));
	T6_ = (NU64)0;
	{
		if (!((NU64)(lo) < (NU64)(t))) goto LA9_;
		colontmpD__3 = 1ULL;
		T6_ = colontmpD__3;
	}
	goto LA7_;
LA9_: ;
	{
		colontmpD__4 = 0ULL;
		T6_ = colontmpD__4;
	}
LA7_: ;
	c += T6_;
	hi = (NU64)((NU64)((NU64)((NU64)((NU64)((NU64)(rHH) + (NU64)((NU64)((NU64)(rHL) >> (NU64)(((NI)32)))))) + (NU64)((NU64)((NU64)(rLH) >> (NU64)(((NI)32)))))) + (NU64)(c));
	result = (NU64)(hi ^ lo);
	goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
static N_INLINE(NU64, hiXorLo__pureZhashes_u80)(NU64 a_p0, NU64 b_p1) {
	NU64 result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NU64)0;
	result = hiXorLoFallback64__pureZhashes_u36(a_p0, b_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
static N_INLINE(NI, hashWangYi1__pureZhashes_u139)(NU64 x_p0) {
	NI result;
	NU64 T1_;
	NU64 T2_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NI)0;
	T1_ = (NU64)0;
	T1_ = hiXorLo__pureZhashes_u80(11562461410679940143ULL, (NU64)(x_p0 ^ 16646288086500911323ULL));
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	T2_ = (NU64)0;
	T2_ = hiXorLo__pureZhashes_u80(T1_, 16952864883938283885ULL);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	result = ((NI) (T2_));
	}BeforeRet_: ;
	return result;
}
static N_INLINE(NI, hash__pureZjson_u3365)(NI64 x_p0) {
	NI result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NI)0;
	result = hashWangYi1__pureZhashes_u139(((NU64) (x_p0)));
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, enlarge__icZic_u3374)(tyObject_BiTable__9aUXwPoJbNPqj9a249c0iSmlg* t_p0) {
	tySequence__9bAGqSvkAaFL9bWjsEPslrFA n;
	NI T1_;
	tySequence__9bAGqSvkAaFL9bWjsEPslrFA T2_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	n.len = 0; n.p = NIM_NIL;
	T1_ = (*t_p0).keys.len;
	newSeq__icZic_u1572((&n), ((NI)(T1_ * ((NI)2))));
	T2_.len = 0; T2_.p = NIM_NIL;
	T2_ = (*t_p0).keys;
	(*t_p0).keys = n;
	n = T2_;
	{
		NI i;
		NI colontmp_;
		NI T4_;
		NI res;
		i = (NI)0;
		colontmp_ = (NI)0;
		T4_ = (n.len-1);
		colontmp_ = T4_;
		res = ((NI)0);
		{
			while (1) {
				NU32 eh;
				if (!(res <= colontmp_)) goto LA6;
				i = ((NI) (res));
				eh = n.p->data[i];
				{
					NI j;
					NI T11_;
					NI T12_;
					if (!((NU32)(((NU32)0)) < (NU32)(eh))) goto LA9_;
					T11_ = (NI)0;
					T11_ = hash__pureZjson_u3365((*t_p0).vals.p->data[(NI)(((NI) (eh)) - ((NI)1))]);
					if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
					T12_ = ((*t_p0).keys.len-1);
					j = (NI)(T11_ & T12_);
					{
						while (1) {
							NI T15_;
							if (!((NU32)(((NU32)0)) < (NU32)((*t_p0).keys.p->data[j]))) goto LA14;
							T15_ = ((*t_p0).keys.len-1);
							j = nextTry__icZbitabs_u12(j, T15_);
							if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
						} LA14: ;
					}
					(*t_p0).keys.p->data[j] = n.p->data[i];
					eqwasMoved___icZiclineinfos_u100((&n.p->data[i]));
				}
LA9_: ;
				res += ((NI)1);
			} LA6: ;
		}
	}
	eqdestroy___icZic_u1677(n);
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(NU32, getOrIncl__icZic_u3346)(tyObject_BiTable__9aUXwPoJbNPqj9a249c0iSmlg* t_p0, NI64 v_p1) {
	NU32 result;
	NI64 colontmpD_;
	NI origH;
	NI h;
	NI T1_;
	NI T35_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NU32)0;
	colontmpD_ = (NI64)0;
	origH = hash__pureZjson_u3365(v_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	T1_ = ((*t_p0).keys.len-1);
	h = (NI)(origH & T1_);
	{
		NI T4_;
		T4_ = (*t_p0).keys.len;
		if (!!((T4_ == ((NI)0)))) goto LA5_;
		{
			while (1) {
				NU32 litId;
				NI T17_;
				litId = (*t_p0).keys.p->data[h];
				{
					if (!!(((NU32)(((NU32)0)) < (NU32)(litId)))) goto LA11_;
					goto LA7;
				}
LA11_: ;
				{
					if (!((*t_p0).vals.p->data[(NI)(((NI) ((*t_p0).keys.p->data[h])) - ((NI)1))] == v_p1)) goto LA15_;
					result = litId;
					goto BeforeRet_;
				}
LA15_: ;
				T17_ = ((*t_p0).keys.len-1);
				h = nextTry__icZbitabs_u12(h, T17_);
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			}
		} LA7: ;
		{
			NI T20_;
			NI T21_;
			NIM_BOOL T22_;
			NI T25_;
			T20_ = (*t_p0).keys.len;
			T21_ = (*t_p0).vals.len;
			T22_ = (NIM_BOOL)0;
			T22_ = mustRehash__icZbitabs_u56(T20_, T21_);
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			if (!T22_) goto LA23_;
			enlarge__icZic_u3374(t_p0);
			if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			T25_ = ((*t_p0).keys.len-1);
			h = (NI)(origH & T25_);
			{
				while (1) {
					NU32 litId_2;
					NI T32_;
					litId_2 = (*t_p0).keys.p->data[h];
					{
						if (!!(((NU32)(((NU32)0)) < (NU32)(litId_2)))) goto LA30_;
						goto LA26;
					}
LA30_: ;
					T32_ = ((*t_p0).keys.len-1);
					h = nextTry__icZbitabs_u12(h, T32_);
					if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
				}
			} LA26: ;
		}
LA23_: ;
	}
	goto LA2_;
LA5_: ;
	{
		NI T34_;
		setLen__icZic_u1580((&(*t_p0).keys), ((NI)16));
		T34_ = ((*t_p0).keys.len-1);
		h = (NI)(origH & T34_);
	}
LA2_: ;
	T35_ = (*t_p0).vals.len;
	result = ((NU32) ((NI)(T35_ + ((NI)1))));
	(*t_p0).keys.p->data[h] = result;
	colontmpD_ = v_p1;
	add__icZic_u3428((&(*t_p0).vals), colontmpD_);
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, store__icZic_u10170)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tyObject_BiTable__KR9c7QZguPO3OKZOIcphgcA t_p1) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	storeSeq__icZic_u10174(f_p0, t_p1.vals);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	storeSeq__icZic_u10207(f_p0, t_p1.keys);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, store__icZic_u10315)(tyObject_RodFile__k9byU8W8yLbzhHX06zPe4zg* f_p0, tyObject_BiTable__9aUXwPoJbNPqj9a249c0iSmlg t_p1) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	storeSeq__icZic_u10319(f_p0, t_p1.vals);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	storeSeq__icZic_u10207(f_p0, t_p1.keys);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(NU32, getKeyId__icZnavigator_u729)(tyObject_BiTable__KR9c7QZguPO3OKZOIcphgcA t_p0, NimStringV2 v_p1) {
	NU32 result;
	NI origH;
	NI h;
	NI T1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NU32)0;
	origH = hash__pureZhashes_u593(v_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	T1_ = (t_p0.keys.len-1);
	h = (NI)(origH & T1_);
	{
		NI T4_;
		T4_ = t_p0.keys.len;
		if (!!((T4_ == ((NI)0)))) goto LA5_;
		{
			while (1) {
				NU32 litId;
				NI T17_;
				litId = t_p0.keys.p->data[h];
				{
					if (!!(((NU32)(((NU32)0)) < (NU32)(litId)))) goto LA11_;
					goto LA7;
				}
LA11_: ;
				{
					if (!eqStrings(t_p0.vals.p->data[(NI)(((NI) (t_p0.keys.p->data[h])) - ((NI)1))], v_p1)) goto LA15_;
					result = litId;
					goto BeforeRet_;
				}
LA15_: ;
				T17_ = (t_p0.keys.len-1);
				h = nextTry__icZbitabs_u12(h, T17_);
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			}
		} LA7: ;
	}
LA5_: ;
	result = ((NU32)0);
	goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NI, len__icZic_u22072)(tyObject_BiTable__KR9c7QZguPO3OKZOIcphgcA t_p0) {
	NI result;
	NI T1_;
	T1_ = t_p0.vals.len;
	result = T1_;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NI, len__icZic_u22121)(tyObject_BiTable__9aUXwPoJbNPqj9a249c0iSmlg t_p0) {
	NI result;
	NI T1_;
	T1_ = t_p0.vals.len;
	result = T1_;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NI, sizeOnDisc__icZbitabs_u123)(tyObject_BiTable__KR9c7QZguPO3OKZOIcphgcA t_p0) {
	NI result;
	NI T5_;
	result = ((NI)4);
	{
		NimStringV2* x;
		NI i;
		NI L;
		NI T2_;
		x = (NimStringV2*)0;
		i = ((NI)0);
		T2_ = t_p0.vals.len;
		L = T2_;
		{
			while (1) {
				if (!(i < L)) goto LA4;
				x = (&t_p0.vals.p->data[i]);
				result += (NI)((*x).len + ((NI)4));
				i += ((NI)1);
			} LA4: ;
		}
	}
	T5_ = t_p0.keys.len;
	result += (NI)(T5_ * ((NI)4));
	return result;
}
