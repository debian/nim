/* Generated by Nim Compiler v2.2.0 */
#define NIM_INTBITS 64

#include "nimbase.h"
#include <stdio.h>
#undef LANGUAGE_C
#undef MIPSEB
#undef MIPSEL
#undef PPC
#undef R3000
#undef R4000
#undef i386
#undef linux
#undef mips
#undef near
#undef far
#undef powerpc
#undef unix
typedef struct tyObject_ConfigRefcolonObjectType___S9atckdgEHYT7qfQK9cvzJOg tyObject_ConfigRefcolonObjectType___S9atckdgEHYT7qfQK9cvzJOg;
typedef struct tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ;
typedef struct NimStrPayload NimStrPayload;
typedef struct NimStringV2 NimStringV2;
typedef struct tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg;
typedef struct tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA;
typedef struct tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg;
typedef struct tyObject_TIdent__LG4CHFyohryWo9bzZeyjDjA tyObject_TIdent__LG4CHFyohryWo9bzZeyjDjA;
typedef struct tySequence__9ahcSjIErJohSBBFLwuF56Q tySequence__9ahcSjIErJohSBBFLwuF56Q;
typedef struct tySequence__9ahcSjIErJohSBBFLwuF56Q_Content tySequence__9ahcSjIErJohSBBFLwuF56Q_Content;
typedef struct tyTuple__kN8up2W6YKc5YA9avn5mV5w tyTuple__kN8up2W6YKc5YA9avn5mV5w;
typedef struct tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg;
typedef struct RootObj RootObj;
typedef struct TNimTypeV2 TNimTypeV2;
struct NimStrPayload {
	NI cap;
	NIM_CHAR data[SEQ_DECL_SIZE];
};
struct NimStringV2 {
	NI len;
	NimStrPayload* p;
};
struct tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA {
	NU16 line;
	NI16 col;
	NI32 fileIndex;
};
typedef NU32 tySet_tyEnum_TNodeFlag__3UjGgL9bLM4RXBnoZJs9cviw;
typedef NU8 tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw;
struct tySequence__9ahcSjIErJohSBBFLwuF56Q {
  NI len; tySequence__9ahcSjIErJohSBBFLwuF56Q_Content* p;
};
struct tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ {
	tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg* typ;
	tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA info;
	tySet_tyEnum_TNodeFlag__3UjGgL9bLM4RXBnoZJs9cviw flags;
	tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw kind;
union{
	struct {
	NI64 intVal;
} _kind_1;
	struct {
	NF floatVal;
} _kind_2;
	struct {
	NimStringV2 strVal;
} _kind_3;
	struct {
	tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* sym;
} _kind_4;
	struct {
	tyObject_TIdent__LG4CHFyohryWo9bzZeyjDjA* ident;
} _kind_5;
	struct {
	tySequence__9ahcSjIErJohSBBFLwuF56Q sons;
} _kind_6;
};
};
typedef NU8 tyEnum_TMsgKind__Ug4VJQkHi8g218Fa9bQrePA;
typedef NU8 tyEnum_TErrorHandling__9aJ2MrE0UrBy3xCw3BT762w;
typedef NU16 tySet_tyEnum_TRenderFlag__Il2q467fyuki59ci9aspexFg;
struct tyTuple__kN8up2W6YKc5YA9avn5mV5w {
	NimStringV2 Field0;
	NI Field1;
	NI Field2;
};
struct tyObject_TIdent__LG4CHFyohryWo9bzZeyjDjA {
	NI id;
	NimStringV2 s;
	tyObject_TIdent__LG4CHFyohryWo9bzZeyjDjA* next;
	NI h;
};
struct TNimTypeV2 {
	void* destructor;
	NI size;
	NI16 align;
	NI16 depth;
	NU32* display;
	void* traceImpl;
	void* typeInfoV1;
	NI flags;
	void* vTable[SEQ_DECL_SIZE];
};
struct RootObj {
	TNimTypeV2* m_type;
};
typedef NU8 tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ;
typedef struct {
N_NIMCALL_PTR(NI, ClP_0) (tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* s_p0, void* buf_p1, NI bufLen_p2, void* ClE_0);
void* ClE_0;
} tyProc__WBr0JFZ9bbIGwzgqNYcBJtQ;
typedef struct {
N_NIMCALL_PTR(void, ClP_0) (void* ClE_0);
void* ClE_0;
} tyProc__HzVCwACFYM9cx9aV62PdjtuA;
struct tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg {
	RootObj Sup;
	tyEnum_TLLStreamKind__zJDOAevuyfVWFMJ82eilbQ kind;
	FILE* f;
	NimStringV2 s;
	NI rd;
	NI wr;
	NI lineOffset;
	tyProc__WBr0JFZ9bbIGwzgqNYcBJtQ repl;
	tyProc__HzVCwACFYM9cx9aV62PdjtuA onPrompt;
};
typedef NU8 tySet_tyChar__nmiMWKVIe46vacnhAFrQvw[32];
struct tySequence__9ahcSjIErJohSBBFLwuF56Q_Content { NI cap; tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* data[SEQ_DECL_SIZE]; };
N_LIB_PRIVATE N_NIMCALL(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ*, getArg__filters_u12)(tyObject_ConfigRefcolonObjectType___S9atckdgEHYT7qfQK9cvzJOg* conf_p0, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* n_p1, NimStringV2 name_p2, NI pos_p3);
static N_INLINE(NI, len__ast_u3163)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* n_p0);
N_LIB_PRIVATE N_NIMCALL(void, invalidPragma__filters_u9)(tyObject_ConfigRefcolonObjectType___S9atckdgEHYT7qfQK9cvzJOg* conf_p0, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* n_p1);
N_LIB_PRIVATE N_NOINLINE(void, liMessage__msgs_u1223)(tyObject_ConfigRefcolonObjectType___S9atckdgEHYT7qfQK9cvzJOg* conf_p0, tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA info_p1, tyEnum_TMsgKind__Ug4VJQkHi8g218Fa9bQrePA msg_p2, NimStringV2 arg_p3, tyEnum_TErrorHandling__9aJ2MrE0UrBy3xCw3BT762w eh_p4, tyTuple__kN8up2W6YKc5YA9avn5mV5w* info2_p5, NIM_BOOL isRaw_p6, NIM_BOOL ignoreError_p7);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, nsuFormatSingleElem)(NimStringV2 formatstr_p0, NimStringV2 a_p1);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, renderTree__renderer_u59)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* n_p0, tySet_tyEnum_TRenderFlag__Il2q467fyuki59ci9aspexFg renderFlags_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___system_u2597)(NimStringV2 dest_p0);
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void);
N_LIB_PRIVATE N_NIMCALL(NI, nsuCmpIgnoreStyle)(NimStringV2 a_p0, NimStringV2 b_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqcopy___ast_u3228)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ** dest_p0, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* src_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___ast_u3225)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* dest_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqcopy___system_u2600)(NimStringV2* dest_p0, NimStringV2 src_p1);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, strArg__filters_u55)(tyObject_ConfigRefcolonObjectType___S9atckdgEHYT7qfQK9cvzJOg* conf_p0, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* n_p1, NimStringV2 name_p2, NI pos_p3, NimStringV2 default_p4);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, boolArg__filters_u71)(tyObject_ConfigRefcolonObjectType___S9atckdgEHYT7qfQK9cvzJOg* conf_p0, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* n_p1, NimStringV2 name_p2, NI pos_p3, NIM_BOOL default_p4);
N_LIB_PRIVATE N_NIMCALL(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg*, llStreamOpen__llstream_u31)(NimStringV2 data_p0);
N_NIMCALL(NimStringV2, rawNewString)(NI cap_p0);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, llStreamReadLine__llstream_u305)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* s_p0, NimStringV2* line_p1);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, nsuStrip)(NimStringV2 s_p0, NIM_BOOL leading_p1, NIM_BOOL trailing_p2, tySet_tyChar__nmiMWKVIe46vacnhAFrQvw chars_p3);
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, nsuStartsWith)(NimStringV2 s_p0, NimStringV2 prefix_p1);
N_LIB_PRIVATE N_NIMCALL(void, llStreamWriteln__llstream_u337)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* s_p0, NimStringV2 data_p1);
N_LIB_PRIVATE N_NIMCALL(void, llStreamClose__llstream_u213)(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* s_p0);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, nsuReplaceStr)(NimStringV2 s_p0, NimStringV2 sub_p1, NimStringV2 by_p2);
static const struct {
  NI cap; NIM_CHAR data[21+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_2 = { 21 | NIM_STRLIT_FLAG, "\'$1\' not allowed here" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_3 = {21, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_2};
static const struct {
  NI cap; NIM_CHAR data[62+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_5 = { 62 | NIM_STRLIT_FLAG, "/home/runner/work/nightlies/nightlies/nim/compiler/filters.nim" };
static const struct {
  NI cap; NIM_CHAR data[0+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_6 = { 0 | NIM_STRLIT_FLAG, "" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_7 = {0, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_6};
static const struct {
  NI cap; NIM_CHAR data[10+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_8 = { 10 | NIM_STRLIT_FLAG, "startswith" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_9 = {10, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_8};
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_10 = {0, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_6};
static const struct {
  NI cap; NIM_CHAR data[4+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_11 = { 4 | NIM_STRLIT_FLAG, "true" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_12 = {4, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_11};
static const struct {
  NI cap; NIM_CHAR data[5+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_13 = { 5 | NIM_STRLIT_FLAG, "false" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_14 = {5, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_13};
static const struct {
  NI cap; NIM_CHAR data[7+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_15 = { 7 | NIM_STRLIT_FLAG, "leading" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_16 = {7, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_15};
static const struct {
  NI cap; NIM_CHAR data[8+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_17 = { 8 | NIM_STRLIT_FLAG, "trailing" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_18 = {8, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_17};
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_19 = {0, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_6};
static NIM_CONST tySet_tyChar__nmiMWKVIe46vacnhAFrQvw TM__TpC7tIfhGAaosc7HIsUuXA_20 = {
0x00, 0x3e, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
;
static const struct {
  NI cap; NIM_CHAR data[3+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_21 = { 3 | NIM_STRLIT_FLAG, "sub" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_22 = {3, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_21};
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_23 = {0, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_6};
static const struct {
  NI cap; NIM_CHAR data[2+1];
} TM__TpC7tIfhGAaosc7HIsUuXA_24 = { 2 | NIM_STRLIT_FLAG, "by" };
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_25 = {2, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_24};
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_26 = {0, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_6};
static const NimStringV2 TM__TpC7tIfhGAaosc7HIsUuXA_27 = {0, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_6};
static NIM_CONST tyTuple__kN8up2W6YKc5YA9avn5mV5w TM__TpC7tIfhGAaosc7HIsUuXA_4 = {{62, (NimStrPayload*)&TM__TpC7tIfhGAaosc7HIsUuXA_5},
((NI)19),
((NI)12)}
;
extern NIM_BOOL nimInErrorMode__system_u4310;
static N_INLINE(NI, len__ast_u3163)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* n_p0) {
	NI result;
	NI T1_;
	T1_ = (*n_p0)._kind_6.sons.len;
	result = T1_;
	return result;
}
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void) {
	NIM_BOOL* result;
	result = (&nimInErrorMode__system_u4310);
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, invalidPragma__filters_u9)(tyObject_ConfigRefcolonObjectType___S9atckdgEHYT7qfQK9cvzJOg* conf_p0, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* n_p1) {
	NimStringV2 colontmpD_;
	NimStringV2 T2_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	colontmpD_.len = 0; colontmpD_.p = NIM_NIL;
	T2_.len = 0; T2_.p = NIM_NIL;
	T2_ = renderTree__renderer_u59(n_p1, 4);
	if (NIM_UNLIKELY(*nimErr_)) {eqdestroy___system_u2597(T2_); goto LA1_;}
	colontmpD_ = nsuFormatSingleElem(TM__TpC7tIfhGAaosc7HIsUuXA_3, T2_);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	liMessage__msgs_u1223(conf_p0, (*n_p1).info, ((tyEnum_TMsgKind__Ug4VJQkHi8g218Fa9bQrePA)17), colontmpD_, ((tyEnum_TErrorHandling__9aJ2MrE0UrBy3xCw3BT762w)0), (&TM__TpC7tIfhGAaosc7HIsUuXA_4), NIM_FALSE, NIM_FALSE);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		LA1_:;
	}
	{
		eqdestroy___system_u2597(colontmpD_);
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ*, getArg__filters_u12)(tyObject_ConfigRefcolonObjectType___S9atckdgEHYT7qfQK9cvzJOg* conf_p0, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* n_p1, NimStringV2 name_p2, NI pos_p3) {
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = ((tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ*) NIM_NIL);
	{
		if (!((*n_p1).kind >= ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)1) && (*n_p1).kind <= ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)23))) goto LA3_;
		goto BeforeRet_;
	}
LA3_: ;
	{
		NI i;
		NI colontmp_;
		NI i_2;
		i = (NI)0;
		colontmp_ = (NI)0;
		colontmp_ = len__ast_u3163(n_p1);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		i_2 = ((NI)1);
		{
			while (1) {
				if (!(i_2 < colontmp_)) goto LA7;
				i = i_2;
				{
					if (!((*(*n_p1)._kind_6.sons.p->data[i]).kind == ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)33))) goto LA10_;
					{
						if (!!(((*(*(*n_p1)._kind_6.sons.p->data[i])._kind_6.sons.p->data[((NI)0)]).kind == ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)2)))) goto LA14_;
						invalidPragma__filters_u9(conf_p0, n_p1);
						if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
					}
LA14_: ;
					{
						NI T18_;
						T18_ = (NI)0;
						T18_ = nsuCmpIgnoreStyle((*(*(*(*n_p1)._kind_6.sons.p->data[i])._kind_6.sons.p->data[((NI)0)])._kind_5.ident).s, name_p2);
						if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
						if (!(T18_ == ((NI)0))) goto LA19_;
						eqcopy___ast_u3228(&result, (*(*n_p1)._kind_6.sons.p->data[i])._kind_6.sons.p->data[((NI)1)]);
						if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
						goto BeforeRet_;
					}
LA19_: ;
				}
				goto LA8_;
LA10_: ;
				{
					if (!(i == pos_p3)) goto LA22_;
					eqcopy___ast_u3228(&result, (*n_p1)._kind_6.sons.p->data[i]);
					if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
					goto BeforeRet_;
				}
				goto LA8_;
LA22_: ;
LA8_: ;
				i_2 += ((NI)1);
			} LA7: ;
		}
	}
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NIM_CHAR, charArg__filters_u35)(tyObject_ConfigRefcolonObjectType___S9atckdgEHYT7qfQK9cvzJOg* conf_p0, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* n_p1, NimStringV2 name_p2, NI pos_p3, NIM_CHAR default_p4) {
	NIM_CHAR result;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* x;
NIM_BOOL oldNimErrFin1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NIM_CHAR)0;
	x = NIM_NIL;
	x = getArg__filters_u12(conf_p0, n_p1, name_p2, pos_p3);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		if (!(x == ((tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ*) NIM_NIL))) goto LA4_;
		result = default_p4;
	}
	goto LA2_;
LA4_: ;
	{
		if (!((*x).kind == ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)5))) goto LA7_;
		result = ((NIM_CHAR) ((((NI) ((*x)._kind_1.intVal)))));
	}
	goto LA2_;
LA7_: ;
	{
		result = 0;
		invalidPragma__filters_u9(conf_p0, n_p1);
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	}
LA2_: ;
	{
		LA1_:;
	}
	{
		oldNimErrFin1_ = *nimErr_; *nimErr_ = NIM_FALSE;
		eqdestroy___ast_u3225(x);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		*nimErr_ = oldNimErrFin1_;
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NimStringV2, strArg__filters_u55)(tyObject_ConfigRefcolonObjectType___S9atckdgEHYT7qfQK9cvzJOg* conf_p0, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* n_p1, NimStringV2 name_p2, NI pos_p3, NimStringV2 default_p4) {
	NimStringV2 result;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* x;
NIM_BOOL oldNimErrFin1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result.len = 0; result.p = NIM_NIL;
	x = NIM_NIL;
	x = getArg__filters_u12(conf_p0, n_p1, name_p2, pos_p3);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		if (!(x == ((tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ*) NIM_NIL))) goto LA4_;
		eqcopy___system_u2600((&result), default_p4);
	}
	goto LA2_;
LA4_: ;
	{
		if (!((*x).kind >= ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)20) && (*x).kind <= ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)22))) goto LA7_;
		eqcopy___system_u2600((&result), (*x)._kind_3.strVal);
	}
	goto LA2_;
LA7_: ;
	{
		result = TM__TpC7tIfhGAaosc7HIsUuXA_7;
		invalidPragma__filters_u9(conf_p0, n_p1);
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	}
LA2_: ;
	{
		LA1_:;
	}
	{
		oldNimErrFin1_ = *nimErr_; *nimErr_ = NIM_FALSE;
		eqdestroy___ast_u3225(x);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		*nimErr_ = oldNimErrFin1_;
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, boolArg__filters_u71)(tyObject_ConfigRefcolonObjectType___S9atckdgEHYT7qfQK9cvzJOg* conf_p0, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* n_p1, NimStringV2 name_p2, NI pos_p3, NIM_BOOL default_p4) {
	NIM_BOOL result;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* x;
NIM_BOOL oldNimErrFin1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NIM_BOOL)0;
	x = NIM_NIL;
	x = getArg__filters_u12(conf_p0, n_p1, name_p2, pos_p3);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		if (!(x == ((tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ*) NIM_NIL))) goto LA4_;
		result = default_p4;
	}
	goto LA2_;
LA4_: ;
	{
		NIM_BOOL T7_;
		NI T9_;
		T7_ = (NIM_BOOL)0;
		T7_ = ((*x).kind == ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)2));
		if (!(T7_)) goto LA8_;
		T9_ = (NI)0;
		T9_ = nsuCmpIgnoreStyle((*(*x)._kind_5.ident).s, TM__TpC7tIfhGAaosc7HIsUuXA_12);
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
		T7_ = (T9_ == ((NI)0));
LA8_: ;
		if (!T7_) goto LA10_;
		result = NIM_TRUE;
	}
	goto LA2_;
LA10_: ;
	{
		NIM_BOOL T13_;
		NI T15_;
		T13_ = (NIM_BOOL)0;
		T13_ = ((*x).kind == ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)2));
		if (!(T13_)) goto LA14_;
		T15_ = (NI)0;
		T15_ = nsuCmpIgnoreStyle((*(*x)._kind_5.ident).s, TM__TpC7tIfhGAaosc7HIsUuXA_14);
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
		T13_ = (T15_ == ((NI)0));
LA14_: ;
		if (!T13_) goto LA16_;
		result = NIM_FALSE;
	}
	goto LA2_;
LA16_: ;
	{
		result = NIM_FALSE;
		invalidPragma__filters_u9(conf_p0, n_p1);
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	}
LA2_: ;
	{
		LA1_:;
	}
	{
		oldNimErrFin1_ = *nimErr_; *nimErr_ = NIM_FALSE;
		eqdestroy___ast_u3225(x);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		*nimErr_ = oldNimErrFin1_;
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg*, filterStrip__filters_u91)(tyObject_ConfigRefcolonObjectType___S9atckdgEHYT7qfQK9cvzJOg* conf_p0, tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* stdin_p1, NimStringV2 filename_p2, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* call_p3) {
	tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* result;
	NimStringV2 pattern;
	NimStringV2 line;
	NIM_BOOL leading;
	NIM_BOOL trailing;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = NIM_NIL;
	pattern.len = 0; pattern.p = NIM_NIL;
	line.len = 0; line.p = NIM_NIL;
	pattern = strArg__filters_u55(conf_p0, call_p3, TM__TpC7tIfhGAaosc7HIsUuXA_9, ((NI)1), TM__TpC7tIfhGAaosc7HIsUuXA_10);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	leading = boolArg__filters_u71(conf_p0, call_p3, TM__TpC7tIfhGAaosc7HIsUuXA_16, ((NI)2), NIM_TRUE);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	trailing = boolArg__filters_u71(conf_p0, call_p3, TM__TpC7tIfhGAaosc7HIsUuXA_18, ((NI)3), NIM_TRUE);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	result = llStreamOpen__llstream_u31(TM__TpC7tIfhGAaosc7HIsUuXA_19);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	line = rawNewString(((NI)80));
	{
		while (1) {
			NIM_BOOL T4_;
			NimStringV2 stripped;
			T4_ = (NIM_BOOL)0;
			T4_ = llStreamReadLine__llstream_u305(stdin_p1, (&line));
			if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
			if (!T4_) goto LA3;
			stripped.len = 0; stripped.p = NIM_NIL;
			stripped = nsuStrip(line, leading, trailing, TM__TpC7tIfhGAaosc7HIsUuXA_20);
			if (NIM_UNLIKELY(*nimErr_)) goto LA5_;
			{
				NIM_BOOL T8_;
				T8_ = (NIM_BOOL)0;
				T8_ = (pattern.len == ((NI)0));
				if (T8_) goto LA9_;
				T8_ = nsuStartsWith(stripped, pattern);
				if (NIM_UNLIKELY(*nimErr_)) goto LA5_;
LA9_: ;
				if (!T8_) goto LA10_;
				llStreamWriteln__llstream_u337(result, stripped);
				if (NIM_UNLIKELY(*nimErr_)) goto LA5_;
			}
			goto LA6_;
LA10_: ;
			{
				llStreamWriteln__llstream_u337(result, line);
				if (NIM_UNLIKELY(*nimErr_)) goto LA5_;
			}
LA6_: ;
			{
				LA5_:;
			}
			{
				eqdestroy___system_u2597(stripped);
			}
			if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
		} LA3: ;
	}
	llStreamClose__llstream_u213(stdin_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		LA1_:;
	}
	{
		eqdestroy___system_u2597(line);
		eqdestroy___system_u2597(pattern);
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg*, filterReplace__filters_u102)(tyObject_ConfigRefcolonObjectType___S9atckdgEHYT7qfQK9cvzJOg* conf_p0, tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* stdin_p1, NimStringV2 filename_p2, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* call_p3) {
	tyObject_TLLStream__FoDsO8lsQbcCVaLehnuOYg* result;
	NimStringV2 sub;
	NimStringV2 by;
	NimStringV2 line;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = NIM_NIL;
	sub.len = 0; sub.p = NIM_NIL;
	by.len = 0; by.p = NIM_NIL;
	line.len = 0; line.p = NIM_NIL;
	sub = strArg__filters_u55(conf_p0, call_p3, TM__TpC7tIfhGAaosc7HIsUuXA_22, ((NI)1), TM__TpC7tIfhGAaosc7HIsUuXA_23);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		if (!(sub.len == ((NI)0))) goto LA4_;
		invalidPragma__filters_u9(conf_p0, call_p3);
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	}
LA4_: ;
	by = strArg__filters_u55(conf_p0, call_p3, TM__TpC7tIfhGAaosc7HIsUuXA_25, ((NI)2), TM__TpC7tIfhGAaosc7HIsUuXA_26);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	result = llStreamOpen__llstream_u31(TM__TpC7tIfhGAaosc7HIsUuXA_27);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	line = rawNewString(((NI)80));
	{
		while (1) {
			NIM_BOOL T8_;
			NimStringV2 colontmpD_;
			T8_ = (NIM_BOOL)0;
			T8_ = llStreamReadLine__llstream_u305(stdin_p1, (&line));
			if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
			if (!T8_) goto LA7;
			colontmpD_.len = 0; colontmpD_.p = NIM_NIL;
			colontmpD_ = nsuReplaceStr(line, sub, by);
			if (NIM_UNLIKELY(*nimErr_)) goto LA9_;
			llStreamWriteln__llstream_u337(result, colontmpD_);
			if (NIM_UNLIKELY(*nimErr_)) goto LA9_;
			{
				LA9_:;
			}
			{
				eqdestroy___system_u2597(colontmpD_);
			}
			if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
		} LA7: ;
	}
	llStreamClose__llstream_u213(stdin_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		LA1_:;
	}
	{
		eqdestroy___system_u2597(line);
		eqdestroy___system_u2597(by);
		eqdestroy___system_u2597(sub);
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
