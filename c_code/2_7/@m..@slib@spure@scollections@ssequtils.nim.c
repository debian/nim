/* Generated by Nim Compiler v2.2.0 */
#define NIM_INTBITS 32
#define NIM_EmulateOverflowChecks

#include "nimbase.h"
#undef LANGUAGE_C
#undef MIPSEB
#undef MIPSEL
#undef PPC
#undef R3000
#undef R4000
#undef i386
#undef linux
#undef mips
#undef near
#undef far
#undef powerpc
#undef unix
typedef struct tySequence__qwqHTkRvwhrRyENtudHQ7g tySequence__qwqHTkRvwhrRyENtudHQ7g;
typedef struct tySequence__qwqHTkRvwhrRyENtudHQ7g_Content tySequence__qwqHTkRvwhrRyENtudHQ7g_Content;
struct tySequence__qwqHTkRvwhrRyENtudHQ7g {
  NI len; tySequence__qwqHTkRvwhrRyENtudHQ7g_Content* p;
};
struct tySequence__qwqHTkRvwhrRyENtudHQ7g_Content { NI cap; NI data[SEQ_DECL_SIZE]; };
N_LIB_PRIVATE N_NIMCALL(void, add__varpartitions_u1531)(tySequence__qwqHTkRvwhrRyENtudHQ7g* x_p0, NI y_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqwasMoved___stdZenumutils_u116)(NI* dest_p0);
N_LIB_PRIVATE N_NIMCALL(void, addUnique__importer_u3309)(tySequence__qwqHTkRvwhrRyENtudHQ7g* s_p0, NI x_p1) {
	NI blitTmp;
{	{
		NI i;
		NI colontmp_;
		NI T2_;
		NI res;
		i = (NI)0;
		colontmp_ = (NI)0;
		T2_ = ((*s_p0).len-1);
		colontmp_ = T2_;
		res = ((NI)0);
		{
			while (1) {
				if (!(res <= colontmp_)) goto LA4;
				i = ((NI) (res));
				{
					if (!((*s_p0).p->data[i] == x_p1)) goto LA7_;
					goto BeforeRet_;
				}
LA7_: ;
				res += ((NI)1);
			} LA4: ;
		}
	}
	blitTmp = x_p1;
	eqwasMoved___stdZenumutils_u116((&x_p1));
	add__varpartitions_u1531((&(*s_p0)), blitTmp);
	}BeforeRet_: ;
}
