/* Generated by Nim Compiler v2.2.0 */
#define NIM_INTBITS 64

#include "nimbase.h"
#include <string.h>
#include <sys/types.h>
#undef LANGUAGE_C
#undef MIPSEB
#undef MIPSEL
#undef PPC
#undef R3000
#undef R4000
#undef i386
#undef linux
#undef mips
#undef near
#undef far
#undef powerpc
#undef unix
typedef struct NimStrPayload NimStrPayload;
typedef struct NimStringV2 NimStringV2;
typedef struct tyObject_ProcessObj__KkmVhVBIhdGPRUKkZJq9cLQ tyObject_ProcessObj__KkmVhVBIhdGPRUKkZJq9cLQ;
typedef struct RootObj RootObj;
typedef struct TNimTypeV2 TNimTypeV2;
typedef struct tyObject_StreamObj__nX3kZRlh8Dq3gQ2cC6DE8w tyObject_StreamObj__nX3kZRlh8Dq3gQ2cC6DE8w;
typedef struct tyObject_StringTableObj__eKsdjq89aDBcvVY68b206Qg tyObject_StringTableObj__eKsdjq89aDBcvVY68b206Qg;
typedef struct Exception Exception;
typedef struct tySequence__S89cE01OvNjsN9c71FKsSrqQ tySequence__S89cE01OvNjsN9c71FKsSrqQ;
typedef struct tySequence__S89cE01OvNjsN9c71FKsSrqQ_Content tySequence__S89cE01OvNjsN9c71FKsSrqQ_Content;
typedef struct tyObject_StackTraceEntry__RcJz5RXFIYjIseutvVFICA tyObject_StackTraceEntry__RcJz5RXFIYjIseutvVFICA;
struct NimStrPayload {
	NI cap;
	NIM_CHAR data[SEQ_DECL_SIZE];
};
struct NimStringV2 {
	NI len;
	NimStrPayload* p;
};
struct TNimTypeV2 {
	void* destructor;
	NI size;
	NI16 align;
	NI16 depth;
	NU32* display;
	void* traceImpl;
	void* typeInfoV1;
	NI flags;
	void* vTable[SEQ_DECL_SIZE];
};
struct RootObj {
	TNimTypeV2* m_type;
};
typedef NU8 tySet_tyEnum_ProcessOption__f8zQPCJAq3Y51oMx2CR9cxw;
struct tyObject_ProcessObj__KkmVhVBIhdGPRUKkZJq9cLQ {
	RootObj Sup;
	int inHandle;
	int outHandle;
	int errHandle;
	pid_t id;
	tyObject_StreamObj__nX3kZRlh8Dq3gQ2cC6DE8w* inStream;
	tyObject_StreamObj__nX3kZRlh8Dq3gQ2cC6DE8w* outStream;
	tyObject_StreamObj__nX3kZRlh8Dq3gQ2cC6DE8w* errStream;
	int exitStatus;
	NIM_BOOL exitFlag;
	tySet_tyEnum_ProcessOption__f8zQPCJAq3Y51oMx2CR9cxw options;
};
typedef NimStringV2 tyArray__nHXaesL0DJZHyVS07ARPRA[1];
struct tySequence__S89cE01OvNjsN9c71FKsSrqQ {
  NI len; tySequence__S89cE01OvNjsN9c71FKsSrqQ_Content* p;
};
struct Exception {
	RootObj Sup;
	Exception* parent;
	NCSTRING name;
	NimStringV2 message;
	tySequence__S89cE01OvNjsN9c71FKsSrqQ trace;
	Exception* up;
};
struct tyObject_StackTraceEntry__RcJz5RXFIYjIseutvVFICA {
	NCSTRING procname;
	NI line;
	NCSTRING filename;
};
struct tySequence__S89cE01OvNjsN9c71FKsSrqQ_Content { NI cap; tyObject_StackTraceEntry__RcJz5RXFIYjIseutvVFICA data[SEQ_DECL_SIZE]; };
N_LIB_PRIVATE N_NIMCALL(void, failedAssertImpl__stdZassertions_u242)(NimStringV2 msg_p0);
N_LIB_PRIVATE N_NIMCALL(void, openDefaultBrowserRaw__pureZbrowsers_u10)(NimStringV2 url_p0);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, nospquoteShell)(NimStringV2 s_p0);
N_LIB_PRIVATE N_NIMCALL(NI, nosexecShellCmd)(NimStringV2 command_p0);
static N_INLINE(void, appendString)(NimStringV2* dest_p0, NimStringV2 src_p1);
static N_INLINE(void, copyMem__system_u1713)(void* dest_p0, void* source_p1, NI size_p2);
static N_INLINE(void, nimCopyMem)(void* dest_p0, void* source_p1, NI size_p2);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, rawNewString)(NI space_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___system_u2597)(NimStringV2 dest_p0);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, getEnv__stdZenvvars_u15)(NimStringV2 key_p0, NimStringV2 default_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqsink___system_u2606)(NimStringV2* dest_p0, NimStringV2 src_p1);
N_LIB_PRIVATE N_NIMCALL(NimStringV2, substr__system_u8370)(NimStringV2 s_p0, NI first_p1, NI last_p2);
N_LIB_PRIVATE N_NIMCALL(tyObject_ProcessObj__KkmVhVBIhdGPRUKkZJq9cLQ*, nospstartProcess)(NimStringV2 command_p0, NimStringV2 workingDir_p1, NimStringV2* args_p2, NI args_p2Len_0, tyObject_StringTableObj__eKsdjq89aDBcvVY68b206Qg* env_p3, tySet_tyEnum_ProcessOption__f8zQPCJAq3Y51oMx2CR9cxw options_p4);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___pureZosproc_u216)(tyObject_ProcessObj__KkmVhVBIhdGPRUKkZJq9cLQ* dest_p0);
static N_INLINE(NIM_BOOL, isObjDisplayCheck)(TNimTypeV2* source_p0, NI16 targetDepth_p1, NU32 token_p2);
static N_INLINE(Exception*, nimBorrowCurrentException)(void);
static N_INLINE(void, popCurrentException)(void);
N_LIB_PRIVATE N_NIMCALL(void, eqcopy___stdZassertions_u22)(Exception** dest_p0, Exception* src_p1, NIM_BOOL cyclic_p2);
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void);
static const struct {
  NI cap; NIM_CHAR data[62+1];
} TM__CZNeKGdQ8AWxD1HSDIq5DQ_2 = { 62 | NIM_STRLIT_FLAG, "browsers.nim(77, 3) `url.len > 0` URL must not be empty string" };
static const NimStringV2 TM__CZNeKGdQ8AWxD1HSDIq5DQ_3 = {62, (NimStrPayload*)&TM__CZNeKGdQ8AWxD1HSDIq5DQ_2};
static const struct {
  NI cap; NIM_CHAR data[9+1];
} TM__CZNeKGdQ8AWxD1HSDIq5DQ_4 = { 9 | NIM_STRLIT_FLAG, "xdg-open " };
static const NimStringV2 TM__CZNeKGdQ8AWxD1HSDIq5DQ_5 = {9, (NimStrPayload*)&TM__CZNeKGdQ8AWxD1HSDIq5DQ_4};
static const struct {
  NI cap; NIM_CHAR data[7+1];
} TM__CZNeKGdQ8AWxD1HSDIq5DQ_6 = { 7 | NIM_STRLIT_FLAG, "BROWSER" };
static const NimStringV2 TM__CZNeKGdQ8AWxD1HSDIq5DQ_7 = {7, (NimStrPayload*)&TM__CZNeKGdQ8AWxD1HSDIq5DQ_6};
static const struct {
  NI cap; NIM_CHAR data[0+1];
} TM__CZNeKGdQ8AWxD1HSDIq5DQ_8 = { 0 | NIM_STRLIT_FLAG, "" };
static const NimStringV2 TM__CZNeKGdQ8AWxD1HSDIq5DQ_9 = {0, (NimStrPayload*)&TM__CZNeKGdQ8AWxD1HSDIq5DQ_8};
static const NimStringV2 TM__CZNeKGdQ8AWxD1HSDIq5DQ_10 = {0, (NimStrPayload*)&TM__CZNeKGdQ8AWxD1HSDIq5DQ_8};
extern Exception* currException__system_u3980;
extern NIM_BOOL nimInErrorMode__system_u4276;
static N_INLINE(void, nimCopyMem)(void* dest_p0, void* source_p1, NI size_p2) {
	void* T1_;
	T1_ = (void*)0;
	T1_ = memcpy(dest_p0, source_p1, ((size_t) (size_p2)));
}
static N_INLINE(void, copyMem__system_u1713)(void* dest_p0, void* source_p1, NI size_p2) {
	nimCopyMem(dest_p0, source_p1, size_p2);
}
static N_INLINE(void, appendString)(NimStringV2* dest_p0, NimStringV2 src_p1) {
	{
		if (!(((NI)0) < src_p1.len)) goto LA3_;
		copyMem__system_u1713(((void*) ((&(*(*dest_p0).p).data[(*dest_p0).len]))), ((void*) ((&(*src_p1.p).data[((NI)0)]))), ((NI)(src_p1.len + ((NI)1))));
		(*dest_p0).len += src_p1.len;
	}
LA3_: ;
}
static N_INLINE(NIM_BOOL, isObjDisplayCheck)(TNimTypeV2* source_p0, NI16 targetDepth_p1, NU32 token_p2) {
	NIM_BOOL result;
	NIM_BOOL T1_;
	T1_ = (NIM_BOOL)0;
	T1_ = (targetDepth_p1 <= (*source_p0).depth);
	if (!(T1_)) goto LA2_;
	T1_ = ((*source_p0).display[targetDepth_p1] == token_p2);
LA2_: ;
	result = T1_;
	return result;
}
static N_INLINE(Exception*, nimBorrowCurrentException)(void) {
	Exception* result;
	result = currException__system_u3980;
	return result;
}
static N_INLINE(void, popCurrentException)(void) {
	eqcopy___stdZassertions_u22(&currException__system_u3980, (*currException__system_u3980).up, NIM_FALSE);
}
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void) {
	NIM_BOOL* result;
	result = (&nimInErrorMode__system_u4276);
	return result;
}
N_LIB_PRIVATE N_NIMCALL(void, openDefaultBrowserRaw__pureZbrowsers_u10)(NimStringV2 url_p0) {
	NimStringV2 u;
	NimStringV2 colontmpD_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	u.len = 0; u.p = NIM_NIL;
	colontmpD_.len = 0; colontmpD_.p = NIM_NIL;
	u = nospquoteShell(url_p0);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		NimStringV2 T4_;
		NI T5_;
		T4_.len = 0; T4_.p = NIM_NIL;
		T4_ = rawNewString(u.len + 9);
appendString((&T4_), TM__CZNeKGdQ8AWxD1HSDIq5DQ_5);
appendString((&T4_), u);
		colontmpD_ = T4_;
		T5_ = (NI)0;
		T5_ = nosexecShellCmd(colontmpD_);
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
		if (!(T5_ == ((NI)0))) goto LA6_;
		eqdestroy___system_u2597(colontmpD_);
		eqdestroy___system_u2597(u);
		goto BeforeRet_;
	}
LA6_: ;
	{
		NimStringV2 b;
		NimStringV2 colontmp_;
		NI lastX60gensym34_;
		NI splitsX60gensym34_;
		b.len = 0; b.p = NIM_NIL;
		colontmp_.len = 0; colontmp_.p = NIM_NIL;
		colontmp_ = getEnv__stdZenvvars_u15(TM__CZNeKGdQ8AWxD1HSDIq5DQ_7, TM__CZNeKGdQ8AWxD1HSDIq5DQ_9);
		if (NIM_UNLIKELY(*nimErr_)) goto LA9_;
		lastX60gensym34_ = ((NI)0);
		splitsX60gensym34_ = ((NI)-1);
		{
			while (1) {
				NI firstX60gensym34_;
				NimStringV2 T20_;
				tyObject_ProcessObj__KkmVhVBIhdGPRUKkZJq9cLQ* colontmpD__2;
				tyArray__nHXaesL0DJZHyVS07ARPRA T23_;
				tyObject_ProcessObj__KkmVhVBIhdGPRUKkZJq9cLQ* T24_;
				if (!(lastX60gensym34_ <= colontmp_.len)) goto LA11;
				firstX60gensym34_ = lastX60gensym34_;
				{
					while (1) {
						NIM_BOOL T14_;
						T14_ = (NIM_BOOL)0;
						T14_ = (lastX60gensym34_ < colontmp_.len);
						if (!(T14_)) goto LA15_;
						T14_ = !(((NU8)(colontmp_.p->data[lastX60gensym34_]) == (NU8)(58)));
LA15_: ;
						if (!T14_) goto LA13;
						lastX60gensym34_ += ((NI)1);
					} LA13: ;
				}
				{
					if (!(splitsX60gensym34_ == ((NI)0))) goto LA18_;
					lastX60gensym34_ = colontmp_.len;
				}
LA18_: ;
				T20_.len = 0; T20_.p = NIM_NIL;
				T20_ = substr__system_u8370(colontmp_, firstX60gensym34_, (NI)(lastX60gensym34_ - ((NI)1)));
				eqsink___system_u2606((&b), T20_);
				colontmpD__2 = NIM_NIL;
				T23_[0] = url_p0;
				T24_ = NIM_NIL;
				T24_ = nospstartProcess(b, TM__CZNeKGdQ8AWxD1HSDIq5DQ_10, T23_, 1, ((tyObject_StringTableObj__eKsdjq89aDBcvVY68b206Qg*) NIM_NIL), 2);
				if (NIM_UNLIKELY(*nimErr_)) {eqdestroy___pureZosproc_u216(T24_); goto LA22_;}
				colontmpD__2 = T24_;
				(void)(colontmpD__2);
				eqdestroy___pureZosproc_u216(colontmpD__2);
				eqdestroy___system_u2597(colontmp_);
				eqdestroy___system_u2597(b);
				eqdestroy___system_u2597(colontmpD_);
				eqdestroy___system_u2597(u);
				goto BeforeRet_;
				{
					LA22_:;
				}
				{
					eqdestroy___pureZosproc_u216(colontmpD__2);
				}
				if (NIM_UNLIKELY(*nimErr_)) goto LA21_;
				if (NIM_UNLIKELY(*nimErr_)) {
					LA21_:;
					if (isObjDisplayCheck(nimBorrowCurrentException()->Sup.m_type, 3, 3796386048)) {
						*nimErr_ = NIM_FALSE;
						popCurrentException();
						LA28_:;
					}
				}
				if (NIM_UNLIKELY(*nimErr_)) goto LA9_;
				{
					if (!(splitsX60gensym34_ == ((NI)0))) goto LA32_;
					goto LA10;
				}
LA32_: ;
				splitsX60gensym34_ -= ((NI)1);
				lastX60gensym34_ += ((NI)1);
			} LA11: ;
		} LA10: ;
		{
			LA9_:;
		}
		{
			eqdestroy___system_u2597(colontmp_);
			eqdestroy___system_u2597(b);
		}
		if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	}
	{
		LA1_:;
	}
	{
		eqdestroy___system_u2597(colontmpD_);
		eqdestroy___system_u2597(u);
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
N_LIB_PRIVATE N_NIMCALL(void, openDefaultBrowser__pureZbrowsers_u14)(NimStringV2 url_p0) {
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	{
		if (!!((((NI)0) < url_p0.len))) goto LA3_;
		failedAssertImpl__stdZassertions_u242(TM__CZNeKGdQ8AWxD1HSDIq5DQ_3);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}
LA3_: ;
	openDefaultBrowserRaw__pureZbrowsers_u10(url_p0);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
}
