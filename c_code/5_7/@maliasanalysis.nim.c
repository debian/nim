/* Generated by Nim Compiler v2.2.0 */
#define NIM_INTBITS 32
#define NIM_EmulateOverflowChecks

#include "nimbase.h"
#undef LANGUAGE_C
#undef MIPSEB
#undef MIPSEL
#undef PPC
#undef R3000
#undef R4000
#undef i386
#undef linux
#undef mips
#undef near
#undef far
#undef powerpc
#undef unix
typedef struct tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ;
typedef struct tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg;
typedef struct tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg;
typedef struct tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA;
typedef struct NimStrPayload NimStrPayload;
typedef struct NimStringV2 NimStringV2;
typedef struct tyObject_TIdent__LG4CHFyohryWo9bzZeyjDjA tyObject_TIdent__LG4CHFyohryWo9bzZeyjDjA;
typedef struct tySequence__9ahcSjIErJohSBBFLwuF56Q tySequence__9ahcSjIErJohSBBFLwuF56Q;
typedef struct tySequence__9ahcSjIErJohSBBFLwuF56Q_Content tySequence__9ahcSjIErJohSBBFLwuF56Q_Content;
typedef struct tyObject_ItemId__N3z7uIvKCrk0S9cej5ew0nA tyObject_ItemId__N3z7uIvKCrk0S9cej5ew0nA;
typedef struct tyObject_TLoc__KvQVlvKb6mbV567NQ0hMOw tyObject_TLoc__KvQVlvKb6mbV567NQ0hMOw;
typedef struct tyObject_TLib__sVuuRvQkiYNOUiPRZhmZig tyObject_TLib__sVuuRvQkiYNOUiPRZhmZig;
typedef struct tySequence__d8ZV4IMgHt9biik9cS9a0hrmg tySequence__d8ZV4IMgHt9biik9cS9a0hrmg;
typedef struct tySequence__d8ZV4IMgHt9biik9cS9a0hrmg_Content tySequence__d8ZV4IMgHt9biik9cS9a0hrmg_Content;
struct tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA {
	NU16 line;
	NI16 col;
	NI32 fileIndex;
};
typedef NU32 tySet_tyEnum_TNodeFlag__3UjGgL9bLM4RXBnoZJs9cviw;
typedef NU8 tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw;
struct NimStrPayload {
	NI cap;
	NIM_CHAR data[SEQ_DECL_SIZE];
};
struct NimStringV2 {
	NI len;
	NimStrPayload* p;
};
struct tySequence__9ahcSjIErJohSBBFLwuF56Q {
  NI len; tySequence__9ahcSjIErJohSBBFLwuF56Q_Content* p;
};
struct tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ {
	tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg* typ;
	tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA info;
	tySet_tyEnum_TNodeFlag__3UjGgL9bLM4RXBnoZJs9cviw flags;
	tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw kind;
union{
	struct {
	NI64 intVal;
} _kind_1;
	struct {
	NF floatVal;
} _kind_2;
	struct {
	NimStringV2 strVal;
} _kind_3;
	struct {
	tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* sym;
} _kind_4;
	struct {
	tyObject_TIdent__LG4CHFyohryWo9bzZeyjDjA* ident;
} _kind_5;
	struct {
	tySequence__9ahcSjIErJohSBBFLwuF56Q sons;
} _kind_6;
};
};
struct tyObject_ItemId__N3z7uIvKCrk0S9cej5ew0nA {
	NI32 module;
	NI32 item;
};
typedef NU8 tyEnum_TSymKind__9a75lHd9cV25XwN4jXXBmM0Q;
typedef NU16 tyEnum_TMagic__I3BbtzbLAKAfOym58MkGAg;
typedef NU64 tySet_tyEnum_TSymFlag__Ii9cPbwXR8ePKVRFOdTKpDg;
typedef NU32 tySet_tyEnum_TOption__LEwWf0yAgZkZx5Got2wGSQ;
typedef NU8 tyEnum_TLocKind__4f5UmCRM8alrCgj32FqIRQ;
typedef NU8 tyEnum_TStorageLoc__f19b6ZkRyh7BNPxlyjRbEGg;
typedef NU16 tySet_tyEnum_TLocFlag__Df76pxukl21zlyRFq9aa0Bg;
struct tyObject_TLoc__KvQVlvKb6mbV567NQ0hMOw {
	tyEnum_TLocKind__4f5UmCRM8alrCgj32FqIRQ k;
	tyEnum_TStorageLoc__f19b6ZkRyh7BNPxlyjRbEGg storage;
	tySet_tyEnum_TLocFlag__Df76pxukl21zlyRFq9aa0Bg flags;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* lode;
	NimStringV2 snippet;
};
struct tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg {
	tyObject_ItemId__N3z7uIvKCrk0S9cej5ew0nA itemId;
	tyEnum_TSymKind__9a75lHd9cV25XwN4jXXBmM0Q kind;
union{
	struct {
	tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* gcUnsafetyReason;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* transformedBody;
} _kind_1;
	struct {
	tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* guard;
	NI bitsize;
	NI alignment;
} _kind_2;
};
	tyEnum_TMagic__I3BbtzbLAKAfOym58MkGAg magic;
	tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg* typ;
	tyObject_TIdent__LG4CHFyohryWo9bzZeyjDjA* name;
	tyObject_TLineInfo__cGj50BJ9bj3Zx09a9bMvKs8HA info;
	tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* owner;
	tySet_tyEnum_TSymFlag__Ii9cPbwXR8ePKVRFOdTKpDg flags;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* ast;
	tySet_tyEnum_TOption__LEwWf0yAgZkZx5Got2wGSQ options;
	NI position;
	NI32 offset;
	NI32 disamb;
	tyObject_TLoc__KvQVlvKb6mbV567NQ0hMOw loc;
	tyObject_TLib__sVuuRvQkiYNOUiPRZhmZig* annex;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* constraint;
	tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* instantiatedFrom;
};
typedef NU8 tyEnum_TTypeKind__DYVdhzQ9cMfgoirdzGDwSCg;
typedef NU8 tyEnum_TCallingConvention__MvGn9cvx9bNYQKo7Hhd5GM9cQ;
typedef NU64 tySet_tyEnum_TTypeFlag__HJaExo9c84N85e9br9anJmV1A;
struct tySequence__d8ZV4IMgHt9biik9cS9a0hrmg {
  NI len; tySequence__d8ZV4IMgHt9biik9cS9a0hrmg_Content* p;
};
struct tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg {
	tyObject_ItemId__N3z7uIvKCrk0S9cej5ew0nA itemId;
	tyEnum_TTypeKind__DYVdhzQ9cMfgoirdzGDwSCg kind;
	tyEnum_TCallingConvention__MvGn9cvx9bNYQKo7Hhd5GM9cQ callConv;
	tySet_tyEnum_TTypeFlag__HJaExo9c84N85e9br9anJmV1A flags;
	tySequence__d8ZV4IMgHt9biik9cS9a0hrmg sons;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* n;
	tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* owner;
	tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* sym;
	NI64 size;
	NI16 align;
	NI16 paddingAtEnd;
	tyObject_TLoc__KvQVlvKb6mbV567NQ0hMOw loc;
	tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg* typeInst;
	tyObject_ItemId__N3z7uIvKCrk0S9cej5ew0nA uniqueId;
};
typedef NU8 tySet_tyEnum_TTypeKind__DYVdhzQ9cMfgoirdzGDwSCg[9];
typedef NU8 tyEnum_TTypeFlag__HJaExo9c84N85e9br9anJmV1A;
typedef NU8 tyEnum_AliasKind__lRVSpyjZSQUjiqOwUaCudg;
struct tySequence__9ahcSjIErJohSBBFLwuF56Q_Content { NI cap; tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* data[SEQ_DECL_SIZE]; };
struct tySequence__d8ZV4IMgHt9biik9cS9a0hrmg_Content { NI cap; tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg* data[SEQ_DECL_SIZE]; };
N_LIB_PRIVATE N_NIMCALL(void, eqcopy___ast_u3228)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ** dest_p0, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* src_p1);
N_LIB_PRIVATE N_NIMCALL(tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg*, skipTypes__ast_u5346)(tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg* t_p0, tySet_tyEnum_TTypeKind__DYVdhzQ9cMfgoirdzGDwSCg kinds_p1);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___ast_u3351)(tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg* dest_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___ast_u3225)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* dest_p0);
static N_INLINE(NIM_BOOL, isSinkParam__ast_u7393)(tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* s_p0);
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void);
N_LIB_PRIVATE N_NIMCALL(void*, newSeqPayload)(NI cap_p0, NI elemSize_p1, NI elemAlign_p2);
N_LIB_PRIVATE N_NIMCALL(void, add__ast_u3198)(tySequence__9ahcSjIErJohSBBFLwuF56Q* x_p0, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* y_p1);
N_LIB_PRIVATE N_NIMCALL(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ*, eqdup___ast_u3231)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* src_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqwasMoved___ast_u3222)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ** dest_p0);
N_LIB_PRIVATE N_NIMCALL(void, eqdestroy___ast_u3669)(tySequence__9ahcSjIErJohSBBFLwuF56Q dest_p0);
static N_INLINE(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ**, X5BX5D___ast_u5178)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ** s_p0, NI s_p0Len_0, NI i_p1);
static NIM_CONST tySet_tyEnum_TTypeKind__DYVdhzQ9cMfgoirdzGDwSCg TM__G8KBvM9abjBQzcaWQdD0HzQ_2 = {
0x10, 0xa9, 0x00, 0x00, 0x00, 0x40, 0x80, 0x00,
0x00}
;
extern NIM_BOOL nimInErrorMode__system_u4276;
static N_INLINE(NIM_BOOL, isSinkParam__ast_u7393)(tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* s_p0) {
	NIM_BOOL result;
	NIM_BOOL T1_;
	NIM_BOOL T3_;
	T1_ = (NIM_BOOL)0;
	T1_ = ((*s_p0).kind == ((tyEnum_TSymKind__9a75lHd9cV25XwN4jXXBmM0Q)3));
	if (!(T1_)) goto LA2_;
	T3_ = (NIM_BOOL)0;
	T3_ = ((*(*s_p0).typ).kind == ((tyEnum_TTypeKind__DYVdhzQ9cMfgoirdzGDwSCg)46));
	if (T3_) goto LA4_;
	T3_ = (((*(*s_p0).typ).flags &((NU64)1<<((NU)((((tyEnum_TTypeFlag__HJaExo9c84N85e9br9anJmV1A)4)))&63U)))!=0);
LA4_: ;
	T1_ = T3_;
LA2_: ;
	result = T1_;
	return result;
}
static N_INLINE(NIM_BOOL*, nimErrorFlag)(void) {
	NIM_BOOL* result;
	result = (&nimInErrorMode__system_u4276);
	return result;
}
N_LIB_PRIVATE N_NIMCALL(NIM_BOOL, isAnalysableFieldAccess__aliasanalysis_u8)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* orig_p0, tyObject_TSym__lYFc3T7YHHNBnaZR9at0NSg* owner_p1) {
	NIM_BOOL result;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* n;
NIM_BOOL oldNimErrFin7_;
	NIM_BOOL T15_;
	NIM_BOOL T16_;
	NIM_BOOL T17_;
	tySet_tyEnum_TSymFlag__Ii9cPbwXR8ePKVRFOdTKpDg T20_;
	NIM_BOOL T22_;
NIM_BOOL oldNimErrFin1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (NIM_BOOL)0;
	n = NIM_NIL;
	eqcopy___ast_u3228(&n, orig_p0);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		while (1) {
			switch ((*n).kind) {
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)42):
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)45) ... ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)46):
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)63) ... ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)64):
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)66) ... ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)67):
			{
				eqcopy___ast_u3228(&n, (*n)._kind_6.sons.p->data[((NI)0)]);
				if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
			}
			break;
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)58) ... ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)59):
			{
				eqcopy___ast_u3228(&n, (*n)._kind_6.sons.p->data[((NI)1)]);
				if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
			}
			break;
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)65):
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)47):
			{
				tyObject_TType__8qAQZfpTDk89aeclTNn4d9cg* colontmpD_;
				NIM_BOOL T8_;
				NIM_BOOL T9_;
				colontmpD_ = NIM_NIL;
				eqcopy___ast_u3228(&n, (*n)._kind_6.sons.p->data[((NI)0)]);
				if (NIM_UNLIKELY(*nimErr_)) goto LA7_;
				T8_ = (NIM_BOOL)0;
				T9_ = (NIM_BOOL)0;
				T9_ = ((*n).kind == ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)3));
				if (!(T9_)) goto LA10_;
				T9_ = ((*(*n)._kind_4.sym).owner == owner_p1);
LA10_: ;
				T8_ = T9_;
				if (!(T8_)) goto LA11_;
				colontmpD_ = skipTypes__ast_u5346((*(*n)._kind_4.sym).typ, TM__G8KBvM9abjBQzcaWQdD0HzQ_2);
				if (NIM_UNLIKELY(*nimErr_)) goto LA7_;
				T8_ = ((*colontmpD_).kind == ((tyEnum_TTypeKind__DYVdhzQ9cMfgoirdzGDwSCg)45));
LA11_: ;
				result = T8_;
				eqdestroy___ast_u3351(colontmpD_);
				if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
				eqdestroy___ast_u3225(n);
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
				goto BeforeRet_;
				{
					LA7_:;
				}
				{
					oldNimErrFin7_ = *nimErr_; *nimErr_ = NIM_FALSE;
					eqdestroy___ast_u3351(colontmpD_);
					if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
					*nimErr_ = oldNimErrFin7_;
				}
				if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
			}
			break;
			default:
			{
				goto LA2;
			}
			break;
			}
		}
	} LA2: ;
	T15_ = (NIM_BOOL)0;
	T16_ = (NIM_BOOL)0;
	T17_ = (NIM_BOOL)0;
	T17_ = ((*n).kind == ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)3));
	if (!(T17_)) goto LA18_;
	T17_ = ((*(*n)._kind_4.sym).owner == owner_p1);
LA18_: ;
	T16_ = T17_;
	if (!(T16_)) goto LA19_;
	T20_ = 0;
	T16_ = ((IL64(2199090364424) & (*(*n)._kind_4.sym).flags) == T20_);
LA19_: ;
	T15_ = T16_;
	if (!(T15_)) goto LA21_;
	T22_ = (NIM_BOOL)0;
	T22_ = !(((*(*n)._kind_4.sym).kind == ((tyEnum_TSymKind__9a75lHd9cV25XwN4jXXBmM0Q)3)));
	if (T22_) goto LA23_;
	T22_ = isSinkParam__ast_u7393((*n)._kind_4.sym);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
LA23_: ;
	T15_ = T22_;
LA21_: ;
	result = T15_;
	{
		LA1_:;
	}
	{
		oldNimErrFin1_ = *nimErr_; *nimErr_ = NIM_FALSE;
		eqdestroy___ast_u3225(n);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		*nimErr_ = oldNimErrFin1_;
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
N_LIB_PRIVATE N_NIMCALL(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ*, skipConvDfa__aliasanalysis_u5)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* n_p0) {
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* result;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = NIM_NIL;
	eqcopy___ast_u3228(&result, n_p0);
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	{
		while (1) {
			switch ((*result).kind) {
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)66):
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)67):
			{
				eqcopy___ast_u3228(&result, (*result)._kind_6.sons.p->data[((NI)0)]);
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			}
			break;
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)58) ... ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)59):
			{
				eqcopy___ast_u3228(&result, (*result)._kind_6.sons.p->data[((NI)1)]);
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
			}
			break;
			default:
			{
				goto LA1;
			}
			break;
			}
		}
	} LA1: ;
	}BeforeRet_: ;
	return result;
}
static N_INLINE(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ**, X5BX5D___ast_u5178)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ** s_p0, NI s_p0Len_0, NI i_p1) {
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ** result;
	result = &s_p0[(NI)(s_p0Len_0 - i_p1)];
	return result;
}
N_LIB_PRIVATE N_NIMCALL(tyEnum_AliasKind__lRVSpyjZSQUjiqOwUaCudg, aliases__aliasanalysis_u61)(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* obj_p0, tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* field_p1) {
	tyEnum_AliasKind__lRVSpyjZSQUjiqOwUaCudg result;
	tySequence__9ahcSjIErJohSBBFLwuF56Q objImportantNodes;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* obj_2;
	tySequence__9ahcSjIErJohSBBFLwuF56Q fieldImportantNodes;
	tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* field_2;
NIM_BOOL oldNimErrFin26_;
NIM_BOOL oldNimErrFin1_;
NIM_BOOL* nimErr_;
{nimErr_ = nimErrorFlag();
	result = (tyEnum_AliasKind__lRVSpyjZSQUjiqOwUaCudg)0;
	objImportantNodes.len = 0; objImportantNodes.p = NIM_NIL;
	obj_2 = NIM_NIL;
	fieldImportantNodes.len = 0; fieldImportantNodes.p = NIM_NIL;
	field_2 = NIM_NIL;
	objImportantNodes.len = 0; objImportantNodes.p = (tySequence__9ahcSjIErJohSBBFLwuF56Q_Content*) newSeqPayload(0, sizeof(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ*), NIM_ALIGNOF(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ*));
	eqcopy___ast_u3228(&obj_2, obj_p0);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		while (1) {
			switch ((*obj_2).kind) {
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)46):
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)63) ... ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)64):
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)66) ... ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)67):
			{
				eqcopy___ast_u3228(&obj_2, (*obj_2)._kind_6.sons.p->data[((NI)0)]);
				if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
			}
			break;
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)58) ... ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)59):
			{
				eqcopy___ast_u3228(&obj_2, (*obj_2)._kind_6.sons.p->data[((NI)1)]);
				if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
			}
			break;
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)45):
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)42):
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)47):
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)65):
			{
				tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* colontmpD_;
				colontmpD_ = NIM_NIL;
				colontmpD_ = eqdup___ast_u3231(obj_2);
				if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
				add__ast_u3198((&objImportantNodes), colontmpD_);
				eqcopy___ast_u3228(&obj_2, (*obj_2)._kind_6.sons.p->data[((NI)0)]);
				if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
			}
			break;
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)3):
			{
				tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* blitTmp;
				blitTmp = obj_2;
				eqwasMoved___ast_u3222(&obj_2);
				if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
				add__ast_u3198((&objImportantNodes), blitTmp);
				goto LA2;
			}
			break;
			default:
			{
				result = ((tyEnum_AliasKind__lRVSpyjZSQUjiqOwUaCudg)1);
				eqdestroy___ast_u3225(field_2);
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
				eqdestroy___ast_u3669(fieldImportantNodes);
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
				eqdestroy___ast_u3225(obj_2);
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
				eqdestroy___ast_u3669(objImportantNodes);
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
				goto BeforeRet_;
			}
			break;
			}
		}
	} LA2: ;
	fieldImportantNodes.len = 0; fieldImportantNodes.p = (tySequence__9ahcSjIErJohSBBFLwuF56Q_Content*) newSeqPayload(0, sizeof(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ*), NIM_ALIGNOF(tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ*));
	eqcopy___ast_u3228(&field_2, field_p1);
	if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
	{
		while (1) {
			switch ((*field_2).kind) {
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)46):
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)63) ... ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)64):
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)66) ... ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)67):
			{
				eqcopy___ast_u3228(&field_2, (*field_2)._kind_6.sons.p->data[((NI)0)]);
				if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
			}
			break;
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)58) ... ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)59):
			{
				eqcopy___ast_u3228(&field_2, (*field_2)._kind_6.sons.p->data[((NI)1)]);
				if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
			}
			break;
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)45):
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)42):
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)47):
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)65):
			{
				tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* colontmpD__2;
				colontmpD__2 = NIM_NIL;
				colontmpD__2 = eqdup___ast_u3231(field_2);
				if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
				add__ast_u3198((&fieldImportantNodes), colontmpD__2);
				eqcopy___ast_u3228(&field_2, (*field_2)._kind_6.sons.p->data[((NI)0)]);
				if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
			}
			break;
			case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)3):
			{
				tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* blitTmp_2;
				blitTmp_2 = field_2;
				eqwasMoved___ast_u3222(&field_2);
				if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
				add__ast_u3198((&fieldImportantNodes), blitTmp_2);
				goto LA9;
			}
			break;
			default:
			{
				result = ((tyEnum_AliasKind__lRVSpyjZSQUjiqOwUaCudg)1);
				eqdestroy___ast_u3225(field_2);
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
				eqdestroy___ast_u3669(fieldImportantNodes);
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
				eqdestroy___ast_u3225(obj_2);
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
				eqdestroy___ast_u3669(objImportantNodes);
				if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
				goto BeforeRet_;
			}
			break;
			}
		}
	} LA9: ;
	{
		NI T18_;
		NI T19_;
		T18_ = fieldImportantNodes.len;
		T19_ = objImportantNodes.len;
		if (!(T18_ < T19_)) goto LA20_;
		result = ((tyEnum_AliasKind__lRVSpyjZSQUjiqOwUaCudg)1);
		eqdestroy___ast_u3225(field_2);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqdestroy___ast_u3669(fieldImportantNodes);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqdestroy___ast_u3225(obj_2);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqdestroy___ast_u3669(objImportantNodes);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		goto BeforeRet_;
	}
LA20_: ;
	result = ((tyEnum_AliasKind__lRVSpyjZSQUjiqOwUaCudg)0);
	{
		NI i;
		NI colontmp_;
		NI T23_;
		NI res;
		i = (NI)0;
		colontmp_ = (NI)0;
		T23_ = objImportantNodes.len;
		colontmp_ = T23_;
		res = ((NI)1);
		{
			while (1) {
				tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* currFieldPath;
				tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ* currObjPath;
				tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ** T27_;
				tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ** T28_;
				if (!(res <= colontmp_)) goto LA25;
				currFieldPath = NIM_NIL;
				currObjPath = NIM_NIL;
				i = ((NI) (res));
				T27_ = (tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ**)0;
				T27_ = X5BX5D___ast_u5178(((fieldImportantNodes).p) ? (fieldImportantNodes.p->data) : NIM_NIL, fieldImportantNodes.len, i);
				if (NIM_UNLIKELY(*nimErr_)) goto LA26_;
				eqcopy___ast_u3228(&currFieldPath, (*T27_));
				if (NIM_UNLIKELY(*nimErr_)) goto LA26_;
				T28_ = (tyObject_TNode__eTSqWXWVL7LuzmZf0c6unQ**)0;
				T28_ = X5BX5D___ast_u5178(((objImportantNodes).p) ? (objImportantNodes.p->data) : NIM_NIL, objImportantNodes.len, i);
				if (NIM_UNLIKELY(*nimErr_)) goto LA26_;
				eqcopy___ast_u3228(&currObjPath, (*T28_));
				if (NIM_UNLIKELY(*nimErr_)) goto LA26_;
				{
					if (!!(((*currFieldPath).kind == (*currObjPath).kind))) goto LA31_;
					result = ((tyEnum_AliasKind__lRVSpyjZSQUjiqOwUaCudg)1);
					eqdestroy___ast_u3225(currObjPath);
					if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
					eqdestroy___ast_u3225(currFieldPath);
					if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
					eqdestroy___ast_u3225(field_2);
					if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
					eqdestroy___ast_u3669(fieldImportantNodes);
					if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
					eqdestroy___ast_u3225(obj_2);
					if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
					eqdestroy___ast_u3669(objImportantNodes);
					if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
					goto BeforeRet_;
				}
LA31_: ;
				switch ((*currFieldPath).kind) {
				case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)3):
				{
					{
						if (!!(((*currFieldPath)._kind_4.sym == (*currObjPath)._kind_4.sym))) goto LA36_;
						result = ((tyEnum_AliasKind__lRVSpyjZSQUjiqOwUaCudg)1);
						eqdestroy___ast_u3225(currObjPath);
						if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
						eqdestroy___ast_u3225(currFieldPath);
						if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
						eqdestroy___ast_u3225(field_2);
						if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
						eqdestroy___ast_u3669(fieldImportantNodes);
						if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
						eqdestroy___ast_u3225(obj_2);
						if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
						eqdestroy___ast_u3669(objImportantNodes);
						if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
						goto BeforeRet_;
					}
LA36_: ;
				}
				break;
				case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)45):
				{
					{
						if (!!(((*(*currFieldPath)._kind_6.sons.p->data[((NI)1)])._kind_4.sym == (*(*currObjPath)._kind_6.sons.p->data[((NI)1)])._kind_4.sym))) goto LA41_;
						result = ((tyEnum_AliasKind__lRVSpyjZSQUjiqOwUaCudg)1);
						eqdestroy___ast_u3225(currObjPath);
						if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
						eqdestroy___ast_u3225(currFieldPath);
						if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
						eqdestroy___ast_u3225(field_2);
						if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
						eqdestroy___ast_u3669(fieldImportantNodes);
						if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
						eqdestroy___ast_u3225(obj_2);
						if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
						eqdestroy___ast_u3669(objImportantNodes);
						if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
						goto BeforeRet_;
					}
LA41_: ;
				}
				break;
				case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)47):
				case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)65):
				{
				}
				break;
				case ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)42):
				{
					{
						NIM_BOOL T47_;
						T47_ = (NIM_BOOL)0;
						T47_ = ((*(*currFieldPath)._kind_6.sons.p->data[((NI)1)]).kind >= ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)5) && (*(*currFieldPath)._kind_6.sons.p->data[((NI)1)]).kind <= ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)22));
						if (!(T47_)) goto LA48_;
						T47_ = ((*(*currObjPath)._kind_6.sons.p->data[((NI)1)]).kind >= ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)5) && (*(*currObjPath)._kind_6.sons.p->data[((NI)1)]).kind <= ((tyEnum_TNodeKind__eZrq5BxQzEyOryCcwdj4Kw)22));
LA48_: ;
						if (!T47_) goto LA49_;
						{
							if (!!(((*(*currFieldPath)._kind_6.sons.p->data[((NI)1)])._kind_1.intVal == (*(*currObjPath)._kind_6.sons.p->data[((NI)1)])._kind_1.intVal))) goto LA53_;
							result = ((tyEnum_AliasKind__lRVSpyjZSQUjiqOwUaCudg)1);
							eqdestroy___ast_u3225(currObjPath);
							if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
							eqdestroy___ast_u3225(currFieldPath);
							if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
							eqdestroy___ast_u3225(field_2);
							if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
							eqdestroy___ast_u3669(fieldImportantNodes);
							if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
							eqdestroy___ast_u3225(obj_2);
							if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
							eqdestroy___ast_u3669(objImportantNodes);
							if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
							goto BeforeRet_;
						}
LA53_: ;
					}
					goto LA45_;
LA49_: ;
					{
						result = ((tyEnum_AliasKind__lRVSpyjZSQUjiqOwUaCudg)2);
					}
LA45_: ;
				}
				break;
				default:
				{
				}
				break;
				}
				res += ((NI)1);
				{
					LA26_:;
				}
				{
					oldNimErrFin26_ = *nimErr_; *nimErr_ = NIM_FALSE;
					eqdestroy___ast_u3225(currObjPath);
					if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
					eqdestroy___ast_u3225(currFieldPath);
					if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
					*nimErr_ = oldNimErrFin26_;
				}
				if (NIM_UNLIKELY(*nimErr_)) goto LA1_;
			} LA25: ;
		}
	}
	{
		LA1_:;
	}
	{
		oldNimErrFin1_ = *nimErr_; *nimErr_ = NIM_FALSE;
		eqdestroy___ast_u3225(field_2);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqdestroy___ast_u3669(fieldImportantNodes);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqdestroy___ast_u3225(obj_2);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		eqdestroy___ast_u3669(objImportantNodes);
		if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
		*nimErr_ = oldNimErrFin1_;
	}
	if (NIM_UNLIKELY(*nimErr_)) goto BeforeRet_;
	}BeforeRet_: ;
	return result;
}
